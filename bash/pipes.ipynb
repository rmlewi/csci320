{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1c7e450d",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">Bash Survival School</h1>\n",
    "<h1 style=\"text-align:center;\">Pipes and command pipelines</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0e3e151",
   "metadata": {},
   "source": [
    "<a href=\"redirection.ipynb\" style=\"float:left;\">Previous: I/O redirection</a> \n",
    "<a href=\"env.ipynb\" style=\"float:right;\">Next: Shell variables</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8bcc386",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Pipes](#Pipes)\n",
    "    * [The clumsy way](#The-clumsy-way)\n",
    "    * [The elegant way: introducing pipes](#The-elegant-way:-introducing-pipes)\n",
    "* [Examples of pipes](#Examples-of-pipes)\n",
    "    * [Example: `tr` revisited](#Example:-tr-revisited)\n",
    "    * [Looking at disk usage](#Looking-at-disk-usage)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5eea6e71",
   "metadata": {},
   "source": [
    "# Pipes \n",
    "\n",
    "Suppose we want to build a list of the notebooks in the notes for this class.  All the filenames of all the notebooks end with `.ipynb`, so we can use that to search for the files.\n",
    "\n",
    "There is a powerful command called `find` that recursively searches directories looking for files that satisfied specified criteria.  Here we ask it to look in the root directory of the class notes and find every file ending in `.ipynb`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b8140af",
   "metadata": {},
   "outputs": [],
   "source": [
    "find .. -name \\*.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41d6f855",
   "metadata": {},
   "source": [
    "In this case, we escape the wildcard in `\\*.ipynb` since otherwise it would expanded before `find` is executed and the result added to the command line.  Here we first execute `set -x` so that subsequent commands are echoed to the command line showing what they look like after globbing (wildcard expansion):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b25a7a2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "set -x  # Show commands as executed after globbing.\n",
    "\n",
    "find .. -name \\*.ipynb\n",
    "\n",
    "set +x  # Turn off post-globbing command echoing."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5410adcf",
   "metadata": {},
   "source": [
    "Without the escape `\\*.ipynb` the filenames from globbing are passed to `find` as command line arguments, leading to an error."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3256ba28",
   "metadata": {},
   "source": [
    "Returning to our original task, notice that `find` finds a lot of duplicate notebooks that are checkpoints of the ones we want:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "87c6b27e",
   "metadata": {},
   "outputs": [],
   "source": [
    "find .. -name \\*.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0512b070",
   "metadata": {},
   "source": [
    "We would like to get rid of all the files with `.ipynb_checkpoints` in their pathnames.  First we do this the clumsy way, and then the elegant way."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b151fdc",
   "metadata": {},
   "source": [
    "## The clumsy way\n",
    "\n",
    "As a first step we redirect the output of `find` to a file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fafa3c3c",
   "metadata": {},
   "outputs": [],
   "source": [
    "find .. -name \\*.ipynb > find.out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "442a5491",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat find.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efcc2801",
   "metadata": {},
   "source": [
    "Now we use `fgrep` with the `-v` option, which says ignore lines that contain the specified pattern:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7c5ed5b0",
   "metadata": {},
   "outputs": [],
   "source": [
    "fgrep -v checkpoint find.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8196392c",
   "metadata": {},
   "source": [
    "## The elegant way: introducing pipes\n",
    "\n",
    "In the clumsy approach, we redirected the output of `stdout` for `find` to a file, and then read the file with `fgrep`.  Wouldn't it be more efficient to redirect the output of `stdout` for `find` to the `stdin` of `fgrep`, and eliminate the write/read steps?\n",
    "\n",
    "We can do so with a **pipe**.  The pipe operator is `|`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35cc45e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "find .. -name \\*.ipynb | fgrep -v checkpoint"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12dd40c3",
   "metadata": {},
   "source": [
    "The pipe redirects the `stdout` of `find` to the `stdin` of `fgrep`.  Because `fgrep` wasn't given the name of a file to read, it expects its input from `stdin` and knows to read what it receives there as the text to be processed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f18d0fb",
   "metadata": {},
   "source": [
    "# Examples of pipes\n",
    "\n",
    "Pipes are a powerful tool that allow us to take simple tools and combine them into complex data processing pipelines.  You are limited only by your imagination!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7db16096",
   "metadata": {},
   "source": [
    "## Example: `tr` revisited\n",
    "\n",
    "Let's redo the `tr` example from the section on redirecting standard input, with a twist.  Suppose we want to find all lines containing the word \"gland\".  A problem we face is that it might appear as \"gland\" or \"Gland\".  The standard solution in such situations is to convert all the letters to lowercase (or uppercase, depending on your tastes).\n",
    "\n",
    "In the earlier example we used"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09ce2088",
   "metadata": {},
   "outputs": [],
   "source": [
    "tr a-z A-Z < muskrat.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5794d208",
   "metadata": {},
   "source": [
    "Here is a pipeline that does the same thing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2d3cf98",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat muskrat.txt | tr a-z A-Z"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e932926e",
   "metadata": {},
   "source": [
    "Since all-capitalized documents look like ransom notes or crank email, let's go the other direction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d524c34d",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat muskrat.txt | tr A-Z a-z"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19b695a0",
   "metadata": {},
   "source": [
    "Let's use `fgrep` on the resulting preprocessed text in a three-stage pipeline:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e27cd3e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat muskrat.txt | tr A-Z a-z | fgrep gland"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a1f90e9",
   "metadata": {},
   "source": [
    "## Looking at disk usage\n",
    "\n",
    "The `du` command tells us about disk usage at the level of directories.  I find it useful when looking for big files that are taking up a lot of space unnecessarily.\n",
    "\n",
    "It has a number of useful options.  Here is `-s`, which is a summary for a directory, including all its subdirectories:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b13ba2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "du -s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7b1cec6",
   "metadata": {},
   "source": [
    "By default `du` reports sizes in terms of 512 byte blocks because this was a common disk block size at the time the `du` command was [standardized under POSIX](https://en.wikipedia.org/wiki/POSIX#512-_vs_1024-byte_blocks).  \n",
    "\n",
    "The `-h` option tells `du` to present the result in human-friendly terms, in this case bytes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1d67fbf",
   "metadata": {},
   "outputs": [],
   "source": [
    "du -sh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3927e19f",
   "metadata": {},
   "source": [
    "We can also request a directory-by-directory listing of disk usage:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e5f7e4dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "du"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b2c27bb",
   "metadata": {},
   "source": [
    "Now suppose we would like this sorted from largest to smallest, so we can spot where the disk hogs are.  Conveniently, there is a `sort` command, and we can pipe the results from `du` to `sort`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e5489855",
   "metadata": {},
   "outputs": [],
   "source": [
    "du | sort"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4b7a777",
   "metadata": {},
   "source": [
    "Oops!  we sorted the lines in lexicographical order.  To get numerical order we need to use the `-n` for `sort`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42655586",
   "metadata": {},
   "outputs": [],
   "source": [
    "du | sort -n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "850161db",
   "metadata": {},
   "source": [
    "By default `sort` sorts in ascending order; to get the results in reverse order we need to use the `-r` option:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a297a0b1",
   "metadata": {},
   "outputs": [],
   "source": [
    "du | sort -rn"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f7823ce",
   "metadata": {},
   "source": [
    "Finally, suppose we only want to see, say, the top 10 lines of the output.  The `head` command displays the top lines (by default, 10) of its input.  Here is `head` applied to a file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c279a9f2",
   "metadata": {},
   "outputs": [],
   "source": [
    "head muskrat.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "842de682",
   "metadata": {},
   "source": [
    "We can specify a different number of lines with the `-n` option:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43f8c810",
   "metadata": {},
   "outputs": [],
   "source": [
    "head -n 3 muskrat.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77d468c7",
   "metadata": {},
   "source": [
    "Here is `head` used in our pipeline to extract the 3 largest directories:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7096b363",
   "metadata": {},
   "outputs": [],
   "source": [
    "du .. | sort -nr | head -n 3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48ca4131",
   "metadata": {},
   "source": [
    " There is a similar `tail` command that extracts the last lines of its input:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f355d65b",
   "metadata": {},
   "outputs": [],
   "source": [
    "tail -n 3 muskrat.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d27bd36",
   "metadata": {},
   "source": [
    "Here is our pipeline using `tail`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "88898f57",
   "metadata": {},
   "outputs": [],
   "source": [
    "du .. | sort -n | tail -n 3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "835148ab",
   "metadata": {},
   "source": [
    "<a href=\"redirection.ipynb\" style=\"float:left;\">Previous: I/O redirection</a> \n",
    "<a href=\"env.ipynb\" style=\"float:right;\">Next: Shell variables</a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
