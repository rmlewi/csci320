{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b25e4c35",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">Bash Survival School</h1>\n",
    "<h1 style=\"text-align:center;\">Getting started</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd2f46c8",
   "metadata": {},
   "source": [
    "<a href=\"navigation.ipynb\" style=\"float:right;\">Next: Navigation</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8443cd7e",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Unix, Linux, and Posix](#Unix,-Linux,-and-Posix)\n",
    "* [What is a shell?](#What-is-a-shell?)\n",
    "* [Speaking &#42;nix](#speak-nix)\n",
    "* [Remote login with `ssh`](#ssh)\n",
    "* [&iexcl;Cuidado!  &iexcl;llamas!](#llamas)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5629ac64",
   "metadata": {},
   "source": [
    "# Unix, Linux, and Posix\n",
    "\n",
    "The Unix operating system (OS) was developed at AT&amp;T Bell Labs starting in 1969.  The name is a play on [Multics](https://en.wikipedia.org/wiki/Multics), an OS that preceded Unix.  \n",
    "\n",
    "Linux (more precisely, the Linux kernel) got its start in 1991 as a personal project of Linus Torvalds, a Finnish computer science student; the name Linux is derived from his name.\n",
    "\n",
    "Linux and Unix differ in technical ways as operating systems, but the way we interact with them is pretty much the same for both. We will use &lowast;nix to refer to [Unix-like OS](https://en.wikipedia.org/wiki/Unix-like) (in the world of the Unix/Linux command line, [&lowast; means \"match anything\"](http://localhost:8888/notebooks/navigation.ipynb#Wildcards)).  The [POSIX standard](https://en.wikipedia.org/wiki/POSIX) standardizes &lowast;nix operating system behavior; in varying degrees both Unix and Linux conform to this standard.\n",
    "\n",
    "Both Mac and Windows bow to Unix and Linux.  Mac OS is [just Unix with a man bun](https://www.opengroup.org/openbrand/register/), while Windows 10 includes the [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/), which is a full-fledged Linux distribution that runs inside Windows."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf1f02ea",
   "metadata": {},
   "source": [
    "# What is a shell?\n",
    "\n",
    "We will spend most of our time in terminal windows working at a command line shell.\n",
    "<blockquote>\n",
    "In computing, a shell is a computer program which exposes an operating system's services to a human user or other program. <br/>\n",
    "&ndash; <a href=\"https://en.wikipedia.org/wiki/Shell_(computing)\")>Wikipedia</a>\n",
    "</blockquote>\n",
    "The idea is that a shell surrounds the OS like the shell of a turtle and is an interface to the outside world.\n",
    "\n",
    "There are a lot of command line shells in the &lowast;nix world.  Here are some notable examples:\n",
    "* `sh`, the original shell (Ken Thompson, 1971),\n",
    "* `csh`, the C shell (Bill Joy, 1978),\n",
    "* `sh`, the Bourne shell (Stephen Bourne, 1979),\n",
    "* `ksh`, the Korn shell (David Korn, 1983),\n",
    "* `bash`, the Bourne again shell (Brian Fox, 1989),\n",
    "* `ash`, the Almquist shell (Kenneth Almquist, 1989),\n",
    "* `zsh`, the Z shell (Paul Falstad et al., 1990).\n",
    "\n",
    "These shells [differ in relatively minor ways](https://en.wikipedia.org/wiki/Comparison_of_command_shells); if you know how to use one it isn't a problem to switch to another.  \n",
    "\n",
    "The most commonly used shell is `bash`, so that will be the shell we will study.\n",
    "\n",
    "Another popular shell is `zsh`, an extension of `bash` that has been around since 1990 but has recently grown in popularity because\n",
    "* it is highly customizable,\n",
    "* the `zsh` enthusiasts behind [Oh My Zsh](https://ohmyz.sh) have made it easy to customize `zsh`, and\n",
    "* `zsh` is now the default shell on Macs.\n",
    "\n",
    "There are also command line shells in Windows:\n",
    "* the ```cmd``` shell descended from DOS,\n",
    "* PowerShell."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69dd6b28",
   "metadata": {},
   "source": [
    "# Speaking &lowast;nix  <a id=\"speak-nix\"/>\n",
    "\n",
    "The control key, indicated by `CTRL` or `C`, modifies another key when held down at the same time.  Thus, `CTRL-Z` (spoken as \"control z\") means to hold the control and z keys down at the same time.\n",
    "\n",
    "The escape key, indicated by `ESC` or `M`, is a key in its own right.  Thus, `ESC-F` (spoken as \"escape f\") means to enter escape followed separately by \"f\".\n",
    "\n",
    "| Written as | Spoken as |\n",
    "| :----: | :---: |\n",
    "| `etc` | \"et cetera\" or \"etsy\" |\n",
    "| `proc` | \"prock\" (even though it's short for \"process\") |\n",
    "| `tmp` | \"temp\" |\n",
    "| `usr` | \"user\" |\n",
    "| `/` | \"slash\" or a brief pause in speaking |\n",
    "| `~` | \"tilde\" |\n",
    "| `.` | \"dot\" |\n",
    "| `..` | \"dot dot\" |\n",
    "\n",
    "The \"G\" in the name of the [GNU](gnu.org) open source project is pronounced as a hard \"G\".  The name is a recursive acronym for \"GNU's not Unix\"."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e05a757c",
   "metadata": {},
   "source": [
    "# Remote login with `ssh`  <a id=\"ssh\"/>\n",
    "\n",
    "You will need an account on the CS system for this course.  You may request one at [http://accounts.cs.wm.edu](http://accounts.cs.wm.edu).\n",
    "\n",
    "You can log in remotely using `ssh`, the secure shell.  \n",
    "1. Fire up Terminal.app (Mac) or PowerShell (Windows).\n",
    "2. If your CS userid is the same as the id on the machine you are logging in from, then enter `ssh hostname` at the prompt, where hostname is the name of the computer you want to login to.\n",
    "3. If your CS userid is the **not** same as the id on the machine you are logging in from, then enter `ssh your_cs_userid@hostname` at the prompt.\n",
    "\n",
    "The machines in the undergraduate lab in MCGL 121 are named `th121-N.cs.wm.edu`, where `N` is an integer between 1 and 24 (inclusive)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c5b74b7",
   "metadata": {},
   "source": [
    "# &iexcl;Cuidado!  &iexcl;llamas!  <a id=\"llamas\"/>\n",
    "\n",
    "<div class=\"danger\"></div> We will use the metric road sign for dangerous curves (also known as the <a href=\"https://en.wikipedia.org/wiki/Bourbaki_dangerous_bend_symbol\">Bourbaki dangerous bend symbol</a>) to indicate important information such as common pitfalls and mistakes.  Two such signs will be used to indicate especially important things.\n",
    "\n",
    "We will use the exploding head emoticon, 🤯, to indicate in-depth or advanced topics.\n",
    "\n",
    "We will use the sceam emoticon, 😱 , to indicate particularly frightening topics.\n",
    "\n",
    "Discussions of things that lead to bugs are marked with a 🐞."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9feb9a4a",
   "metadata": {},
   "source": [
    "<a href=\"navigation.ipynb\" style=\"float:right;\">Next: Navigation</a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
