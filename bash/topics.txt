------------------------------------
Lecture 0  (Jan 26)

Getting started
- why the CLI is more efficient
- Unix vs Linux vs Posix
- what is a shell?
- -nix lingo: CTRL-, ESC-
------------------------------------
Lecture 1 (Jan 31)

A first peek at the shell
* whoami
* man
* date
* cal
* hostname
* w
* command history

Non-notebook
* ping
* ssh
- scp
* logging out

Navigating the filesystem
* the structure of the filesystem
* pwd
* cd
* moving around the command line
  - CTRL-A
  - CTRL-B  
  - CTRL-E
  - CTRL-N  
  - CTRL-K
  - ESC-B
  - ESC-F
* ~, ., ..
- pushd, popd, dirs
* relative and absolute pathnames
* ls and its many options
  - last modified date
* wildcards

Working at the command line
* mkdir
* cp
* mv
* rm, rmdir
- touch
- dotfiles
  - the bash dotfile hierarchy:
    - .bash_profile
    - .bashrc
  - the source command
* - alias, unalias
- \cmd
- which
* emacs .bashrc, .bash_profile
- rehash
- ln
- file
* cat

Non-notebook
* less
* more
------------------------------------
File utilities
* cat, more, less
- grep
* wc
- cut
- paste
- find
- df
- du
- awk
- sed
- tar
- zip, unzip
- gzip2, gunzip2
- bzip2, bunzip2
- colrm
- diff, sdiff, cmp
- uniq
- od
- head, tail
- sed
- sort
------------------------------------
I/O redirection
* stdin, stdout, stderr
* buffered and unbuffered i/o
* redirecting i/o
------------------------------------
Pipes
* pipes
* yes
* CTRL-C
------------------------------------
The command history
- ^
- !
- !-
- !-
- sed
------------------------------------
Job management
- time
- CTRL-Z
- background jobs
  - bg
  - fg
- jobs
- CTRL-C
- kill
- ps
- nice, renice
- cron
- at
- sleep
- timeout
- sync
------------------------------------
Environment variables
* printenv, env
* echo
* USER
* PATH
* export
------------------------------------
Navigating the network
* ping
- nslookup
- RSA keys
- sftp
- curl
- ports
------------------------------------
Access permissions
- groups: user, group, other
- umask
- chmod
- chown
- chgrp
- groups
- stat
------------------------------------
Shell scripting
- variables
- environment variables
- command line arguments
- lists
- iteration
- flow control
- arithmetic
- functions
- basename
- extracting parts of file names
------------------------------------
- tmux
- su
- sudo
------------------------------------
Windowing systems
- X
- Wayland
------------------------------------
Editors
- emacs
- vim
- customization
- keyboard macros
------------------------------------
Compilation and linkage
- gcc
- clang
- make
- linkage errors
- nm
- using libraries
- ar
- building libraries
- static vs dynamic linkage
- strip
- g++
- clang++
- java
- javac
- jar
- the JRE
------------------------------------
Runtime analysis
- valgrind
- -nix debuggers
- gprof
- gcov
------------------------------------
Laptop Linux
- Windows setup
  - Installing WSL in Windows
- Mac setup 
  - Installing Homebrew
- starting a shell
  - Apple
  - Windows
- adding packages
  - Apple: Homebrew, XCode, .dmg files
  - Linux: apt, Homebrew
