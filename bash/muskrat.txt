Muskrat

Please read About Small Game, 513.

Skin and remove all fat from hams and shoulders
of:
    1 muskrat
These are the only edible portions.  Remove musk
glands under the legs and belly and white stringy tis-
sue attached to musk glands.  Poach, 150, in salted
water for 45 minutes.  Drain.  Place cut-up meat in
Dutch oven and cover with:
    Bacon strips
Add:
    1 cup water or light stock
    1 small sliced onion
    1 bay leaf
    3 cloves
    1/2 teaspoon thyme
Cover and simmer until very tender.  Serve with:
    Creamed Celery, 297

Joy of Cooking, Irma S. Rombauer and Marion Rombauer Becker,
Bobbs-Merrill Company, Inc., Indianapolis, 1975, page 516.
