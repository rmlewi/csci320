\documentclass[12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage[landscape,text={10.5in,7.5in},centering]{geometry}

\usepackage{listings}
\lstset{language=bash,showspaces=false,basicstyle=\ttfamily\bfseries}

\usepackage{scrextend}

\usepackage{multicol}

\usepackage[table]{xcolor}
\definecolor{wmgreen}{rgb}{0,0.4,0}
\definecolor{wmgreenbg}{rgb}{0.86,0.85,0.74} % dbdabd
\definecolor{wmgold}{rgb}{1,0.84,0}
\definecolor{Gray}{gray}{0.9}
\definecolor{LightCyan}{rgb}{0.88,1,1}

\usepackage{hyperref}
\hypersetup{colorlinks=true,linkcolor=wmgreen,citecolor=wmgreen,urlcolor=wmgreen}

\usepackage[first=0,last=9]{lcg}
\newcommand{\ra}{\rand0.\arabic{rand}}

\usepackage{multicol}
\setlength{\columnsep}{0.1in}

% Compute the time in hours and minutes; make new variables \timehh and \timemm.
\newcount\timehh\newcount\timemm
\timehh=\time 
\divide\timehh by 60 \timemm=\time
\count255=\timehh\multiply\count255 by -60 \advance\timemm by \count255

\pagestyle{empty}

\begin{document}

\begin{center}
  \textbf{Linux Cheat Sheet for the Bash Shell} \\
  Version: \ifnum\timehh<10 0\fi\number\timehh:\ifnum\timemm<10 0\fi\number\timemm, \today \\
  Robert Michael Lewis \\
  Department of Computer Science \\
  College of William \& Mary
\end{center}

\rowcolors{1}{}{LightCyan}

\begin{multicols}{2}
\begin{tabular}{|p{1in}p{3.75in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Moving around the command line}} \\ \hline
  $\rightarrow$ & Move the cursor forward one character \\
  $\leftarrow$  & Move the cursor backwards one character \\
  CTRL-f & Move the cursor forward one character \\
  CTRL-b & Move the cursor backwards one character \\
  ESC-f  & Move the cursor forward one word \\
  ESC-b  & Move the cursor backwards one word \\
  CTRL-a & Go to start of line \\
  CTRL-e & Go to end of line \\
  CTRL-d & Delete the character at the cursor \\
  ESC-d  & Delete the text from the cursor to the end of word \\
  CTRL-u & Delete the text from start of line to cursor \\
  CTRL-k & Delete the text from cursor to end of line \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1in}p{3.75in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Command history}} \\ \hline
  history & Show command history \\
  $\uparrow$ & Scroll backwards through history \\
  $\downarrow$ & Scroll backwards through history \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1in}p{3.75in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Bash shortcuts}} \\ \hline
  CTRL-c & Interrupt (kill) current command \\
  CTRL-z & Suspend current command \\
  !!     & Repeat last command \\
  !-n    & Repeat the command that is n commands back \\
  !n     & Repeat command n in the history \\
  !abc   & Run last command starting with abc \\
  !abc:p & Print last command starting with abc \\
  !\$    & Last argument of previous command \\
  !*     & All arguments of previous command \\
  \textasciicircum foo\textasciicircum bar\textasciicircum
         & Run previous command, replacing first instance of foo with bar \\
  !!:gs/foo/bar/ & Run previous command, replacing all instances of foo with bar \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{0.75in}p{3.5in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Who's there?}} \\
  w      & show who is logged in and system load \\
  whoami & display userid \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{0.75in}p{3.5in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Directory shorthand}} \\ 
  \textasciitilde      & Your home directory (``tilde'') \\
  \textasciitilde foo  & The home directory of user foo (``tilde foo'') \\
  .                    & The current working directory (``dot'') \\ 
  ..                   & The parent directory (``dot dot'') \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1in}p{3.5in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Directory operations}} \\ \hline
  pwd        & Print current working directory \\
  cd dir     & Change directory to dir \\
  cd ..      & Go up a directory level  \\
  ls         & List files \\ 
  pushd dir  & cd to dir; push dir onto directory stack \\
  popd       & Pop directory off stack; cd to new top directory \\
  dirs       & Show the directory stack \\
  mkdir dir  & Create directory dir \\ 
  rmdir dir  & Remove director dir (must be empty) \\ 
  rm -rf dir & Remove dir without asking for confirmation \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1in}p{3.5in}|} \hline
  \multicolumn{2}{|l|}{\textbf{ls options}} \\ \hline
  -a & Show all, including hidden files \\ 
  -R & Recursively list all subdirectories \\ 
  -t & Sort by last modified date \\
  -l & Long listing format \\
  -S & Sort by file size \\
  -h & Use familiar units for file sizes \\
  -1 & List one file per line \\
  -r & List in reverse alphabetical order \\
  -m & Comma separated output \\ \hline
\end{tabular}
\end{multicols}

\ifnum\timehh<10 0\fi\number\timehh:\ifnum\timemm<10 0\fi\number\timemm, \today

\begin{multicols*}{2}
\begin{tabular}{|p{1in}p{3.25in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Getting help}} \\ \hline
  man cmd  & Show manual page for cmd \\ 
  type cmd & Show command type (e.g., built-in, alias) \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1in}p{3.25in}|} \hline
  \multicolumn{2}{|l|}{\textbf{File operations}} \\ \hline
  touch file & Create file or update its last modified time \\
  cat file1 file2 & Concatenate files and print to terminal \\
  less file & View file \\
  file file & Get type of file \\
  cp file1 file2 & Copy file1 to file2 \\
  mv file1 file2 & Rename (move) file1 to file2 \\
  rm file & Delete (remove) file \\
  head file & Show first 10 lines of file \\
  head -n N file & Show first N lines of file \\
  tail file & Show last 10 lines of file \\
  tail -n N file & Show last N lines of file \\
  tail -F file & Print the last lines of file as it changes \\
  sdiff file1 file2 & Side-by-side comparison of two files \\
  uniq file & Print unique lines in file \\
  sort file & Sort lines in file alphabetically \\
  sort -n file & Sort lines in file numerically \\
  cut -f n file & Cut column (field) n from file \\
  wc file & word, line, character, and byte count \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1.5in}p{4in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Searching for files}} \\ \hline
  find . -name foo   & Recursively search the current directory for file foo \\
  find . -name \textbackslash *.py & Recursively search for filenames ending in .py \\ 
  find . -name foo\textbackslash*  & Recursively search for filenames beginning with foo \\ 
  find . -name \textbackslash*foo\textbackslash* & Recursively search for filenames containing foo \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1.5in}p{4in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Searching in files}} \\ \hline
  fgrep "string" foo    & Search for lines containing string in file foo \\
  fgrep -i "string" foo & Ignore case while searching \\
  fgrep -v "string" foo & Search for lines NOT containing string in file foo \\
  fgrep "string" -r .   & Recursively search files in current directory \\
  fgrep "string" -r dir & Recursively search files in dir subdirectories \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1.25in}p{3.5in}|} \hline
  \multicolumn{2}{|l|}{\textbf{I/O redirection in bash (cmd is a command)}} \\ \hline
  \multicolumn{2}{|l|}{stdin is standard input from terminal} \\
  \multicolumn{2}{|l|}{stdout is standard output from terminal} \\
  \multicolumn{2}{|l|}{stderr is error output to terminal} \\
  cmd \textless\ file & Read stdin for cmd from file \\ 
  cmd1 \textless(cmd2) & Execute cmd2; use its output as stdin to cmd1 \\
  cmd \textgreater\ file & Write stdout of cmd to file \\
  cmd \textgreater\ /dev/null & Discard stdout of cmd (write to bit bucket) \\
  cmd \textgreater\textgreater\ file & Append stdout to file \\
  cmd 2\textgreater\ file & Redirect stderr of cmd to file \\
  cmd 1\textgreater\&2 & Redirect stdout to same place as stderr \\
  cmd 2\textgreater\&1 & Redirect stderr to same place as stdout \\
  cmd \&\textgreater\ file & Redirect stdout and stderr of cmd to file \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1.25in}p{3.5in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Pipes}} \\ \hline
  cmd1 \textbar\ cmd2  & pipe stdout of cmd1 to stdin of cmd2 \\
  cmd1 \textbar\& cmd2 & pipe stderr of cmd1 to stdin of cmd2 \\ \hline
\end{tabular}

\medskip
\begin{tabular}{|p{1.25in}p{3.5in}|} \hline
  \multicolumn{2}{|l|}{\textbf{Bash shell variables}} \\ \hline
  echo \$VAR  & Print shell variable VAR \\
  VAR=value   & Set VAR to value \\
  env         & Print all shell variables \\
  printenv    & Print all shell variables \\ \hline
\end{tabular}
\end{multicols*}

% \begin{tabular}{|p{1.25in}p{4.25in}|} \hline
%   \multicolumn{2}{|l|}{\textbf{I/O redirection (cmd is a command)}} \\ \hline
%   cmd \textless\ file & Read the input of cmd from file \\ 
%   cmd1 \textless(cmd2) & Execute cmd2 and use its output as input to cmd1 \\
%   cmd \textgreater\ file & Write standard output of cmd to file \\
%   cmd \textgreater\ /dev/null & Discard standard output of cmd by writing to the bit bucket \\
%   cmd \textgreater\textgreater\ file & Append standard output to file \\
%   cmd 2\textgreater\ file & Redirect error output of cmd to file \\
%   cmd 1\textgreater\&2 & Redirect standard output to the same place as standard error \\
%   cmd 2\textgreater\&1 & Redirect standard error to the same place as standard output \\
%   cmd \&\textgreater\ file & Redirect all output of cmd to file \\ \hline
% \end{tabular}

% \medskip
% \begin{tabular}{|p{1.25in}p{4.25in}|} \hline
%   \multicolumn{2}{|l|}{\textbf{Pipes}} \\ \hline
%   cmd1 \textbar\ cmd2  & send standard output of cmd1 to standard input of cmd2 \\
%   cmd1 \textbar\& cmd2 & send standard error of cmd1 to standard input of cmd2 \\ \hline
% \end{tabular}

% \medskip
% \begin{tabular}{|p{1.25in}p{4.25in}|} \hline
%   \multicolumn{2}{|l|}{\textbf{Shell variables}} \\ \hline
%   echo \$VAR  & Print shell variable VAR \\
%   VAR=value   & Set VAR to value \\
%   env         & Print all shell variables \\
%   printenv    & Print all shell variables \\ \hline
% \end{tabular}
\end{document}
