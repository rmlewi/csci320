#include <stdio.h>
#include <unistd.h>

/* 
 * Illustration of buffered i/o and the fact that output need not appear
 * immediately after execution of a printf() call.
 */

int main(int argc, char **argv)
{
  unsigned int n = 10;

  printf("Sleeping for %u seconds...", n);
  
  /* Sleep for n seconds. */
  sleep(n);

  printf("done!\n");
  return 0;
}
