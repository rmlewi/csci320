#include <stdio.h>
#include <stdlib.h>

/* An illustration of programatically interacting with shell variables. */

int main(int argc, char **argv)
{
  if (argc == 1) {
    printf("Usage: %s shell_var1 shell_var2 ...\n", argv[0]);
    return 0;
  }
  
  /* Read a list of shell variables from the command line and print their values. */
  for (int i = 1; i < argc; i++) {
    printf("%s = %s\n", argv[i], getenv(argv[i]));
  }

  /* Create a new shell variable FOO with the value "42" (a string). */
  setenv("FOO", "42", 1);
  return 0;
}
