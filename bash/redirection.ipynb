{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1c7e450d",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">Bash Survival School</h1>\n",
    "<h1 style=\"text-align:center;\">I/O redirection</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0830e3c",
   "metadata": {},
   "source": [
    "<a href=\"navigation.ipynb\" style=\"float:left;\">Previous: Navigation</a> \n",
    "<a href=\"pipes.ipynb\" style=\"float:right;\">Next: Pipes</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8bcc386",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Redirecting standard output](#Redirecting-standard-output)\n",
    "    * [Playing it safe: the `noclobber` option](#Playing-it-safe:-the-noclobber-option)\n",
    "* [Redirecting standard error](#Redirecting-standard-error)\n",
    "    * [Redirecting only `stderr`](#Redirecting-only-stderr)\n",
    "    * [Redirecting `stderr` and `stdout` to the same file](#Redirecting-stderr-and-stdout-to-the-same-file)\n",
    "    * [Redirecting `stderr` and `stdout` to different files](#Redirecting-stderr-and-stdout-to-different-files)\n",
    "* [Redirecting standard input](#Redirecting-standard-input)\n",
    "* [`/dev/null` and `/dev/zero`](#bitbucket)\n",
    "* [Further reading](#Further-reading)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23e41585",
   "metadata": {},
   "source": [
    "In this notebook we make the acquaintance of the three standard i/o streams associated with programs:\n",
    "- **standard output**,\n",
    "- **standard error**, and \n",
    "- **standard input**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec455761",
   "metadata": {},
   "source": [
    "# Redirecting standard output\n",
    "\n",
    "Output written using the `printf()` function is written to **standard output** (**`stdout`** for short), which, in the case of programs started in an interactive shell (there are also non-interactive cells we will discuss later) this is the terminal where the program was initiated.\n",
    "\n",
    "However, there are situations where we might wish to capture the output written to standard output.  We can do this using i/o redirection.  We begin with an example."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33bae03c",
   "metadata": {},
   "source": [
    "Here is the `date` command, which writes the date to the screen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4f37e85",
   "metadata": {},
   "outputs": [],
   "source": [
    "date"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "854d47e1",
   "metadata": {},
   "source": [
    "Now suppose we wanted to store the result of `date` in a file (say, as a timestamp on data to follow).  We do so using the **redirect operator** `>`.  The following redirects `stdout` to the file `date.out`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b96df42",
   "metadata": {},
   "outputs": [],
   "source": [
    "date > date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5efa0ebf",
   "metadata": {},
   "source": [
    "Let's see what's in the file now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "461152a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5cec30ce",
   "metadata": {},
   "source": [
    "The next command is `fgrep` for finding fixed expressions in files.  The `-ir ../c/*.c` option says to search for the string `main` in the the files in the directory `../c` (one level up and over into the directory `c`) that end in `.c` (i.e., C language source files):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3dba694d",
   "metadata": {},
   "outputs": [],
   "source": [
    "fgrep main -i ../c/*.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ee80ca6",
   "metadata": {},
   "source": [
    "Now let's redirect the output from the call to `fgrep` to `date.out`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2ea73b0c",
   "metadata": {},
   "outputs": [],
   "source": [
    "fgrep main -i ../c/*.c > date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d271d610",
   "metadata": {},
   "source": [
    "Now what is in our file?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e973777",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "128b8ca9",
   "metadata": {},
   "source": [
    "The time and date timestamp are missing &ndash; where did they go?!\n",
    "\n",
    "The redirection operator `>` has the following behavior:\n",
    "* If the target file does not exist, **the redirection will create it and then write the output from `stdout` to the file**.\n",
    "* If the target file does exist, **the redirection will overwrite the file with the output from `stdout`**.\n",
    "\n",
    "If we want to append to an existing file rather than overwrite it we use the redirection operator `>>`.  The redirection operator `>>` has the following behavior:\n",
    "* If the target file does not exist, **the redirection will create it and then write the output from `stdout` to the file**.\n",
    "* If the target file does exist, **the redirection will append the output from `stdout`**.\n",
    "\n",
    "Let's try `>>`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7be5861e",
   "metadata": {},
   "outputs": [],
   "source": [
    "date > date.out\n",
    "fgrep main -ir ../c/*.c >> date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdd6f6a2",
   "metadata": {},
   "source": [
    "Now we have saved the output of both `date` and `fgrep`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2a2b3bff",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "badb6eb0",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> 🐞🐞Because of the way redirection is done, the following will cause all the data in the file `date.out` to be destroyed!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "684b45f6",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat date.out > date.out\n",
    "ls -ls date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f64b565",
   "metadata": {},
   "source": [
    "You cannot get around this using `>>`.  In Linux, the command\n",
    "```\n",
    "cat date.out >> date.out\n",
    "```\n",
    "fails with a warning that the input and output files are the same, while in Unix on a Mac it goes into an infinite loop!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75e8ac03",
   "metadata": {},
   "source": [
    "## Playing it safe: the `noclobber` option\n",
    "\n",
    "If you are worred about accidentally using `>` when you meant to use `>>` you can set the `noclobber` option for your shell with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8501260f",
   "metadata": {},
   "outputs": [],
   "source": [
    "set -o noclobber"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05ee653e",
   "metadata": {},
   "source": [
    "This prevents redirection from overwriting existing files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7a9a1d4",
   "metadata": {},
   "outputs": [],
   "source": [
    "date > date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de17cf08",
   "metadata": {},
   "source": [
    "If you turn on `noclobber` in an interactive session it will be in effect for that shell for the rest of the session.  If you want to make it the default for all shells, you should place the `set` command in your `.bashrc` file so it is executed when a shell starts up.\n",
    "\n",
    "You can turn `noclobber` off with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2d007d9",
   "metadata": {},
   "outputs": [],
   "source": [
    "set +o noclobber"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7de64736",
   "metadata": {},
   "outputs": [],
   "source": [
    "date > fgrep.out\n",
    "cat fgrep.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23f9076b",
   "metadata": {},
   "source": [
    "If `noclobber` is in effect, you can still use redirection to overwrite an existing file by using the `>|` operator.  Let's turn `noclobber` on again and try to overwrite an existing file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d727d68",
   "metadata": {},
   "outputs": [],
   "source": [
    "set -o noclobber\n",
    "date > date.out "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8f13b7a",
   "metadata": {},
   "source": [
    "Now let's try the exigent redirection operator `>|`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d67f597",
   "metadata": {},
   "outputs": [],
   "source": [
    "date >| date.out\n",
    "cat date.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05340a72",
   "metadata": {},
   "source": [
    "The old date is overwritten with the new one."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddc48883",
   "metadata": {},
   "source": [
    "The potentially destructive behavior of `>` might seem a strange design decision to you.  But remember:\n",
    "<blockquote>\n",
    "Unix was not designed to stop you from doing stupid things, because that would also stop you from doing clever   things. <br/>\n",
    "&ndash; Doug Gwyn\n",
    "</blockquote>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dff305a3",
   "metadata": {},
   "source": [
    "# Redirecting standard error\n",
    "\n",
    "There is another output unit associated with a program called **standard error** (**`stderr`** for short).\n",
    "\n",
    "Consider the following C program.  It prints a string to `stdout`, pauses, and then prints another string to `stdout`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7c3e844",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n sleep.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b9171e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc sleep.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aff12651",
   "metadata": {},
   "source": [
    "Now run the executable we just built.  Be patient, since the program sleeps for 10 seconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "005432a0",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f8aa8fb",
   "metadata": {},
   "source": [
    "Notice that the output of the first print statement appears **after** the program has slept for 10 seconds.  This is because standard output uses **buffered output**.  In buffered output the output is held in a **buffer** (chunk of memory) until the buffer fills up.  It then writes the buffer to the screen (writing an output buffer is called **flushing** or **syncing** it).  This is more efficent than writing to the output device after every print statement, since then the very fast CPU would be waiting on the much, much slower output device to finish the write.  Writing to a buffer in memory is much more efficient than writing to an output device.\n",
    "\n",
    "However, this means that if the program crashes or is interrupted, buffered output might be lost before it is written.  In the event of a crash or infinite loop, we might lost messages that would help us figure out what went wrong.\n",
    "\n",
    "This is where standard error comes in.  Standard error uses **unbuffered output** &ndash; characters are written as soon as the print statement executes.  It incurs more overhead than standard error, so it should not be used for writing massive volumes of data; its use should be limited to logging error or other important information about the program state.\n",
    "\n",
    "By default output written to `stderr` also goes to the terminal.\n",
    "\n",
    "Let's start the illustration of redirecting `stderr` by turning off `noclobber`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e49cbe92",
   "metadata": {},
   "outputs": [],
   "source": [
    "set +o noclobber"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ea270ca",
   "metadata": {},
   "source": [
    "## Redirecting only `stderr`\n",
    "\n",
    "If we try to `ls` (list) a file that does not exist, `ls` writes a messages to standard error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9274d2bd",
   "metadata": {},
   "outputs": [],
   "source": [
    "ls -lsat redirection.ipynb foobar.cpp start_here.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad740e67",
   "metadata": {},
   "source": [
    "We can redirect `stderr` to a file with the operator `2>`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90a96203",
   "metadata": {},
   "outputs": [],
   "source": [
    "ls -lsat redirection.ipynb foobar.cpp start_here.ipynb 2> errors.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea0a132e",
   "metadata": {},
   "source": [
    "This time only the output to `stderr` appears at the terminal.\n",
    "\n",
    "Each of the three standard i/o streams has a number associated with it:\n",
    "* standard input: 0\n",
    "* standard output: 1\n",
    "* standard error: 2\n",
    "\n",
    "The `2>` says to redirect stream 2, `stderr`.\n",
    "\n",
    "Now let's look at the redirected output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d988f7a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat errors.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3687bbf0",
   "metadata": {},
   "source": [
    "This is the warning message from `ls`. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c120deb6",
   "metadata": {},
   "source": [
    "The output file `foo.log` contains only information about the files that exist:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af9979ce",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat foo.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37dabfc0",
   "metadata": {},
   "source": [
    "## Redirecting `stderr` and `stdout` to the same file\n",
    "\n",
    "We can redirect both `stderr` and `stdout` to the same file with the `&>` operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85ab9895",
   "metadata": {},
   "outputs": [],
   "source": [
    "ls -lsat redirection.ipynb foobar.cpp start_here.ipynb &> foo.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab5eddb7",
   "metadata": {},
   "source": [
    "The output from both streams appears in the target file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f170a1d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat foo.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ab7d4f4",
   "metadata": {},
   "source": [
    "## Redirecting `stderr` and `stdout` to different files\n",
    "\n",
    "We can also redirect `stderr` and `stdout` using the `1>` (for redirecting `stdout`) and `2>` (for redirecting `stderr`) operators.  Here we send `stdout` to `ls.out` and `stderr` to `errors.log`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab1790c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "ls -lsat redirection.ipynb foobar.cpp start_here.ipynb 1> ls.out 2> errors.log"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8467ddc",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat ls.out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "901d575a",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat errors.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e525605f",
   "metadata": {},
   "source": [
    "# Redirecting standard input\n",
    "\n",
    "**Standard input** (**`stdin`** for short) is the standard input stream, typically input entered at the command line.  We can redirect standard input to read from other sources such as files, however."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7635a860",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat muskrat.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ed1b3e2",
   "metadata": {},
   "source": [
    "Now suppose we want to convert all the text in this file to uppercase using the `tr` command.  Normally `tr` expects its input to be typed at the command line:\n",
    "```\n",
    "tr a-z A-Z  # The command.\n",
    "boo!  # Input entered by me.\n",
    "BOO!  # Translation.\n",
    "foo!  # Input entered by me.\n",
    "FOO!  # Translation, and so on.\n",
    "bar!\n",
    "BAR!\n",
    "```\n",
    "\n",
    "However, we want to translate a file, not input entered at the command line.\n",
    "\n",
    "This is where we can redirect `stdin`.  This tells `stdin` to read the contents of a file and treat it as if it were typed at the command line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13dfff2c",
   "metadata": {},
   "outputs": [],
   "source": [
    "tr a-z A-Z < muskrat.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aca4e583",
   "metadata": {},
   "source": [
    "Input redirection is less common than output redirection because of our next topic: pipes and pipelines.  The most  common use I have found for it is automating the use of programs that expect interactive input."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "86f32a2a",
   "metadata": {},
   "source": [
    "# `/dev/null` and `/dev/zero`\n",
    "\n",
    "There is a special file named `/dev/null`.  It functions as the [**bit bucket**](https://en.wikipedia.org/wiki/Null_device); anything written to it is discarded.  You can use it to redirect (and ignore) command-line output from particularly chatty programs.  It also figures in [really geeky humor](https://en.wikipedia.org/wiki/Null_device#References_in_computer_culture)).  If you read from `/dev/null` you will immediately get an end-of-file.  \n",
    "\n",
    "There is a related file named `/dev/zero`. It also ignores anything written to iit.  Reading from `/dev/zero` returns bytes that are zero (i.e., the ASCII null character).  This functions as a file of whatever size we wish that we can use in debugging and testing, for instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7abb1e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "date > /dev/null"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c56e00db",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat /dev/null"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca110ce0",
   "metadata": {},
   "source": [
    "# Further reading\n",
    "\n",
    "I/O redirection can be quite subtle and complicated.  You can find a thorough discussion [here](https://www.gnu.org/software/bash/manual/html_node/Redirections.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02a65d2b",
   "metadata": {},
   "source": [
    "<a href=\"navigation.ipynb\" style=\"float:left;\">Previous: Navigation</a> \n",
    "<a href=\"pipes.ipynb\" style=\"float:right;\">Next: Pipes</a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
