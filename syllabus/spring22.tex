\documentclass{article}

\usepackage[text={6.5in,9in}]{geometry}

\usepackage{pdflscape}
\usepackage{xspace}
\usepackage{graphicx}

\usepackage{hyperref}
\hypersetup{colorlinks=true,linkcolor=blue,citecolor=black,urlcolor=blue}

% Compute the time in hours and minutes; make new variables \timehh and \timemm.
\newcount\timehh\newcount\timemm
\timehh=\time 
\divide\timehh by 60 \timemm=\time
\count255=\timehh\multiply\count255 by -60 \advance\timemm by \count255

% https://tex.stackexchange.com/questions/30720/footnote-without-a-marker
\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

% C++
\usepackage{xspace}
\usepackage{relsize}
\usepackage{lipsum}

%c from texinfo.tex
\def\ifmonospace{\ifdim\fontdimen3\font=0pt }
\def\Cpp{%
  \ifmonospace%
  C++%
  \else%
  %C\kern-.1667em\raise.30ex\hbox{\smaller{++}}%
  C\kern-0.05em\raise.30ex\hbox{\smaller{++}}%
  \fi%
  \xspace}%\spacefactor1000}

\def\ppC{%
  \ifmonospace%
  ++C%
  \else%
  %C\kern-.1667em\raise.30ex\hbox{\smaller{++}}%
  \raise.30ex\hbox{\smaller{++}}\kern-0.05em C%
  \fi%
  \xspace}%\spacefactor1000}
%---------------------------------------------------------------------------------------------------------------
\begin{document}
\begin{center}
  \textbf{CSCI 320, Life Beyond Python, Spring 2022}
\end{center}
%---------------------------------------------------------------------------------------------------------------
\paragraph*{About this course}
This course is intended to initiate you into the mysteries of working at the command line in Unix/Linux as well
as introduce you to the C, \Cpp, and Java programming languages.

The first language we will look at is C, since much of the syntax of \Cpp and Java is the same as C.  We will
then move onto \Cpp, and conclude with Java.  These will be quick looks at the languages and also how to build
and run programs in them.  As we go we will learn about Unix/Linux command line tools and how to work at the
command line.

There will be a lot of material coming at you in this class.  \textbf{Don't panic!}  I do not expect you to
learn all of this material by heart the first time you see it.  Mastery will come with practice.

We will use the course Blackboard site for course organization and quizzes.  The Blackboard site also has a
link to Gradescope, which we will use to test and grade programming assignments.
%---------------------------------------------------------------------------------------------------------------
\paragraph*{Instructor}
\begin{tabular}[t]{@{}l}
  Robert Michael Lewis \\
  McGlothlin-Street Hall 329 \\
  \texttt{rmlewi@wm.edu} \\
\end{tabular}

\bigskip
The best way to communicate with me is to see me in person.  Be warned that the department is hiring and until
spring break I will be meeting with faculty candidates shortly after class at 5:00 pm, so I won't be able to
talk then.  The Blackboard site contains a link to Microsoft Bookings where you can make an appointment to meet
either in-person or via Zoom.

Email is a terrible way to try to communicate with me as I already receive an unmanageable amount of email in my
role as department chair.  I will try to respond to email you send within a fortnight, but no guarantees.  Email
that requires only a short answer (e.g., ``yea'', ``nay'', ``42'') is most likely to receive a prompt reply.
%---------------------------------------------------------------------------------------------------------------
\paragraph*{CS account---very important!}
You will need an account on the CS system for this class.  If you are a declared major your account should
persist from semester to semester but you should check to make sure.  If you do not have a CS account you can
request one at \href{https://accounts.cs.wm.edu}{accounts.cs.wm.edu}.  This is not a fully automated system
since we need to check that you are actually enrolled in a CS class so it will take a day or two for the account
to be created.
%---------------------------------------------------------------------------------------------------------------
\paragraph*{Grading}
The grade for this course will be based on the following components. 

\textbf{Quizzes, 10\%.}  These will mostly test your facility with Linux and familiarity with its commands.
There may also be questions about the programming languages we are studying, as well as random questions to
further your liberal education.

\textbf{Programming projects, 80\%.}  These will be graded by a Gradescope autograder.  There will be no limit 
on the number of times you can submit a project for grading and no penalty for late project submissions.  This
policy may seem unusally lax, but I would like for it to be possible for you to try and fail without penalty,
and I want to encourage you to continue to work on the projects until you have them working.

\textbf{Final exam, 10\%.} Our final exam period is 
by \href{https://www.wm.edu/offices/registrar/calendarsandexams/examschedules/spring22exam/index.php}%
{0900 to 1200 on Monday, May 9, 2022}.  I do not have the authority to change the exam date without
authorization from the Dean of Undergraduate Studies or the Dean of Students, depending on the situation: 
\begin{center}
  \href{http://www.wm.edu/offices/registrar/calendarsandexams/examschedules/rescheduledeferexam/}%
  {http://www.wm.edu/offices/registrar/calendarsandexams/examschedules/rescheduledeferexam/}%
\end{center}
Petitions to the Dean of Undergraduate Studies are due by 5:00 pm on the last day of the classes.

Your final grade for the semester will be determined using the following scale.
If $S$ is your total score for the semester, then
\begin{center}
  \begin{tabular}{|r|l|} \hline
              $S \geq 90\%$ & A- to A  \\ \hline
    $80\% \leq S < 90\%$    & B- to B+ \\ \hline
    $70\% \leq S < 80\%$    & C- to C+ \\ \hline
    $60\% \leq S < 70\%$    & D- to D+ \\ \hline
    $S < 60\%$              & F \\ \hline
  \end{tabular}
\end{center}
\noindent I reserve the right to relax these standards, but I will not make them more stringent. 
%---------------------------------------------------------------------------------------------------------------
\paragraph*{Important dates}
\begin{itemize}
\item Add/drop deadline: Friday, February 4, 2022.
\item Withdrawal deadline: Monday, March 28, 2022.
\item Final examination date: Monday, May 9, 2022.
\end{itemize}
%---------------------------------------------------------------------------------------------------------------
\paragraph*{Academic accommodations of disabilities\footnote{This section is from Student Accessibility
Services.}}
``William \& Mary accommodates students with disabilities in accordance with federal laws and university
policy. Any student who feels they may need an accommodation based on the impact of a learning, psychiatric,
physical, or chronic health diagnosis should contact Student Accessibility Services staff at 757-221-2512 or at
texttt{sas@wm.edu} to determine if accommodations are warranted and to obtain an official letter of
accommodation. For more information, please see \href{http://www.wm.edu/sas}{http://www.wm.edu/sas}.''

Once I have received an accommodation letter, it is your responsibility to discuss accommodations and
accommodation logistics with me.
%---------------------------------------------------------------------------------------------------------------
\paragraph*{Fun with coronavirus\footnote{This is section is from the Dean of Arts \& Sciences.}}
``This semester, the world will enter its third year with COVID.  As we experience a fifth surge of pandemic with
the highly transmissible omicron variant, it is reasonable to expect significant levels of infection at W\&M. As
an academic community based on faculty and students convening, Spring 2022 courses will largely consist of
in-person instruction.  All of us will follow W\&M requirements---vaccinations and boosters, indoor masking, as
well as quarantine and isolation when ill.  That last is really important: for those who have tested positive,
W\&M’s requirements must be fulfilled before class can be attended in person, and, out of an abundance of
caution, anyone with symptoms consistent with COVID---even if they don’t have a positive test---should not come
to class.  

Please note that testing positive for COVID or any other temporary illness is not considered a disability as
defined by ADA guidelines and is not under the purview of W\&M’s Student Accessibility Services (SAS). Thus, any
questions should be addressed to the instructor.

Consistent with W\&M’s belief that learning is most effective when the instructor and students convene, our
course this semester is scheduled for in-person instruction.  That said, we are still dealing with a pandemic,
even as it ebbs toward an endemic.  In this setting, we need to have a way to communicate when students or the
instructor cannot be in person.  As soon as a student knows they will not be able to attend class in person
(either because of having tested positive, having symptoms consistent with COVID, or other health matters),
please email the instructor.  In that case, the instructor will activate our mode of accommodating absences for
your situation.  Since this is a very challenging time with the potential for quite complicated comings and
goings, we need to operate on the basis of a trustful relationship; please try your very best not to miss
classes for non-health related reasons.''
%---------------------------------------------------------------------------------------------------------------
\paragraph*{Dealing with absences}
The course materials will be available online, so you should be able to make up missing one or two classes.  The
liberal late policy for programming assignments will make it possible for you to catch up without penalty.  If
students start falling like tenpins or for long periods of time I may hold special office hours to summarize the
lectures.

There is also the possibility that I will be stricken and unable to lecture in person.  If I am fit enough we
will meet remotely via Zoom.  There is a link on the Blackboard site to a Zoom site if we have to go remote.
%--------------------------------------------------------------------------------------------------------------- 
\paragraph*{The Honor Code}
CS has sent a depressingly large number of cheating cases to the Honor Council in recent years.
\textit{Please, please, please---do not cheat.}  All work in this course is subject to the 
\href{https://www.wm.edu/offices/deanofstudents/services/communityvalues/studenthandbook/honor_system/}
{College's Honor Code}.
Cheating cases involving projects in CS courses are typically
\href{https://www.wm.edu/offices/deanofstudents/services/communityvalues/studenthandbook/honor_system/appendix_I/index.php}
{Level III violations of the Honor Code}.  This means that they \textit{must} be adjudicated by the Honor Council, 
and may result in one or more of the following: permanent dismissal from the College, suspension from the College,
a failing grade, a grade reduction in the course, and having your nose torn off with red-hot pincers.

You are free to talk to anyone at William \& Mary about basic questions concerning Python, Linux, C, \Cpp, Java
editors, and the like.  For instance, you may ask about syntax, e.g., what does
\begin{center}
  \texttt{auto z4 = [=,y]()->int \{return (y ? 1 : 2);\}}\footnote{You may be alarmed to learn that this is
  actually valid \Cpp.}
\end{center}
mean?  You may ask others about the standard library functions and classes, errors and warnings, and you may
also search the interwebs for answers about these matters.  You may also search for answers to generic
questions, e.g., how do I allocate an array in C?

You may discuss discuss the details of the programming projects only with others currently enrolled in the class
and the instructor.  Please observe an ``empty hands'' policy when you discuss a programming assignment with your
classmates.  You are free to discuss any aspect of the assignment, but you must leave the meeting without any
written or electronic record of these discussions.  This includes photographs.

Searching the web for a turnkey solution to a programming project will be considered a violation of the Honor
Code.  This includes downloading a potential solution for ``reference''.

Actively soliciting code from another student, whether or not you submit it as your answer for a project, will
also be considered a violation of the Honor Code.  If, in their enthusiasm to help, someone sends you
unsolicited code, you should immediately avert your gaze, delete it, and remind your would-be benefactor that
sending code to others is a no-no.
\end{document}
