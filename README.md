# CSCI 320: Life Beyond Python

Materials from my Life Beyond Python course, a crash course in bash, C, C++, and Java.

Copyright © 2022 Robert Michael Lewis.  All rights reserved.

Permission is granted to copy, distribute, and/or modify these files under the terms of the Creative Commons Attribution-NonCommercial 3.0 Unported License, which is available at http://creativecommons.org/licenses/by-nc/3.0/.
