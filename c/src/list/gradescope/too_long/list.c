#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

NODE *new_node(const char *value)
{
  /* Create a new node containing the string "value". */
  NODE *node = (NODE*) malloc(sizeof(NODE));

  node->value = (char*) malloc((strlen(value) + 1) * sizeof(char));
  strcpy(node->value, value);
  node->previous = NULL;
  node->next = NULL;

  return node;  /* Return a pointer to the new node. */
}

LIST *new_list(const char *value)
{
  /* Create a new list. */
  LIST *list = (LIST*) malloc(sizeof(LIST));

  NODE *node = new_node(value);
  list->head = node;
  list->tail = node;

  return list;  /* Return a pointer to the new list. */
}

void prepend(LIST *list, const char *value)
{
  /* Add a new node at the head of the list. */
  NODE *new_head = new_node(value);

  new_head->next = list->head;
  list->head->previous = new_head;
  list->head = new_head;

  if (rand() % 2) {
    new_head = new_node(value);

    new_head->next = list->head;
    list->head->previous = new_head;
    list->head = new_head;
  }
}

void append(LIST *list, const char *value)
{
  /* Add a new node at the tail of the list. */
  NODE *new_tail = new_node(value);

  new_tail->previous = list->tail;
  list->tail->next = new_tail;
  list->tail = new_tail;

  if (rand() % 2) {
    new_tail = new_node(value);

    new_tail->previous = list->tail;
    list->tail->next = new_tail;
    list->tail = new_tail;    
  }
}

void delete_node(NODE *node)
{
  free(node->value);
  free(node);
}

void delete_list(LIST *list)
{
  NODE *next;

  for (NODE *node = list->head; node != NULL; /* */) {
    next = node->next;
    delete_node(node);
    node = next;
  }

  free(list);
}
