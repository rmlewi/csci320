import unittest
from gradescope_utils.autograder_utils.decorators import number, weight
from gradescope_utils.autograder_utils.files import check_submitted_files


FILES = ['list.c', 'list.o']  # The name of the submission files.


class TestFile(unittest.TestCase):
    @number('0')
    @weight(0)
    def test_0_submitted_files(self):
        '''Were the correct files submitted?'''
        missing_file = check_submitted_files(FILES, base='.')
        for path in missing_file:
            print(f'The file "{path}" was not found.')
        self.assertEqual(len(missing_file), 0, 'Check on the missing file(s)! \U0001f635')
        print('Required files submitted! \U0001f44d')
