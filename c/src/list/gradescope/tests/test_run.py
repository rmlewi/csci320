import os
import subprocess
import unittest

from gradescope_utils.autograder_utils.decorators import number, weight

class Test_Dot(unittest.TestCase):
    '''Build and runtime tests.'''

    @number('1')
    @weight(0)
    def test_1_obj(self):
        '''Can we build an executable using the object file list.o?'''
        # We use gnu11 rather than c11 since drand48() is Posix, not standard C, and is missing from c11.
        cmd = ['gcc', '-g', '-std=gnu11', '-Wall', '-pedantic', '-Werror', 'rml_list.c', 'list.o']  # No spaces in args!
        print('Building an executable with')
        print(f'   {" ".join(cmd)}')

        # test = subprocess.run(cmd, capture_output=True)  # capture_output is not in Gradescope's Python 3.6.
        test = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if test.returncode != 0:
            print(f'Nonzero return code: {test.returncode} indicating errors. \U0001f635')
            print('I could not build the program!')
            print('Did you submit a non-Linux object file?', end='\n\n')
            print('Here is the output from stderr:')
            print(test.stderr.decode('utf-8'), end='\n\n')
            print('Here is the output from stdout:')
            print(test.stdout.decode('utf-8'))
        else:
            print('...most excellent! \U0001f600 \U0001F389 \U0001F38A \U0001F387')
        self.assertTrue(test.returncode == 0, f'Non-zero return code: {test.returncode}.')


    @number('2')
    @weight(10)
    def test_2_obj(self):
        '''Does the executable built from list.o work correctly?'''
        self.assertTrue(os.path.isfile('a.out'), 'No a.out found.')

        cmd = ['./a.out']  # No spaces in args!
        print(f'Executing {" ".join(cmd)}...')

        try:
            timeout = 5
            test = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=timeout)
        except subprocess.TimeoutExpired:
            self.assertTrue(False, f'Executable timed out after {timeout} seconds. \U0001f635')

        if test.returncode != 0:
            print(f'Nonzero return code: {test.returncode} indicating errors. \U0001f635')
            print('Here is the output from stdout:')
            print(test.stdout.decode('utf-8'))
            print('Here is the output from stderr:')
            print(test.stderr.decode('utf-8'), end='\n\n')
        else:
            print('...no errors detected---most excellent! \U0001f600 \U0001F389 \U0001F38A \U0001F387')
        self.assertTrue(test.returncode == 0, f'Nonzero return code: {test.returncode}. \U0001f635')


    @number('3')
    @weight(0)
    def test_3_obj(self):
        '''Can we build an executable using the object file list.c?'''
        # We use gnu11 rather than c11 since drand48() is Posix, not standard C, and is missing from c11.
        if os.path.isfile('a.out'):
            os.unlink('a.out')
        cmd = ['gcc', '-g', '-std=gnu11', '-Wall', '-pedantic', '-Werror', 'rml_list.c', 'list.c']  # No spaces in args!
        print('Building an executable with')
        print(f'   {" ".join(cmd)}')

        test = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if test.returncode != 0:
            print(f'Nonzero return code: {test.returncode} indicating errors. \U0001f635')
            print('I could not build the program!')
            print('Here is the output from stderr:')
            print(test.stderr.decode('utf-8'), end='\n\n')
            print('Here is the output from stdout:')
            print(test.stdout.decode('utf-8'))
        else:
            print('...most excellent! \U0001f600 \U0001F389 \U0001F38A \U0001F387')
        self.assertTrue(test.returncode == 0, f'Non-zero return code: {test.returncode}.')


    @number('4')
    @weight(10)
    def test_4_obj(self):
        '''Does the executable built from list.c work correctly?'''
        self.assertTrue(os.path.isfile('a.out'), 'No a.out found.')

        cmd = ['./a.out']  # No spaces in args!
        print(f'Executing {" ".join(cmd)}...')

        try:
            timeout = 5
            test = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=timeout)
        except subprocess.TimeoutExpired:
            self.assertTrue(False, f'Executable timed out after {timeout} seconds. \U0001f635')

        if test.returncode != 0:
            print(f'Nonzero return code: {test.returncode} indicating errors. \U0001f635')
            print('Here is the output from stdout:')
            print(test.stdout.decode('utf-8'))
            print('Here is the output from stderr:')
            print(test.stderr.decode('utf-8'), end='\n\n')
        else:
            print('...no errors detected---most excellent! \U0001f600 \U0001F389 \U0001F38A \U0001F387')
        self.assertTrue(test.returncode == 0, f'Nonzero return code: {test.returncode}. \U0001f635')


    @number('5')
    @weight(30)
    def test_5_obj(self):
        '''Does the executable built from list.c have memory problems?'''
        self.assertTrue(os.path.isfile('a.out'), 'No a.out found.')

        cmd = ['valgrind', '--leak-check=full', '--error-exitcode=42', './a.out']  # No spaces in args!
        print(f'Executing {" ".join(cmd)}...')

        try:
            timeout = 5
            test = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=timeout)
        except subprocess.TimeoutExpired:
            self.assertTrue(False, f'Executable timed out after {timeout} seconds. \U0001f635')

        if test.returncode != 0:
            print(f'Nonzero return code: {test.returncode} indicating memory errors. \U0001f635', end='\n\n')
#            print('This is an outward and visible sign of an inward and spiritual state.', end='\n\n')
            print('Here is the output from stderr:')
            print(test.stderr.decode('utf-8'), end='\n\n')
            print('Here is the output from stdout:')
            print(test.stdout.decode('utf-8'))
        else:
            print('...no errors detected---most excellent! \U0001f600 \U0001F389 \U0001F38A \U0001F387')
        self.assertTrue(test.returncode == 0, f'Nonzero return code: {test.returncode}. \U0001f635')
