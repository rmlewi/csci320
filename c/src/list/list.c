#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

NODE *rml_new_node(const char *value)
{
  /* Create a new node containing the string "value". */
  NODE *node = (NODE*) malloc(sizeof(NODE));

  node->value = (char*) malloc((strlen(value) + 1) * sizeof(char));
  strcpy(node->value, value);
  node->previous = NULL;
  node->next = NULL;

  return node;  /* Return a pointer to the new node. */
}

LIST *rml_new_list(const char *value)
{
  /* Create a new list. */
  LIST *list = (LIST*) malloc(sizeof(LIST));

  NODE *node = rml_new_node(value);
  list->head = node;
  list->tail = node;

  return list;  /* Return a pointer to the new list. */
}

void rml_prepend(LIST *list, const char *value)
{
  /* Add a new node at the head of the list. */
  NODE *new_head = rml_new_node(value);

  new_head->next = list->head;
  list->head->previous = new_head;
  list->head = new_head;
}

void rml_append(LIST *list, const char *value)
{
  /* Add a new node at the tail of the list. */
  NODE *new_tail = rml_new_node(value);

  new_tail->previous = list->tail;
  list->tail->next = new_tail;
  list->tail = new_tail;
}

void rml_delete_node(NODE *node)
{
  free(node->value);
  free(node);
}

void rml_delete_list(LIST *list)
{
  NODE *next;

  for (NODE *node = list->head; node != NULL; /* */) {
    next = node->next;
    rml_delete_node(node);
    node = next;
  }

  free(list);
}

void gen_random(char s[], const int len)
{
  static const char alphanum[] = "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
      s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }
    s[len] = '\0';
}

void print_node(const NODE *node)
{
  printf("value: %s, ", node->value);
  printf("previous: %p, ", (void*) node->previous);
  printf("next:     %p\n", (void*) node->next);  
}

void print_list(const LIST *list)
{
  for (NODE *node = list->head; node != NULL; node = node->next) {
    printf("%s", node->value);
    if (node->next != NULL) {
      printf(", ");
    }
    else {
      printf("\n");
    }
  }
}

int main(int argc, char **argv)
{
  char word[42];
  LIST *my_list = NULL;
  LIST *their_list = NULL;
  int retcd = 0;
  int num_words = 10;

  for (int len = 1; len <= 5; len++) {
    my_list = rml_new_list("0");
    their_list = new_list("0");    
    for (int i = 0; i < num_words; i++) {
      gen_random(word, len);
      rml_prepend(my_list, word);
      rml_append(my_list, word);
      prepend(their_list, word);
      append(their_list, word);
    }

    NODE *my_node = my_list->head;
    NODE *their_node = their_list->head;	
    do {
      if (strcmp(my_node->value, their_node->value)) {
	retcd = 32;
      }
      my_node = my_node->next;
      their_node = their_node->next;
    } while ((my_node != NULL) && (their_node != NULL));

    if ((my_node == NULL) && (their_node != NULL)) {
      printf("Your list is longer than my list.\n");
      retcd = 32;
    }
    else if ((my_node != NULL) && (their_node == NULL)) {
      printf("Your list is shorter than my list.\n");
      retcd = 32;
    }
    
    if (retcd) {
      printf("my list:   ");
      print_list(my_list);

      printf("your list: ");
      print_list(their_list);
    }

    rml_delete_list(my_list);
    delete_list(their_list);
  }
  
  return retcd;
}

#if defined(RML)
int main(int argc, char **argv)
{
  if (argc == 1) {
    printf("Usage: %s word1 word2 ...\n", argv[0]);
    return 0;
  }
	   
  LIST *my_list = rml_new_list("start");

  for (int i = 1; i < argc; i++) {
    rml_prepend(my_list, argv[i]);
    rml_append(my_list, argv[i]);
  }

  printf("my list: ");
  print_list(my_list);
  printf("\n");

  rml_delete_list(my_list);
  
  return 0;
}
#endif
