#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node {
  char *value;
  struct node *previous;
  struct node *next;
} NODE;

typedef struct list {
  NODE *head;
  NODE *tail;
} LIST;

LIST *new_list(void)
{
  LIST *list = (LIST*) malloc(sizeof(LIST));
  list->head = NULL;
  list->tail = NULL;
  return list;
}

NODE *new_node(char *value)
{
  NODE *node = (NODE*) malloc(sizeof(NODE));

  node->value = (char*) malloc((strlen(value) + 1) * sizeof(char));
  strcpy(node->value, value);
  node->previous = NULL;
  node->next = NULL;
  
  return node; 
}

void prepend(LIST *list, char *value)
{
  NODE *new_head = new_node(value);

  /* If list->head == NULL then new_head is the first node added to the list. */
  if (list->head == NULL) {
    list->head = new_head;
  }
  else {
    new_head->next = list->head;
    list->head->previous = new_head;
  }
}

void append(LIST *list, char *value)
{
  NODE *new_tail = new_node(value);
  /* If list->head == NULL then new_head is the first node added to the list. */
  if (list->head == NULL) {
    list->head = new_tail;
  }
  else {
    new_tail->previous = list->tail;
    list->tail = new_tail;
  }
}

void print_node(NODE *node)
{
  printf("value: %s\n", node->value);
  printf("previous: %p\n", (void*) node->previous);
  printf("next:     %p\n", (void*) node->next);  
}

void print_list(LIST *list)
{
  for (NODE *node = list->head; node != NULL; node = node->next) {
    printf("%s\n", node->value);
  }
}

int main(int argc, char **argv)
{
  char *words[] = {"fee", "fie", "foe", "fum"};
  LIST *list = new_list();
  
  for (int i = 0; i < 4; i++) {
    append(list, words[i]);
  }
  print_list(list);
  return 0;  
}
