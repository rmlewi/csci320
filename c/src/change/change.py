def change(amount):
    '''The optimal change making algorithm.'''
    if (amount < 0):
        return None

    QUARTERS = 25
    DIMES    = 10
    NICKELS  =  5
    PENNIES  =  1

    num_quarters, amount = divmod(amount, QUARTERS)
    num_dimes,    amount = divmod(amount, DIMES)
    num_nickels,  amount = divmod(amount, NICKELS)
    num_pennies,  amount = divmod(amount, PENNIES)

    return num_quarters, num_dimes, num_nickels, num_pennies
