#include <stdio.h>
#include <stdlib.h>

int *change(int amount)
{
  if (amount < 0) return NULL;
  
  const int denom[] = {25, 10, 5, 1};
  int *result = (int*) malloc(4 * sizeof(int));  
  
  for (int k = 0; k < 4; k++) {
    result[k] = amount/denom[k];
    amount %= denom[k];
  }

  return result;
}
