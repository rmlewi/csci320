#!/usr/bin/env bash

# uname trickery to avoid error below.
mv /bin/uname /bin/uname.orig
cat > /bin/uname <<EOF
if [ "\$1" = "-r" ]
then
    echo '4.9.250'
    exit
else
    /bin/uname.orig "\$@"
fi
EOF
chmod 755 /bin/uname
cat /bin/uname

# This script installs Python and gcc.

apt-get install -y python3
apt-get install -y gcc

# As of March 6, 2022, the following leads to an error setting up the container.
# The Linux kernal number is > 255 and this is incompatible with something in libc.
# Revisit this in the future.
# apt-get install -y valgrind

apt-get install -y valgrind

# Build valgrind ourselves.
# apt-get install -y libc6-dbg
# curl https://sourceware.org/pub/valgrind/valgrind-3.18.1.tar.bz2 > valgrind-3.18.1.tar.bz2
# bunzip2 valgrind-3.18.1.tar.bz2
# tar xvf valgrind-3.18.1.tar
# cd valgrind-3.18.1
# ./configure
# make -j 2
# make install
