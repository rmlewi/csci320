#include <stdio.h>
#include <stdlib.h>

int *change(int amount);
  
int *rml_change(int amount)
{
  if (amount < 0) return NULL;
  
  const int denom[] = {25, 10, 5, 1};
  int *result = (int*) malloc(4 * sizeof(int));  
  
  for (int k = 0; k < 4; k++) {
    result[k] = amount/denom[k];
    amount %= denom[k];
  }

  return result;
}

int main(int argc, char **argv)
{
  int retcd = 0;
  int *my_change = NULL;
  int *their_change = NULL;
  for (int n = -199; n < 199; n++) {
    my_change = rml_change(n);
    their_change = change(n);  
    for (int i = 0; i < 4; i++) {
      if ((my_change != NULL) || ((my_change == NULL) && (their_change != NULL))) {
	if (their_change[i] != my_change[i]) {
	  printf("For the amount %d I compute (%d, %d, %d, %d) while you compute (%d, %d, %d, %d)!\n",
		 n, my_change[0], my_change[1], my_change[2], my_change[3],
		 their_change[0], their_change[1], their_change[2], their_change[3]);
	  retcd = 64;
	  continue;
	}
      }
    }
    /* 
     * Per the free man page,
     *   The free() function deallocates the memory allocation pointed to by ptr. If ptr is a NULL pointer, no
     *   operation is performed.
     */
    free(my_change);
    free(their_change);
  }

  return retcd;
}

#ifdef OLD
int main(int argc, char **argv)
{
  int retcd = 0;
  int *my_change = NULL;
  int *their_change = NULL;
  for (int n = -199; n < 199; n++) {
    my_change = rml_change(n);
    their_change = change(n);
    if ((n < 0) && (their_change != NULL)) {
      printf("You returned a non-NULL pointer for the amount %d!\n", n);
      retcd = 32;
    }
    else if (n >= 0) {
      for (int i = 0; i < 4; i++) {
	if (my_change[i] != their_change[i]) {
	  printf("For the amount %d I compute (%d, %d, %d, %d) while you compute (%d, %d, %d, %d)!\n",
		 n, my_change[0], my_change[1], my_change[2], my_change[3],
		 their_change[0], their_change[1], their_change[2], their_change[3]);
	  retcd = 64;
	  continue;
	}
      }
    }
  }

  return retcd;
}
#endif
