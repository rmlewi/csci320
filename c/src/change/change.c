#include <stdio.h>
#include <stdlib.h>

int *change(int amount);

int main(int argc, char **argv)
{
  int amount;
  int *result;
  
  if (argc == 1) {
    printf("Usage: %s n\n", argv[0]);
    return 0;    
  }

  amount = atoi(argv[1]);
  result = change(amount);

  for (int k = 0; k < 4; k++) {
    printf("%d\n", result[k]);
  }

  return 0;
}

int *change(int amount)
{
  const int denom[] = {25, 10, 5, 1};
  int *result = (int*) malloc(4 * sizeof(int));  
  
  for (int k = 0; k < 4; k++) {
    result[k] = amount/denom[k];
    amount %= denom[k];
  }

  return result;
}
