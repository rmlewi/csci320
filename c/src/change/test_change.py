from change import change

def test_change():
    QUARTERS = 25
    DIMES    = 10
    NICKELS  =  5
    PENNIES  =  1

    num_quarters = [i for i in range(8)]
    num_dimes    = [i for i in range(3)]
    num_nickels  = [i for i in range(2)]
    num_pennies  = [i for i in range(5)]

    for q in num_quarters:
        for d in num_dimes:
            for n in num_nickels:
                for p in num_pennies:
                    if (d*DIMES + n*NICKELS < QUARTERS):
                        amount = q*QUARTERS + d*DIMES + n*NICKELS + p*PENNIES
                        amount /= 100
                        assert (q, d, n, p) == change(amount)

import inspect
lines = inspect.getsource(change)
print(lines)
print(f'>{inspect.getdoc(change)}<')
                        
