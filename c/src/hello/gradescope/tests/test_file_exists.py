import unittest
from gradescope_utils.autograder_utils.decorators import number, weight
from gradescope_utils.autograder_utils.files import check_submitted_files


FILES = ['hello.c', 'hello']  # The name of the submission files.


class TestFile(unittest.TestCase):
    @number('0')
    @weight(0)
    def test_submitted_files(self):
        '''Check that the correct file was submitted.'''
        missing_file = check_submitted_files(FILES, base='.')
        for path in missing_file:
            print(f'The file "{path}" was not found.')
        self.assertEqual(len(missing_file), 0, 'Check on the missing file! \U0001f635')
        print('Required file submitted! \U0001f44d')
