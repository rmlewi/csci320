'''
This python script runs the tests using the JSONTestRunner class from
gradescope-utils. This writes the JSON formatted output to stdout,
which is captured and uploaded by the autograder.
'''

import unittest
from gradescope_utils.autograder_utils.json_test_runner import JSONTestRunner

if __name__ == '__main__':
    suite = unittest.defaultTestLoader.discover('tests')
    JSONTestRunner().run(suite)
