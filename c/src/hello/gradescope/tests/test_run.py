import subprocess
import unittest

from gradescope_utils.autograder_utils.decorators import number, weight

class Test_Hello(unittest.TestCase):
    '''Build and runtime tests.'''

    @number('1')
    @weight(10)
    def test_student_exec(self):
        '''Check the student's executable for correctness.'''

        print('Checking for correctness...')
        cmd = ['./hello']
        test = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = test.stdout.read().strip().decode('utf-8')
        test.kill()

        print(f'The command \"{" ".join(cmd)}\" produced this output:')
        print(output)
        self.assertEqual(output, "Hello, world!", 'Look up the correct output! \U0001f635')
        test.terminate()
        print('Most excellent! \U0001F389 \U0001F38A \U0001F387')


    @number('2')
    @weight(0)
    def test_build(self):
        '''Build the executable.'''
        cmd = ['gcc', '-std=c11', '-Wall', '-pedantic', 'hello.c']  # No spaces in args!

        print('Building the executable with')
        print(f'   {" ".join(cmd)}')
        test = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = test.stderr.read().strip().decode('utf-8')
        test.kill()

        if output:
            print('The program failed to compile or link.  Here is the message:')
            print()
            print(output)
            print()
        else:
            print('...good!')
        self.assertTrue(output == "", '\U0001f635')
        test.terminate()


    @number('3')
    @weight(0)
    def test_run(self):
        '''Check whether the executable runs to completion.'''
        cmd = ['./a.out']
        test = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE) #, shell=True)

        output, err = test.communicate()
        retcd = test.returncode

        if retcd != 0:
            print(f'Ack! execution failed with the return code {retcd}!')
            if retcd == -9:  # SIGKILL
                print('This indicates something killed the job.')
            elif retcd == -10:  # SIGBUS
                print('This indicates a bus error.')
            elif retcd == -11:  # SIGSEGV
                print('This indicates a segmentation violation.')
            elif retcd == 124:  # timeout
                print('This indicates the job timed out.')
        else:
            print(f'Execution completed with the return code {retcd}...good! \U0001f44d')

        self.assertEqual(retcd, 0, '\U0001f635')


    @number('4')
    @weight(10)
    def test_correctness(self):
        '''Check the executable for correctness.'''

        print('Checking for correctness...')
        cmd = ['./a.out']
        test = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = test.stdout.read().strip().decode('utf-8')
        test.kill()

        print(f'The command \"{" ".join(cmd)}\" produced this output:')
        print(output)
        self.assertEqual(output, "Hello, world!", 'Look up the correct output! \U0001f635')
        test.terminate()
        print('Most excellent! \U0001F389 \U0001F38A \U0001F387')
