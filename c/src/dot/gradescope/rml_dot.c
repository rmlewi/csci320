#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double dot(const double *const x, const double *const y, const long int n);
  
double rml_dot(const double *const x, const double *const y, const long int n)
{
  double sum = 0.0;
  for (long int i = 0; i < n; i++) {
    sum += x[i] * y[i];
  }
  return sum;
}

int main(int argc, char **argv)
{
  double *x = NULL;
  double *y = NULL;  
  long int n[] = {1, 42, 1000, 1000000};
  double my_dot, their_dot;
  double rtol = 1.0e-08;
  int retcd = 0;

  for (int i = 0; i < 4; i++) {
    x = malloc(n[i] * sizeof(double));
    y = malloc(n[i] * sizeof(double));  

    /* Generate PRNs in the range [-1, 1]. */
    for (int j = 0; j < n[i]; j++) {
      x[j] = 2.0 * (drand48() - 0.5);
      y[j] = 2.0 * (drand48() - 0.5);    
    }

    my_dot    = rml_dot(x, y, n[i]);
    their_dot = dot(x, y, n[i]);

    if (fabs(my_dot - their_dot) > rtol * fabs(my_dot)) {
      printf("The relative difference between one of our results was > %e!  you: %f  me: %f\n", rtol, their_dot, my_dot);
      retcd = 16;
    }
  }

  return retcd;
}
