#!/usr/bin/env bash

# This script installs Python and gcc.

apt-get install -y python3
apt-get install -y gcc
