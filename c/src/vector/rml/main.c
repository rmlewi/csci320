#include <stdio.h>

#include "fun.h"
#include "vector.h"

int main(int argc, char **argv)
{
  int n = 4;
  VECTOR *x = create_vector(n);
  VECTOR *y = create_vector(n);

  printf("length(x): %ld\n\n", x->length(x));

  printf("x, y\n");
  for (long int i = 0; i < n; i++) {
    x->a[i] = i;
    y->a[i] = i * i;
    printf("%lf  %lf\n", x->a[i], y->a[i]);    
  }
  printf("\n");
  
  /*
   * x->plus points to vector addition.
   */

  VECTOR *z = x->plus(x, y);

  printf("plus(x, y)\n");
  for (long int i = 0; i < z->n; i++) {
    printf("%lf\n", z->a[i]);
  }
  printf("\n");

  /*
   * Swap the function pointer so now x->plus will point to 
   * vector concatenation rather than addition.
   */
  
  x->plus = &concat;

  free_vector(z);
  z = x->plus(x, y);

  printf("concat(x, y)\n");
  for (long int i = 0; i < z->n; i++) {
    printf("%lf\n", z->a[i]);
  }
  printf("\n");  

  free_vector(z);
  z = x->times(x, y);

  printf("times(x, y)\n");
  for (long int i = 0; i < z->n; i++) {
    printf("%lf\n", z->a[i]);
  }
  printf("\n");  

  /* Clean up. */
  x->free(x);
  y->free(y);  
  z->free(z);  
}
