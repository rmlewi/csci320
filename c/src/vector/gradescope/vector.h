#ifndef __VECTOR_H__
#define __VECTOR_H__

typedef struct vector {
  long int n;
  double *a;

  /*
   * The following are pointers to functions that return a struct vector* (i.e., VECTOR*)
   * and take two VECTOR* as inputs.
   */
  struct vector* (*plus)(const struct vector *x,  const struct vector *y);
  struct vector* (*times)(const struct vector *x,  const struct vector *y);

  void (*free)(struct vector *x);

  long int (*length)(const struct vector *x);
} VECTOR;

VECTOR *create_vector(long int n);
void free_vector(VECTOR *x);

#endif
