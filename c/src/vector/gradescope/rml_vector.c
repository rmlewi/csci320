#include <stdlib.h>

#include "vector.h"
#include "rml_fun.h"

long int rml_length(const struct vector *x)
{
  return x->n;
}

VECTOR *rml_create_vector(long int n)
{
  VECTOR *pt = malloc(sizeof(VECTOR));
  pt->n = n;
  pt->a = (double*) malloc(n * sizeof(double));

  pt->plus = &rml_plus;  /* Pointer to function. */
  pt->times = &rml_times;
  pt->free = &free_vector;

  pt->length = &rml_length;

  return pt;
}

void rml_free_vector(VECTOR *x)
{
  /*
   * Free all memory associated with a VECTOR*.
   * No bugs here!
   */

  free(x->a);
  free(x);
}
