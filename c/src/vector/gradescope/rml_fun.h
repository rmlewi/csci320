#ifndef __RML_FUN_H__
#define __RML_FUN_H__

#include "vector.h"

VECTOR *rml_create_vector(long int n);
void rml_free_vector(VECTOR *x);

VECTOR *rml_plus(const VECTOR *x, const VECTOR *y);
VECTOR *rml_times(const VECTOR *x, const VECTOR *y);

VECTOR *rml_concat(const VECTOR *x, const VECTOR *y);

#endif
