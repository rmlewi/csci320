#include <stdio.h>

#include "vector.h"
#include "fun.h"
#include "rml_fun.h"

void urand(double *X, int n)
{
  /*
   * This function returns uniformly distributed pseudo-random
   * numbers in the interval [0,1].
   *
   * The algorithm goes like this:  the n^{th} random number u_{n}
   * is generated via the formulae
   *
   *     u_{n}   = z_{n} / m
   *     z_{n+1} = f(z_{n}),
   *
   * where f(z) = (a*z) mod m.
   *
   * The computations are arranged to avoid integer overflow in the
   * product a*z.
   *
   * NB: This implementation is robust ONLY if the maximum (signed)
   * integer that can be represented is >= 2^{31} - 1.  This is the
   * case for every processor I know of that uses 32-bit integers.
   */

  static int seed = 42;
  const int
    a = 16807,
    m = 2147483647,
    q = 127773,
    r = 2836;

  int lo, hi, test;

  for (int k = 0; k < n; k++) {
    hi = seed / q;
    lo = seed % q;
    test = (a * lo) - (r * hi);

    seed = (test > 0.0) ? test : (test + n);

    X[k] = ((double) seed) / ((double) m);
  }

  return;
}

int main(int argc, char **argv)
{
  int n = 42;
  VECTOR *my_struct    = rml_create_vector(n);
  VECTOR *their_struct = create_vector(n);

  VECTOR *x = rml_create_vector(n);
  VECTOR *y = rml_create_vector(n);
  VECTOR *z = rml_create_vector(n);

  int retcd = 0;
  
  for (int i = 0; i < n; i++) {
    x->a[i] = i;
    y->a[i] = i * i;
  }

  VECTOR *my_ans = my_struct->times(x, y);
  VECTOR *their_ans = their_struct->times(x, y);

  for (int i = 0; i < n; i++) {
    if (my_ans->a[i] != their_ans->a[i]) {
      retcd = 42;
    }
  }

  if (retcd) {
    printf("Our results do not agree!\n");
    printf("Me: ");
    for (int i = 0; i < n; i++) {
      printf("%.2f ", my_ans->a[i]);
    }
    printf("\n");
    printf("You: ");
    for (int i = 0; i < n; i++) {
      printf("%.2f ", their_ans->a[i]);
    }
  }

  rml_free_vector(my_struct);
  free_vector(their_struct);

  rml_free_vector(x);
  rml_free_vector(y);
  rml_free_vector(z);

  rml_free_vector(my_ans);
  free_vector(their_ans);

  return retcd;
}
