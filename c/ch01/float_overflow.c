#include <stdio.h>

int main(void)
{
  double x = 1.0e+308;
  double y = 1.0e-308;

  printf("%.2e + %.2e + %.2e = %lf\n", x, x, x, x + x + x);
  printf("%.2e / %.2e = %lf\n", y, x, y/x);
  return 0;
}
