#include <stdio.h>

int main(int argc, char **argv)
{
  /* You cannot /* nest block comments */. */
  // Some compilers accept // to indicate the start of a comment line.
  return 0;
}
