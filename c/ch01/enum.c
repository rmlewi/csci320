#include <stdio.h>

int main(void)
{
  /* FEB is 2, MAR is 3, &c. */
  enum months {
    JAN = 1, FEB, MAR, APR, MAY, JUN,
    JUL,     AUG, SEP, OCT, NOV, DEC
  };

  /* FALSE is 0, TRUE is 1, yea and nay are variables with these values. */
  enum boolean {FALSE, TRUE} yea, nay;

  yea = TRUE;
  nay = FALSE;

  printf("yea: %d, nay: %d\n", yea, nay);
  printf("JAN: %d, DEC: %d\n", JAN, DEC);
  
  return 0;
}
