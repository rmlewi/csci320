#include <stdio.h>

int main(int argc, char **argv)
{
  float x;

  /* Width of a variable. */
  printf("The float variable x occupies %lu bytes.\n\n", sizeof(x));
  
  /* Widths (number of bytes) of different flavors of floating point. */

  printf("A float  is %lu bytes wide.\n", sizeof(float));
  printf("A double is %lu bytes wide.\n\n", sizeof(double));

  return 0;
}
