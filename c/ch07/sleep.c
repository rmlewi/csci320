#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  unsigned int n = 10;

  printf("Sleeping for %u seconds...", n);
  
  /* Sleep for n seconds. */
  sleep(n);

  printf("done!\n");
  return 0;
}
