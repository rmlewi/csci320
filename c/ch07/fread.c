#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  int m;
  unsigned long int n = 1024;
  char *pt = malloc(n * sizeof(char));
  int  *qt = malloc(n * sizeof(int));
  struct stat stat_struct;

  /* Open the file. */
  FILE *fp = fopen("ch07/walrus.txt", "r");  /* A file pointer. */

  /* Get information about the file with stat().  See the man page for stat() for details. */
  stat("ch07/walrus.txt", &stat_struct);
  printf("size of file (st_size): %lld\n\n", stat_struct.st_size);

  /* Read 32 bytes from the file. */
  m = fread(pt, sizeof(char), 32, fp);
  printf("number of bytes read: %d\n", m);
  printf("feof: %d\n", feof(fp));
  printf("ferror: %d\n\n", ferror(fp));

  /* Try to read 1024 bytes from the file; we hit the end of file (EOF) first. */
  m = fread(qt, sizeof(char), n, fp);
  printf("number of bytes read: %d\n", m);
  printf("feof: %d\n", feof(fp));
  printf("ferror: %d\n\n", ferror(fp));

  /* Move to the beginning of the file and read a line at a time.. */
  rewind(fp);
  while (getline(&pt, &n, fp)) {
    printf("%s", pt);
  }

  fclose(fp);  /* Not actually needed since we are about to terminate. */
}
