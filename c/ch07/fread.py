import os

# Open the file.
with open("ch07/walrus.txt", "r") as f:
    # Get information about the file with os.stat()
    statinfo = os.stat("ch07/walrus.txt")
    print(statinfo)

    # Read the entire file.
    text = f.read()
    print(text)

    # I can't find a simple way to read only a specified number of bytes.
    # All I can think to do is read the entire file and then read a specified 
    # number of bytes from the extracted contents.

    # Move to the beginning of the file and read a line at a time...
    f.seek(0)
    for line in f:
        print(line, end='')

# f.close()  # Not needed since we used a context manager (the with statement).
