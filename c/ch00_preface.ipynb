{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "<h1 style=\"text-align:center;\">C Survival School</h1>\n",
    "<h2 style=\"text-align:center;\">Robert Michael Lewis</h2>\n",
    "<h1 style=\"text-align:center;\">Preface</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Copyright © 2022 Robert Michael Lewis.  All rights reserved.\n",
    "\n",
    "Permission is granted to copy, distribute, and/or modify these notes under the terms of the Creative Commons Attribution-NonCommercial 3.0 Unported License, which is available at http://creativecommons.org/licenses/by-nc/3.0/."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"ch01_variables_types.ipynb\" style=\"float:right;\">Next: Variables, types, and expressions</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [About this course](#About-this-course)\n",
    "* [About C](#About-C)\n",
    "* [Toto, I've a feeling we're not in Kansas anymore](#Toto,-I've-a-feeling-we're-not-in-Kansas-anymore)\n",
    "* [C vs. Python vs. C++ vs. Java](#C-vs.-Python-vs.-C++-vs.-Java)\n",
    "* [¡Cuidado! ¡llamas!!](#%C2%A1Cuidado!--%C2%A1llamas!)\n",
    "* [About the Jupyter notebooks](#About-the-Jupyter-notebooks)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# About this course \n",
    "\n",
    "This is an introduction to the C programming language designed for someone who knows Python.  We will assume you are coming from Python; if you are coming from C++ or Java you will have already seen most C syntax.  Conversely, if you know C then you know a fair chunk of the syntax of C++ and Java (among other languages).\n",
    "\n",
    "These notes draw in part on the following books:\n",
    "* *The C Programming Language, 2nd ed.* (1988) by Brian Kernighan and Dennis Ritchie, known as K&amp;R for short.  Even though it is dated this is a classic text in computer science and well worth reading.  You could do worse than to learn programming style from K&amp;R.\n",
    "* *C in a Nutshell, 2nd ed.* (2016) by Peter Prinz and Tony Crawford.  This is a current text.  Half of the book is devoted to describing every function in the C standard library, which is very useful."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# About C <a id=\"C\"/>\n",
    "\n",
    "[C](https://en.wikipedia.org/wiki/The_C_Programming_Language) was developed by <a href=\"https://amturing.acm.org/award_winners/ritchie_1506389.cfm\">Dennis Ritchie</a> at Bell Labs in the early 1970s.  Its name comes from the fact that it was the successor to a language named [B](https://en.wikipedia.org/wiki/B_(programming_language)) (B, in turn, was derived from a language named [BCPL](https://en.wikipedia.org/wiki/BCPL)).  There was no official C standard until 1989 and many different dialects of the language evolved before then.  C was standardized in 1989 (C89) and underwent revisions in 1999 (C99), 2011 (C11), and 2017 (C17).  The C17 standard cleared up some things from C11 but did not change the language. The next revision, which may be finalized in 2022 or 2023, is currently called C2x.\n",
    "\n",
    "By any measure C has been an influential and consequential language.  It is fair to say that C is the language used to create the Internet.  The hearts of the Unix, Linux, and Windows operating systems are written in C, as is [CPython](https://github.com/python/cpython), the reference implementation of the Python interpreter from the Python Software Foundation.\n",
    "\n",
    "C is very nearly a proper subset of the C++ language, but there are some [incompatibilities and inconsistencies](https://en.wikipedia.org/wiki/Compatibility_of_C_and_C%2B%2B).\n",
    "\n",
    "C is a **typed language**, meaning that you must declare the type of every one of your variables.  C is also a **compiled language**, meaning that the code you write must first be transformed into machine instructions before being run.\n",
    "\n",
    "The advantage of a compiled language is that the code runs fast.  However, the resulting executable program is peculiar to the architecture and operating system for which it is built.  This means that typically a program built in Linux is incompatible with Unix or Windows, and a program built for an Intel processor is incompatible with an ARM processor. (This is not 100% true, [as there are ways to build a single universal executable program that can run on multiple architectures](https://en.wikipedia.org/wiki/Rosetta_(software)), but these require specialized software support.)\n",
    "\n",
    "We will work with two of the most common open source C compilers,\n",
    "* [`gcc`](https://gcc.gnu.org), the C front-end of the GNU Compiler Collection, and\n",
    "* [`clang`](https://clang.llvm.org), the C front-end of the LLVM Project.\n",
    "\n",
    "There are also numerous proprietary compilers, such as those from Intel, IBM, and NVIDIA.\n",
    "\n",
    "The GNU C compiler was first released in 1987.  The GNU project is a massive open-source software project that was initiated in the early 1980s by open-source pioneer [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman).  GNU stands for \"GNU's not Unix\" as the original objective was to build an open source, non-proprietary Unix-like operating system and software ecosystem.  \n",
    "\n",
    "The LLVM Project began at the University of Illinois Urbana-Champaign around 2000.  LLVM originally was an acronym for Low-Level Virtual Machine, but LLVM is no longer an acronym but simply the name of the project."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Toto, I've a feeling we're not in Kansas anymore\n",
    "\n",
    "<img src=\"http://www.cs.wm.edu/~rml/images/dont_panic.png\" style=\"float: right; padding: 8px; width: 200px; height: auto;\"></img>\n",
    "\n",
    "C is a **very** different language than Python.  Compared to Python, C is a [low-level language](https://en.wikipedia.org/wiki/High-level_programming_language#Relative_meaning).  \n",
    "\n",
    "To use C in a safe and effective way requires that you view data in the same way that your computer does, as an assemblage of bits and bytes at specific locations in memory.\n",
    "\n",
    "In C you operate at the hardware level, unlike Python, so you are able to introduce **much** more entertaining bugs into your code, and encounter whole new classes of delightful runtime errors.  On the other hand, you can do many more things in C than you can in Python.\n",
    "\n",
    "Coming from Python you might find it annoying that you need to think in terms of bits and bytes and memory locations, but languages like C make languages like Python possible, and it is likely that someday you will find yourself working at the bit and byte level.\n",
    "\n",
    "You will also learn that C trusts that you know what you are doing, and there are no guardrails or safety features.  Want to read random bits from uninitialized memory?  go ahead!  Want to write past allocated memory?  knock yourself out!\n",
    "\n",
    "To paraphrase an observation originally made about the Unix operating system,\n",
    "<blockquote>\n",
    "C was not designed to stop you from doing stupid things, because that would also stop you from doing clever things.\n",
    "</blockquote>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python vs. C vs. C++ vs. Java\n",
    "\n",
    "We will preface each notebook with a table showing the correspondence between C concepts in the notebook and their analogues in Python, C++, and Java."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# &iexcl;Cuidado!  &iexcl;llamas!\n",
    "\n",
    "<p>\n",
    "<div class=\"danger\"></div> \n",
    "We will use the metric road sign for dangerous curves (also known as the <a href=\"https://en.wikipedia.org/wiki/Bourbaki_dangerous_bend_symbol\">Bourbaki dangerous bend symbol</a>) to indicate important information such as common pitfalls and mistakes.  Two such signs will be used to indicate especially important things.\n",
    "</p>\n",
    "\n",
    "We will use the exploding head emoticon, 🤯, to indicate in-depth or more advanced topics they may be skipped on a first reading.\n",
    "\n",
    "We will use the sceam emoticon, 😱 , to indicate particularly frightening topics.\n",
    "\n",
    "Discussions of things that lead to bugs are marked 🐞."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"ch01_variables_types.ipynb\" style=\"float:right;\">Next: Variables, types, and expressions</a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
