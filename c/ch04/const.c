#include <stdio.h>

int main(void)
{
  int m = 54, n = 42;
  const int *p = &n;  /* Pointer value can change; value pointed to cannot change. */
  int *const q = &n;  /* Pointer value cannot change; value pointed to can change. */
  const int *const t = &n;  /* Neither pointer nor value pointed to can change. */

  *p = 54;  /* Can't change the value pointed to... */
   p = &n;  /* ...but can change the pointer!   */
   
  *q = 42;  /* Can change the value pointed to... */
   q = &m;  /* ...but can't change the pointer!   */
   
  *t = 42;  /* Can't change the value pointed to... */
   t = &m;  /* ...and can't change the pointer!     */
  
  return 0;
}
