#include <stdio.h>

/* Fun with function pointers: writing a generic sorting routine. */

void bubblesort(void *a, int n, int (*gt)(void*, int), void (*swap)(void*, int))
{
  int not_done;

  do {
    not_done = 0;
    for (int i = 0; i < n-1; i++) {  /* Sweep from left to right. */
      if (gt(a, i)) {  /* Swap adjacent out-of-order terms. */
        swap(a, i);
        not_done = 1;
      }
    }
  } while (not_done);
}

int int_gt(void *a, int i)
{
  int *x = a;
  return (x[i] > x[i+1]);
}

void int_swap(void *a, int i)
{
  int *x = a;
  int temp;

  temp = x[i];
  x[i] = x[i+1];
  x[i+1] = temp;
}

int double_gt(void *a, int i)
{
  double *x = a;
  return (x[i] > x[i+1]);
}

void double_swap(void *a, int i)
{
  double *x = a;
  double temp;

  temp = x[i];
  x[i] = x[i+1];
  x[i+1] = temp;
}

int main(void)
{
  int n = 5;
  int a[] = {5, 4, 3, 2, 1};
  double x[] = {5, 4, 3, 2, 1};  

  printf("before bubblesort a = ");
  for (int i = 0; i < 5; i++) {
    printf("%d ", a[i]);
  }
  printf("\n");

  bubblesort(a, n, int_gt, int_swap);

  printf("after  bubblesort a = ");
  for (int i = 0; i < 5; i++) {
    printf("%d ", a[i]);
  }
  printf("\n");


  printf("before bubblesort x = ");  
  for (int i = 0; i < 5; i++) {
    printf("%f ", x[i]);
  }
  printf("\n");  

  bubblesort(x, n, double_gt, double_swap);

  printf("before bubblesort x = ");  
  for (int i = 0; i < 5; i++) {
    printf("%f ", x[i]);
  }
  printf("\n");  

  return 0;
}
