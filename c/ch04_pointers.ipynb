{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">C Survival School</h1>\n",
    "<h1 style=\"text-align:center;\">Chapter 4</h1>\n",
    "<h1 style=\"text-align:center;\">Pointers and arrays</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"ch03_flow.ipynb\" style=\"float:left;\">Previous: Flow control</a> \n",
    "<a href=\"ch05_debugging_memory.ipynb\" style=\"float:right;\">Next: Debugging memory errors</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [4.1 Indirection](#4.1-Indirection)\n",
    "* [4.2 Pointers and addresses](#4.2-Pointers-and-addresses)\n",
    "* [4.3 A swap function](#4.3-A-swap-function)\n",
    "* [4.4 Pointer arithmetic](#4.4-Pointer-arithmetic)\n",
    "* [4.5 Pointers and arrays and strings](#4.5-Pointers-and-arrays-and-strings)\n",
    "* [4.6 Bubblesort in C](#4.6-Bubblesort-in-C)\n",
    "* [4.7 Pointers to functions](#4.7-Pointers-to-functions)\n",
    "* [4.8 Pointers in CPython](#4.8-Pointers-in-CPython)\n",
    "* [4.9 Dynamic memory allocation](#4.9-Dynamic-memory-allocation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python vs. C vs. C++ vs. Java\n",
    "\n",
    "|           | Python          | C            | C++   | Java  |\n",
    "| :--------:| :-------------: | :----------: | :---: | :---: |\n",
    "| pointer   |                 | type &#42;  | type &#42; |   |\n",
    "| address of ```var``` |      | ```&var```  | ```&var``` |   |\n",
    "| memory allocation |  | `malloc()`, `calloc()` | `new` | same as C++ |\n",
    "| memory deallocation | | `free()` | `delete` | same as C++ |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.1 Indirection\n",
    "\n",
    "<blockquote>\n",
    "By indirections find directions out. <br/>\n",
    "&ndash; Hamlet, Act 2, Scene 1\n",
    "</blockquote>\n",
    "\n",
    "In computer science, [**indirection**](https://en.wikipedia.org/wiki/Indirection) is the ability to refer to an object, well, indirectly through some mechanism other than the object itself.\n",
    "\n",
    "Here is an illustration.  Imagine we need to sort a set of numbers in ascending order.  One simple approach (which does not scale well!) is **bubblesort**: we make repeated passes through the numbers, and if two adjacent numbers are out of order relative to one another we swap them.  We are done when we make a pass without finding any out of order numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/bubblesort.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "python ch04/bubblesort.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now suppose that instead of sorting numbers, we wish to sort large, heavy rocks so that when we are done they are lined up in order of ascending weight.  The swaps of adjacent rocks would be onerous work, so instead we will use indirection:\n",
    "1.  First label the rocks (in no particular order) $1$ to $n$.\n",
    "2.  For each rock, take a piece of paper and write the rock's weight and the label we gave it in step 1.\n",
    "3.  Perform bubblesort on the pieces of paper so they are sorted in order of increasing weight.\n",
    "4.  Once we have things sorted in order of ascending weight, look up the rocks using the labels from step 1 and assemble the rocks in sorted order.\n",
    "\n",
    "The use of the labels for the rocks rather than the rocks themselves for the purposes of sorting is an example of indirection.  In this example it allows us to move the heavy rocks exactly one time each."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.2 Pointers and addresses\n",
    "\n",
    "Regions of memory are often referred to as **memory buffers** or just **buffers**.  One of C's strengths is the ability manipulate objects using indirection via their addresses in memory.  A **pointer** is a variable that can hold a memory address.  They are declared with the syntax\n",
    "```\n",
    "type *var;\n",
    "```\n",
    "where ```type``` is the type of object we are pointing to (e.g., ```int```, ```float```).\n",
    "\n",
    "For example:\n",
    "```\n",
    "int n;   // An integer.\n",
    "int *p;  // A pointer to an integer.\n",
    "int *q;  // Another pointer to an integer.\n",
    "double *x  // A pointer to a double.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> \n",
    "🐞 Be careful &ndash; the <code>*</code> modifies the variable that follows, not the type that precedes it. 🐞"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is, the declaration is\n",
    "<code>\n",
    "    int* p, q;\n",
    "</code>\n",
    "is equivalent to\n",
    "<code> \n",
    "    int *p, q;\n",
    "</code>    \n",
    "so\n",
    "* <code>p</code> is a pointer to an <code>int</code>, but \n",
    "* <code>q</code> is an <code>int</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can get the address of an existing variable with the `&` operator.\n",
    "\n",
    "Let's take a look at some code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/pt_intro.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall ch04/pt_intro.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At line 11 we set ```p``` to the address of ```n```.  We say that `p` now points to `n`.  We print the value of the pointer in line 12 using the `%p` print format.  The address begins with `0x`, which indicates that this is a hexadecimal (base 16) number.  The hexadecimal digits are\n",
    "```\n",
    "hexadecimal: 0 1 2 3 4 5 6 7 8 9 a  b  c  d  e  f\n",
    "decimal:     0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15\n",
    "```\n",
    "\n",
    "We can access the value of the variable ```n``` through the pointer ```p``` by **dereferencing** the pointer.  The syntax ```*p``` in line 14 means the value at the memory location ```p``` points to.\n",
    "\n",
    "Since ```p``` is declared to point to an ```int```, the statement ```*p``` says to\n",
    "\n",
    "1. go to the location in memory stored in ```p```,\n",
    "2. read one ```int``` worth of bits (typically 32 bits), and\n",
    "3. interpret those bits as an ```int```.\n",
    "\n",
    "In line 16 we write to the location that ```p``` points to, thereby changing `n`.\n",
    "\n",
    "Multiple pointers can point to the same location, as lines 20 and 21 demonstrate."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div>\n",
    "Pointers can be confusing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "If <code>p</code> is a pointer, <code>p</code> refers to a location in memory.\n",
    "\n",
    "In a declaration <code>*</code> means the variable is a pointer.  When used in dereferencing, on the other hand, the <code>*</code> means the object the pointer points to, if it appears on the left-hand side of an assignment, or the value of the object the pointer points to, otherwise.\n",
    "\n",
    "<pre>\n",
    "double x = 54;   /* A double. */\n",
    "double *p = &x;  /* A pointer to x. */\n",
    "double y;        /* Another double. */\n",
    "\n",
    "y = *p;   /* Equivalent to y = x. */\n",
    "*p = 42;  /* Equivalent to x = 42. */\n",
    "    \n",
    "p = 42;   /* Sets p to point to the address 42, which is probably invalid! */\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall **references** from Python &ndash; these are two variables that refer to same underlying object.  In the following Python code, both `a` and `b` refer to the same list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/reference.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "python ch04/reference.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "References are like pointers, only without the need to dereference a pointer.  But references are implemented in practice using pointers that point to the same object in memory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can declare pointers to be `const`, and you can declare pointers that point to `const` variables, and you can declare `const` pointers that point to `const` variables (got that?):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/const.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c -Wall -pedantic ch04/const.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.3 A swap function\n",
    "\n",
    "Let's take a look at how we might use pointers.  Suppose we wanted to implement a function that swaps the value of two integers.  Here was our first attempt in Chapter 2:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/bad_swap.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/bad_swap.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Drats!  we are foiled by the fact C uses call by value to call functions.  We swapped copies of the values of ```x``` and ```y```, not the original variables.\n",
    "\n",
    "The solution is to pass the **addresses** of ```x``` and ```y``` and swap the values found at those addresses.  A copy of a pointer points to the same location, so we will not be thwarted by call by value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat ch04/swap.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch04/swap.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.4 Pointer arithmetic\n",
    "\n",
    "If ```p``` is a pointer to an integer, ```p + i``` refers to the location in memory that is ```i``` integers past ```p```.  \n",
    "\n",
    "In terms of bytes, ```p + i``` is the location ```i * sizeof(int)``` bytes past ```p```.\n",
    "\n",
    "In general, if ```p``` points to a variable of type ```type```, then ```p + i``` refers to the location in memory that is ```i``` of type ```type``` past ```p```, or ```i * sizeof(type)``` bytes past ```p```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/pt_arith.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch04/pt_arith.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In hexadecimal `c` represents decimal 12:\n",
    "\n",
    "```\n",
    "hexadecimal: 0 1 2 3 4 5 6 7 8 9 a  b  c  d  e  f\n",
    "decimal:     0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15\n",
    "```\n",
    "\n",
    "so\n",
    "\n",
    "```\n",
    "(p + 1) - p = 0x7ff7b34657dc - 0x7ff7b34657d8 = c - 8 = 4.\n",
    "```\n",
    "or 4 bytes, which is the size of an `int`.\n",
    "\n",
    "This illustrates an important aspect of working with pointers &ndash; usually we don't care about the values of pointers but rather the relative locations of pointers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.5 Pointers and arrays and strings\n",
    "\n",
    "There is a very close connection in C between pointers, arrays, and strings (which are just arrays of characters).  You should study the idioms in this section closely, as they are commonly encountered in C code.\n",
    "\n",
    "First we look at the pointers and arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/arrays.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In line 11 we use array indexing to access the elements of the array `numbers`.\n",
    "\n",
    "In line 16 we set the integer pointer `p` to equal `numbers`.  Since `numbers` is an array, thie has the effect of setting `p` to point to the beginning of `numbers` in memory.\n",
    "\n",
    "In line 17, `*(p + i)` refers to the value `int` that is `i` integers past the beginning of the array `numbers`:\n",
    "* `p + i` points to the location that is `i * sizeof(int)` bytes past the beginning of `numbers`, and\n",
    "* `*(p + 1)` dereferences the address `p + i` as an `int`.  It is equivalent to `p[i]`.\n",
    "\n",
    "In line 23 we achieve the same effect with the pointer `p` using array indexing, just as in line 11."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch04/arrays.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example illustrates an important feature of C: **arrays are really just pointers (and vice-versa)**.\n",
    "\n",
    "Arrays are not first-class objects in C &ndash; an array is really just a pointer to the beginning of the array.  The type of the pointer (e.g. `int*`, `double*`) determines how the bytes at that location and subsequent locations are interpreted.  You are responsible to keeping track of the length of the array."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "💘 C hackers **love pointers**, as they allow them to write terse and cryptic (but powerful!) code. 💘\n",
    "\n",
    "Here are some other C idioms that implement the loops we just looked at:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/arrays2.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In line 17, `numbers + 10` means the address 10 `int` past the start of `numbers`.  In line 18 we encounter `*p++`.  This is evaluated as\n",
    "* use the value of the `int` that `p` points to (`*p`), and\n",
    "* then increment `p` (`p++`); i.e., advance `p` to point to the next `int`.\n",
    "\n",
    "The `for` loop that begins at line 25 is equivalent to the `while` loop at line 18."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div>\n",
    "In general, incrementing a pointer advances it by the size (in bytes) of the object it points to, while decrementing a pointer moves it backward by the size of the object it points to."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To further illustrate C pointer idioms, here is a function that determines the length of a string.\n",
    "\n",
    "A string in C is an array of characters terminated by an ASCII NULL character.  This means that strings in C are really just pointers of type `char*` that point to the first character in the string.  To read the string you go to that starting address and read bytes until you encounter the terminating ASCII NULL."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/strlen.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch04/strlen.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's break down how this function works.  All the work is done in the ```for``` loop on line 6:\n",
    "\n",
    "<code>\n",
    "    for (count = 0; &#42;s; s++, count++);\n",
    "</code>\n",
    "\n",
    "This is equivalent to\n",
    "\n",
    "<code>\n",
    "count = 0;\n",
    "while (*s) {\n",
    "    s = s + 1;  // Advance one character.\n",
    "    count = count + 1;  // Increment the counter.\n",
    "}\n",
    "</code>\n",
    "\n",
    "We start with the pointer ```s``` at the beginning of the string; the increment ```s++``` moves us across the string.  We use the fact that <code>&#42;s</code> will not be zero until we reach the ASCII NULL that terminates the string so the non-zero value <code>&#42;s</code> is interpreted as \"true\" and the loop continues."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Question.</b>  Why can't we declare ```count``` inside the ```for``` statement:\n",
    "\n",
    "<code>\n",
    "int strlen(char *s)\n",
    "{\n",
    "    for (int count = 0; *s; s++, count++);\n",
    "    return count;\n",
    "}\n",
    "</code>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer</b> <br/>\n",
    "<div class=\"voila\">\n",
    "This would make <code>count</code> local to the <code>for</code> statement so it would not be available to return.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pointers to pointers\n",
    "\n",
    "Since a C string is really a pointer to an array of characters, and an array is really a pointer to the first element of the array, then an array of strings is a pointer to an array of pointers &ndash; a `char**`!!\n",
    "\n",
    "Let's look at an example.  Recall the [`sys.argv`](https://docs.python.org/3/library/sys.html#sys.argv) utility from Python.  It is based on the way C programs can get inputs from the command line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/argv.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variable `argc` is the number of command line arguments (including the name of the program), while `argv` is an array of strings (i.e., a pointer to an array of `char*`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch04/argv.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out fee fie foe fum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pointers to pointers take some getting used to.  You may also encounter them in connection with multiply subscripted arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "cat -n ch04/subscripts.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most levels of indirection (stars) I can recall encountering in the wild was four or five (i.e., `float*****`)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.6 Bubblesort in C\n",
    "\n",
    "As another example we will implement the bubblesort algorithm in C:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/bubblesort.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we use a <code class=\"kw\">do</code> rather than a <code class=\"kw\">while</code> loop as we did in Python.  I prefer this since it allows us to avoid initializing `not_done` outside the loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch04/bubblesort.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.7 Pointers to functions\n",
    "\n",
    "In Python functions are first-class objects that can be treated like variables.  In C, on the other hand, functions are not independent objects.  On the third hand, however, it is possible to define a **pointer to a function** (or **function pointer**) through which a function can be called, passed to functions, &amp;c.\n",
    "\n",
    "Here is a function that takes as its input the name of a function, the function, and prints a table of values of the function at evenly spaced points:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/table.c -lm  # The -lm flag says to link against the math library. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch04/table.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function pointer is declared as `double (*f)(double)`.  This means `f` is a pointer to a function that takes a `double` as its input and returns a `double`.  You have to enclose the `*f` with parentheses, as so: `(*f)`; otherwise you end up with a pointer to a `double`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/table2.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch04/table2.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The file `ch04/bubblesort2.c` contains a more complicated use of function pointers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.8 Pointers in CPython\n",
    "\n",
    "CPython, the reference implementation of Python, is written in C and uses pointers extensively.\n",
    "\n",
    "For instance, the Python `bubblesort()` function above takes a list `a` as its input and sorts it, and the sorting persists after we return from the function.  This is similar to the swap function we just looked at.  CPython is passing a pointer to the list to `bubblesort()` and that allows us to manipulate the actual list in memory rather than a copy of the list.\n",
    "\n",
    "[CPython lists are actually arrays of pointers to Python objects](https://docs.python.org/2/faq/design.html#how-are-lists-implemented-in-cpython).  This makes list references extremely efficient, since accessing, say, `a[42]` is a matter of going to the 42nd entry in an array of pointers and then dereferencing the pointer.  If we used a true list then we would need to travel through the objects `a[0]` through `a[41]` before we could access `a[42]`.  The fact that we are storing pointers to Python objects rather than the objects themselves means we are storing values of the same type in the array so lists can contain heterogeneous types of objects.\n",
    "\n",
    "CPython also handles all the memory management for you.  In particular, CPython manages [garbage collection](https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)), which is the reclamation of memory from variables that are no longer in use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.9 Dynamic memory allocation\n",
    "\n",
    "In C/C++/Java you can dynamically allocate new objects in memory.  These are not to be confused with the **automatic variables** local to functions that pass in and out of being when functions are called.\n",
    "\n",
    "In this discussion we will differentiate between two types of memory:\n",
    "* [**stack memory**](https://en.wikipedia.org/wiki/Stack-based_memory_allocation), which is where things like automatic variables reside (along with a lot of other information needed by the program), and\n",
    "* [**heap memory**](https://en.wikipedia.org/wiki/Memory_management#Dynamic_memory_allocation), which is where we can dynamically allocate memory whose contents persist.\n",
    "\n",
    "Each computational process that is running on your computer has its own stack memory.  Because each process has a chunk of stack memory and there are a lot of processes, stack memory is limited in size.\n",
    "\n",
    "Heap memory, on the other hand, is much more plentiful.\n",
    "\n",
    "There is a family of functions in C you can use to dynamically allocate memory to create new arrays and other objects.  The most commonly encountered memory allocation function is called `malloc()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat ch04/malloc.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `malloc()` allocates a contiguous region of memory and returns a pointer to the beginning of the region.  **It does not initialize the allocated memory.**\n",
    "\n",
    "The function `calloc()` does the same thing as `malloc()` but also clears the allocated region by initializing all the bytes to be zero.  Depending on the situation you might or might not want to incur this overhead (e.g., if you immediately write values to the allocated region there is no need to initialize it to zero).\n",
    "\n",
    "The return type of `malloc()` and `calloc()` is `void*`.  The `void*` type is C's way of indicating a generic memory address.  One typically converts (casts) the value returned by `malloc()` or `calloc()` to another type.  Here we use these functions to allocate arrays of length `n`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/alloc.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch04/alloc.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice the different calling sequences for `malloc()` and `calloc()`: `malloc()` takes as its input the total number of bytes that are to be allocated, while `calloc()` takes the number of objects (doubles, ints) and the size of the individual objects (in bytes).\n",
    "\n",
    "If the memory allocation fails, say, because there is insufficient memory available, then these functions return the special value `NULL`, [the null pointer](https://en.wikipedia.org/wiki/Null_pointer).  The value of `NULL` is defined in the header file `stdlib.h` and is implementation-specific but is typically `0`.  `NULL` is an invalid address that no program should ever reference in the course of normal operation.\n",
    "\n",
    "Here is a common C idiom that attempts a memory allocation and checks for an error.  It uses an assignment operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/alloc1.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In line 9 we first set `x = (double*) malloc(n * sizeof(double))`.  We then check whether `x == NULL`.  If so, we print an error message.\n",
    "\n",
    "The function `free()` takes a pointer as its input and frees the memory it points to.  It assumes that the pointer points to memory allocated using one of C's memory allocation functions.  If not, you will likely encounter a runtime error.\n",
    "\n",
    "C does not perform garbage collection &ndash; it is up to you to clean up and release memory when you are done with it.  If you do not, it is possible that you will run out of memory.  When we look at memory access errors we will look at a particular type of runtime error called a [**memory leak**](https://en.wikipedia.org/wiki/Memory_leak) which results from a failure to properly manage memory usage.\n",
    "\n",
    "Typically all allocated memory is released when a program ends.  I say typically since I have seen operating systems where this was not the case, and failure to explicitly call `free()` to release memory could cause the operating system to run out of memory and crash."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"ch03_flow.ipynb\" style=\"float:left;\">Previous: Flow control</a> \n",
    "<a href=\"ch05_debugging_memory.ipynb\" style=\"float:right;\">Next: Debugging memory errors</a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
