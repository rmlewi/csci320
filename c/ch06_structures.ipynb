{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">C Survival School</h1>\n",
    "<h1 style=\"text-align:center;\">Chapter 6</h1>\n",
    "<h1 style=\"text-align:center;\">Structures</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"ch05_debugging_memory.ipynb\" style=\"float:left;\">Previous: Debugging memory errors</a> \n",
    "<a href=\"ch07_io.ipynb\" style=\"float:right;\">Next: i/o</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [6.1 Structures](#6.1-Structures)\n",
    "* [6.2 The <code class=\"kw\">typedef</code> statement](#6.2-The-typedef-statement)\n",
    "* [6.3 Structures and functions](#6.3-Structures-and-functions)\n",
    "* [6.4 Pointers to structures](#6.4-Pointers-to-structures)\n",
    "* [6.5 Self-referential structures](#6.5-Self-referential-structures)\n",
    "* [6.6 OOP in C 🤯](#6.6-OOP-in-C-🤯)\n",
    "* [6.7 Unions 🤯](#6.7-Unions-🤯)\n",
    "* [6.8 Bit fields 🤯](#6.8-Bit-fields-🤯)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python vs. C vs. C++ vs Java\n",
    "\n",
    "|           | Python          | C            | C++   | Java  |\n",
    "| :--------:| :-------------: | :----------: | :---: | :---: |\n",
    "| structures |    | `struct`   | same as C | |\n",
    "| type alias | | `typedef` | same as C | |\n",
    "| unions | | `union` | same as C | |\n",
    "| bit fields | | `:` in `struct` | same as C | | "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6.1 Structures\n",
    "\n",
    "A **structure** in C is a collection of one or more variables packaged together in a single object.  The variables may be of different types.  A C structure is a bit like a Python class but lacks class features such as methods and inheritance.  There is also no mechanism for access control or information hiding in C structures, unlike Python classes.\n",
    "\n",
    "The type of a structure is ```struct```.  Here is type of structure named ```point``` that stores the coordinates of a point in two dimensions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     1\t#include <stdio.h>\n",
      "     2\t\n",
      "     3\tint main(void)\n",
      "     4\t{\n",
      "     5\t  struct point {  /* The structure type \"point\". */\n",
      "     6\t    int x;\n",
      "     7\t    int y;\n",
      "     8\t  };\n",
      "     9\t\n",
      "    10\t  struct point p;           /* Uninitialized point structure. */\n",
      "    11\t  struct point q = {3, 4};  /* Definition + initialization: q.x = 3, q.y = 4. */\n",
      "    12\t\n",
      "    13\t  printf(\"%d %d\\n\", q.x, q.y);\n",
      "    14\t\n",
      "    15\t  q.x = 42;   /* Assignment to individual struct members. */\n",
      "    16\t  q.y = 54;\n",
      "    17\t  printf(\"%d %d\\n\", q.x, q.y);\n",
      "    18\t  \n",
      "    19\t  return 0;\n",
      "    20\t}\n"
     ]
    }
   ],
   "source": [
    "cat -n ch06/struct.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/struct.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with Python classes, the declaration of a ```struct``` is simply a template.  Instances of the ```struct``` are individually instantiated, as in line 10.\n",
    "\n",
    "C structures do not have anything like Python's ```__init__()``` method to initialize the structure.  You must do that by hand.  You can initialize a structure when it is declared, as in line 11.\n",
    "\n",
    "You can also assign values to individual members of a `struct`, as in lines 15 and 16.  The notation for accessing members of a `struct` is the same as that used in Python (and C++ and Java) for accessing elements of a class, and is almost surely something Python borrowed from C."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unlike a Python class you cannot dynamically add new variables to a C ```struct```.  For instance, Python would allow the following:\n",
    "<code>\n",
    "    class Foo:\n",
    "        def __init__(self):\n",
    "            self.a = 42\n",
    "   \n",
    "        def add_var(self):\n",
    "            self.b = 54     # Dynamically adding self.b.\n",
    "</code>\n",
    "All the variables that will ever be in a C <code>struct</code> must be declared in advance.\n",
    "\n",
    "Here is what happens if we try to add an undeclared variable to a <code>struct</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch06/bad_struct.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/bad_struct.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6.2 The ```typedef``` statement\n",
    "\n",
    "You can use a ```typedef``` to create a synonym for a type, including structures.  Here we create an alias ```Point``` for the ```struct point``` structure above, and `ulint` for `unsigned long int`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch06/typedef.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/typedef.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `typedef` statement does not create a new type.  It merely attaches another name to a type.\n",
    "\n",
    "One use of `typedef` is for clarity.  For instance, the `fopen()` function for opening files for reading and writing returns a pointer to a `FILE` structure; `FILE` is a `typedef` for a complicated structure that keeps of the i/o operations.\n",
    "\n",
    "Another use of `typedef` is to guard against portability problems due to data types that are implementation and machine dependent.  For instance, the C standard specifies [a `typedef` named `size_t`](https://en.wikipedia.org/wiki/C_data_types#stddef.h) for an integer at least 16 bits long used to used for memory-related purposes.  The `man` page for `malloc()` illustrates the use of `size_t` (the `typedef` lives in the header file `stdlib.h`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat ch04/malloc.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> \n",
    "Portability is not as big an issue these days as processors become more standardized, but is something you need to be aware of."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6.3 Structures and functions\n",
    "\n",
    "You can pass structures to and from functions just like any other variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch06/fun.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/fun.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6.4 Pointers to structures\n",
    "\n",
    "You can have pointers to structures just as any other type.  Here is the WRONG way to dereference a pointer to a structure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "cat -n ch06/pointers0.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c -Wall -pedantic ch06/pointers0.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The problem is that the dereferencing operator ```*``` is being applied to ```p.x``` and ```p.y```.  We want to dereference ```p``` first and then access its elements.\n",
    "\n",
    "In order to dereference pointers to structures properly you need to use parentheses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch06/pointers1.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/pointers1.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dereferencing pointers to structures is so common in C that there is a special syntax that is clearer and less verbose.  We use ```p->x``` (for \"points to\") to indicate ```(*p).x``` and ```p->y``` to indicate ```(*p).y```:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch06/pointers2.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/pointers2.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Passing a pointer to a structure rather than a copy of a structure is more efficient when the structure is large.  For instance, if the structure contains an array of 1,000,000 integers, a copy of the structure would require copying 1,000,000 integers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6.5 Self-referential structures\n",
    "\n",
    "Suppose we wish to implement a data structure known as a [**singly linked list**](https://en.wikipedia.org/wiki/Linked_list#Singly_linked_list) in C.  Each node in the list is a chunk of memory that holds the value being stored in the list and information about the following node.  All we need to do is keep track of the first node in the list (the **head** of the list).  In the first node we can look up the second node, in the second node we can look up the third, and so on, thereby traversing the list.  This is called a singly linked list since each node only keeps track of the next node (in a [**doubly linked list**](https://en.wikipedia.org/wiki/Doubly_linked_list) each node also keeps track of the preceding node.\n",
    "\n",
    "Here is our first attempt to implement a singly linked list.  Each node is a `struct` that contains the value being stored (an `int`) and the succeeding node:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat ch06/list0.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/list0.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Woops!  this doesn't work because the definition of `struct list_node` involves itself.  So how can we implement self-referential structures such as this in C?\n",
    "\n",
    "The solution is to use a **pointer** to the next node:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch06/list1.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/list1.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This works because C already knows the size of pointers so it knows how much space to allocate for a `LIST_NODE` and how to interpret the bytes in the `struct`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "<div class=\"danger\"></div>\n",
    "Pointers allow us to build self-referential data structures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a more developed version of the code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "cat -n ch06/list2.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/list2.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6.6 OOP in C 🤯\n",
    "\n",
    "By combining structures and function pointers we can create objects that are similar to classes in Python.\n",
    "\n",
    "Here is a `Rectangle` \"class\" for representing rectangles.  It contains two variables, `height` and `width`, and two \"methods\", `area()` and `perimeter()`.  The latter are implemented via function pointers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "cat -n ch06/oop.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lines 3 to 10 contain the definition of our class-like object.  The \"methods\" are pointers to functions; these functions are defined in lines 12 to 22.  In the initialization function `rect_init()` (lines 24 to 31) we set the function pointers to point to `rect_area()` and `rect_perimeter()`.\n",
    "\n",
    "In line 37 we provoke the methods on the object `rect`.  Unlike Python (and other object-oriented languages) we must explicitly pass the object to its methods since these are standard function calls."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/oop.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More elaborate object-oriented behavior, such as [dynamic binding](https://en.wikipedia.org/wiki/Dynamic_dispatch), can be implemented using tables of function pointers called [virtual function tables](https://en.wikipedia.org/wiki/Virtual_method_table)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6.7 Unions 🤯\n",
    "\n",
    "A **union** is a variable that can hold objects of different types and sizes in the same space.  Here is an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch06/union.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/union.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `flint` union is a chunk of memory large enough to hold either an `int` or a `float`, while the `doint` union is a chunk of memory large enough to hold either an `int` or a `double`.  Unions allow us to operate on different kinds of data in the same space.\n",
    "\n",
    "We can use a `union` to see the bits that make up variables.  Let's look at the bit sequence of a ```float```.  \n",
    "We will use the fact that (usually) in C a ```binary32``` floating point number (C ```float```) and an integer (C ```int```) are the same width:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch06/onion.c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In line 12 we set the bits in the union to those of a `float` `1.0`, while in line 14 we read those bits as an `int`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch06/onion.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Let's write out the bit sequence, nibble by nibble, translating from hexadecimal to binary:\n",
    "<pre>\n",
    "3f = 0011 1111\n",
    "80 = 1000 0000\n",
    "00 = 0000 0000\n",
    "00 = 0000 0000\n",
    "</pre>\n",
    "Putting these all together, the bit string for the 32 bit float `1.0` is\n",
    "<pre>\n",
    "00111111100000000000000000000000\n",
    "</pre>\n",
    "\n",
    "[A ```binary32``` floating point number consists of the following bits](https://en.wikipedia.org/wiki/Single-precision_floating-point_format#IEEE_754_standard:_binary32):\n",
    "<pre>\n",
    "component                no. of bits\n",
    "sign (s):                     1\n",
    "exponent + 127 (e):           8\n",
    "mantissa or significand (m): 23\n",
    "</pre>\n",
    "The exponent part of a `binary32` is the binary exponent plus a bias of 127; the shift of the exponent [simplifies the hardware needed for comparisons](https://en.wikipedia.org/wiki/Exponent_bias).\n",
    "\n",
    "The three parts of our particular ```float``` are\n",
    "<pre>\n",
    "s  e + 127             m\n",
    "0  01111111  00000000000000000000000\n",
    "</pre>\n",
    "\n",
    "There is an implicit ```1.``` in front of the mantissa that is not stored, so\n",
    "<pre>\n",
    "e = 127 - 127 = 0\n",
    "m = 1.00000000000000000000000\n",
    "</pre>\n",
    "and our `float` is $1 \\times 2^{0} = 1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6.8 Bit fields 🤯\n",
    "\n",
    "We briefly mention **bit fields**.  These are useful when space is at a premium and we have values (such as booleans) that can be represented with only a few bits.\n",
    "\n",
    "Here is an example, modeled after one K&amp;R:\n",
    "\n",
    "<code>\n",
    "    struct {\n",
    "        unsigned int is_mammal: 1;\n",
    "        unsigned int is_furry:  1;\n",
    "        unsigned int is_herbivore: 1;\n",
    "    } flags;\n",
    "</code>\n",
    "\n",
    "The variable `flags` contains three 1-bit fields; the width of each field is the number following the colon.  Each field is interpreted as an `unsigned int`.  A 1-bit field can have the values `0` and `1`, meaning we can use a 1-bit field to represent a boolean.\n",
    "\n",
    "We use the operator to access the fields, just as for a regular `struct`:\n",
    "\n",
    "<code>\n",
    "    flags.is_mammal = 1;\n",
    "    flags.is_furry = 0;\n",
    "    flags.is_herbivore = 0;\n",
    "</code>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"ch05_debugging_memory.ipynb\" style=\"float:left;\">Previous: Debugging memory errors</a> \n",
    "<a href=\"ch07_io.ipynb\" style=\"float:right;\">Next: i/o</a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
