{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e4378673",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">C Survival School</h1>\n",
    "<h1 style=\"text-align:center;\">Chapter 2</h1>\n",
    "<h1 style=\"text-align:center;\">Functions</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de7a9b82",
   "metadata": {},
   "source": [
    "<a href=\"ch01_variables_types.ipynb\" style=\"float:left;\">Previous: Variables, types, and expressions</a> \n",
    "<a href=\"ch03_flow.ipynb\" style=\"float:right;\">Next: Flow control</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e4c09ea",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [2.1 Functions](#2.1-Functions)\n",
    "* [2.2 Function prototypes](#2.2-Function-prototypes)\n",
    "* [2.3 Returning multiple values](#2.3-Returning-multiple-values)\n",
    "* [2.4 The <code class=\"kw\">void</code> type](#2.4-The-void-type)\n",
    "* [2.5 Implicit returns 😱 🐞](#2.5-Implicit-returns-😱-🐞)\n",
    "* [2.6 The <code class=\"kw\">const</code> qualifier](#2.6-The-const-qualifier)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ef1fe43",
   "metadata": {},
   "source": [
    "# Python vs. C vs. C++ vs. Java\n",
    "\n",
    "|           | Python          | C            | C++   | Java |\n",
    "| :--------:| :-------------: | :----------: | :---: | :---: |\n",
    "| return a value from a function | `return`   | same | same | same |\n",
    "| type of functions not returning a value |  | `void` | same as C | same as C |\n",
    "| value from implicit returns | `None` | | | \n",
    "| immutable variables | | `const` | same as C | |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdfbc50e",
   "metadata": {},
   "source": [
    "# Functions\n",
    "\n",
    "Functions in C/C++/Java are similar to those in Python.  As always, instead of using indentation, C/C++/Java delimit the body of a function using `{ }`.\n",
    "\n",
    "You should declare the type of a function (i.e., the type of value returned by the function) and the type of all input arguments.  The following declares `square()` to be a function taking an integer input `n` and returning an integer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c580779d",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat ch02/square.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec42663b",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/square.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f74f4ee1",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "143b6f9d",
   "metadata": {},
   "source": [
    "Omitting the return type of a function results in a warning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20260b59",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/untyped_fun.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4faf97a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "clang -c ch02/untyped_fun.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3f12658f",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch02/untyped_fun.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0cd5641b",
   "metadata": {},
   "source": [
    "The reason this is a warning rather than an error is that in the early days of C the default type of a function was `int`, and making the declaration of a function mandatory would break a lot of old code."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e937348c",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div>\n",
    "\n",
    "A warning about a missing type specifier frequently points to an error.  Ignore such warnings at your peril."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d13114e3",
   "metadata": {},
   "source": [
    "It is also an error to omit the type of an argument to a function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cea668f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/untyped_arg.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "896b3404",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/untyped_arg.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9845515c",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f51f2585",
   "metadata": {},
   "source": [
    "When Python passes an object of a fundamental type (e.g., an `int` or `float`) it passes a **copy** of the value of the variable is passed, rather than the variable itself.  This is called **call by value**.\n",
    "\n",
    "On the other hand, when Python passes more complex objects (e.g., lists, dictionaries) to functions, it passes the actual object itself, and changes made to the object in the function will persist after returning from the function.  This is called **call by reference**.\n",
    "\n",
    "The following Python code illustrates both types of calls:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b46c010",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/call_by.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f022dbff",
   "metadata": {},
   "outputs": [],
   "source": [
    "python ch02/call_by.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "904f2098",
   "metadata": {},
   "source": [
    "C is strictly call by value---only a copies of values of variables are passed to functions, as the following code illustrates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ecb61b49",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/bad_swap.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eed2b60f",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/bad_swap.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db928f0c",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0250b39e",
   "metadata": {},
   "source": [
    "When we later introduce **pointers** we will see how we can get the effect of call by reference in C."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eceb6198",
   "metadata": {},
   "source": [
    "When we exit a function the variables local to it cease to exist.  We can make values persist by using the <code class=\"kw\">static</code> qualifier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "50264a53",
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     1\t#include <stdio.h>\n",
      "     2\t\n",
      "     3\tstatic int n;\n",
      "     4\t\n",
      "     5\tvoid set_n(int m)\n",
      "     6\t{\n",
      "     7\t  n = m;\n",
      "     8\t}\n",
      "     9\t\n",
      "    10\tvoid use_n(void)\n",
      "    11\t{\n",
      "    12\t  print(\"n = %d\\n\", n);\n",
      "    13\t}\n",
      "    14\t\n",
      "    15\tvoid local(int m)\n",
      "    16\t{\n",
      "    17\t  static int n = 42;\n",
      "    18\t  printf(\"Before assignment, n = %d\\n\", n);\n",
      "    19\t  n = m;\n",
      "    20\t  printf(\"After assignment,  n = %d\\n\", n);  \n",
      "    21\t}\n",
      "    22\t\n",
      "    23\tint main(void)\n",
      "    24\t{\n",
      "    25\t  set_n(54);\n",
      "    26\t  use_n();\n",
      "    27\t\n",
      "    28\t  local(54);\n",
      "    29\t  local(42);\n",
      "    30\t  \n",
      "    31\t  return 0;\n",
      "    32\t}\n"
     ]
    }
   ],
   "source": [
    "cat -n ch02/static.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "f7e5de65",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic ch02/static.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "1e4c0b08",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello from set_n()!\n",
      "Before assignment, n = 42\n",
      "After assignment,  n = 54\n",
      "\n",
      "Hello from use_n()!\n",
      "n = 54\n",
      "\n",
      "Hello from local()!\n",
      "Before assignment, n = 0\n",
      "After assignment,  n = 1\n",
      "\n",
      "Hello from local()!\n",
      "Before assignment, n = 1\n",
      "After assignment,  n = 2\n",
      "\n"
     ]
    }
   ],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d614a4c9",
   "metadata": {},
   "source": [
    "A static variable defined outside of a function is visible to all code in the file.  A static variable defined inside of a function is local to the function, but will persist after the function exits and is available in subsequent calls.  Any initialization in the declaration of a static variable is performed **only once**.\n",
    "\n",
    "C also has global variables specified via the <code class=\"kw\">extern</code> qualifier.  Since the use of global variables can get you into trouble very quickly we won't talk about them."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac92c0a7",
   "metadata": {},
   "source": [
    "# 2.2 Function prototypes\n",
    "\n",
    "A **function prototype** is a description of a function, its return type, and its input types that is independent of the actual definition of the function (i.e., the code that makes up the function).   They allow functions to be called without the source code being seen as the compiler will know how to handle them.\n",
    "\n",
    "Function prototypes make it possible to detect mismatches between the expected and actual number and type of inputs, as well as detecting whether a returned value is being misused because of its type. \n",
    "\n",
    "Header files contain lots of function prototypes.  For instance, `stdio.h` contains prototypes for the C i/o functions, while `math.h` contains prototypes for all of the functions in the C math library.\n",
    "\n",
    "A function prototype for `foo()` appears on line 3 of the following program.  When the compiler reaches the call to `foo()` at line 7 it knows about the inputs and outputs of the function and can generate the appropriate code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb74526f",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto1.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1ee09228",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto1.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "752e6840",
   "metadata": {},
   "source": [
    "Here is an example where the function is in a different file than where it is called:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9844819b",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto2a.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e0ef9dbd",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto2b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "04e5fb32",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto2a.c ch02/proto2b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5fa9f088",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e87312da",
   "metadata": {},
   "source": [
    "In these situations it is good practice to put the function prototypes in a header file which is included everywhere it might be needed.  Since the header file is ours and not a system header file it appears in `\" \"`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b644fab7",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto3.h"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce84562e",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto3a.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d7674e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto3b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c745900",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto3a.c ch02/proto3b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ed35b19",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c97601be",
   "metadata": {},
   "source": [
    "The inclusion of the function prototype in the file where the function is defined prevents the following situation, where the actual function does not match its prototype:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3fb6378c",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto4.h"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21d60b26",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto4a.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35c5bdc2",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto4b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9802b87c",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto4a.c ch02/proto4b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ae32791",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a98d3a26",
   "metadata": {},
   "source": [
    "Before function prototypes were introduced in C one of my favorite errors was to pass the wrong number of type of inputs to functions.  Used properly, function prototypes catch these kinds of mismatches:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8563a68",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto5.h"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8796bad",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto5a.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ef042dba",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto5b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "66313d1d",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto5a.c ch02/proto5b.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "abc0468e",
   "metadata": {},
   "source": [
    "# 2.3 Returning multiple values\n",
    "\n",
    "Unlike Python, C does not have a simple way of returning multiple values.\n",
    "\n",
    "If all of the values are of the same type, we can pass an array into the function and fill it with the values we wish to return.  C passes arrays to functions the same way Python passes lists and other containers, as a reference.  This means that the array the function sees is the same array the calling code passed, rather than a copy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c706cde",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/min_max.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c8cf43c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/min_max.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8380d889",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b261292e",
   "metadata": {},
   "source": [
    "Later we will see a way of returning multiple values of different types using a type of object called a **structure**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f837637a",
   "metadata": {},
   "source": [
    "# 2.4 The `void` type\n",
    "\n",
    "If a function does not return a value, its type is <code class=\"kw\">void</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8bf0cf7c",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/void1.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f8da8f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch02/void1.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d137a0a",
   "metadata": {},
   "source": [
    "You should also use <code class=\"kw\">void</code> to indicate that a function has no input arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fedc59b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/void2.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3996a69d",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch02/void2.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "be8a8f71",
   "metadata": {},
   "source": [
    "The compiler should detect an attempt to (mis)use the result of a call to a function of type `void`, or to pass arguments to a function that has no inputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ffd5815b",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/void3.c "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8137cf94",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch02/void3.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e81b2ec",
   "metadata": {},
   "source": [
    "# 2.5 Implicit returns 😱 🐞\n",
    "\n",
    "As in Python, reaching the end of a function is implicitly a return.  However, C does not return a default value like Python's <code class=\"kw\">None</code>.  Worse yet, C will let you try to use a returned value when none was returned, in which case there is no telling what will happen.\n",
    "\n",
    "A good compiler will warn you if your code allows for returning from a function without returning a value of the appropriate type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2931ccc",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/implicit_return.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e8335c7f",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/implicit_return.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d4c34e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "clang ch02/implicit_return.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6324aeb3",
   "metadata": {},
   "source": [
    "In Python you can detect whether you've fallen off the end of a function by checking for a returned value of <code class=\"kw\">None</code>.  There is no analogous test in C.\n",
    "\n",
    "This is why you should keep the logic of your functions as simple as possible.  Complex logic makes it more likely there is a path through a function that falls off the end and surprises you at some point."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a541d8d1",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "🐞 Make sure functions that are supposed to return a value always return a value. 🐞"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ee81937",
   "metadata": {},
   "source": [
    "In this case the program runs even when we fall off the end of the function and return nothing, but produces the wrong result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2e707067",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bd2e506",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "<div class=\"danger\"></div>\n",
    "🐞 😱 If you try to use a returned value when none was returned, Very Bad Things&trade; may happen. 😱 🐞"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13808f45",
   "metadata": {},
   "source": [
    "# 2.6 The `const` qualifier\n",
    "\n",
    "We can use the qualifier <code class=\"kw\">const</code> to indicate a variable whose value cannot be changed.  You have seen this idea in the immutable types (e.g., tuples) in Python.  This qualifier is particularly useful in connection with functions to make sure there are no unintended modifications of inputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "677e0af4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/const.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4498593f",
   "metadata": {},
   "source": [
    "At line 7 we accidentally set `n` rather than `m` (and return an uninitialized value `m` with Lord only knows what effect!).  The compiler catches this since we declared `n` to be `const int n` in the definition of `foo()`.  The `const` means the value of `n` cannot change inside `foo()`.\n",
    "\n",
    "In line 14 we declare the variable `pi` to be `const`, as it is probably a good idea to ensure that the value of $\\pi$ cannot change in the midst of our computations."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "236aad2b",
   "metadata": {},
   "source": [
    "<a href=\"ch01_variables_types.ipynb\" style=\"float:left;\">Previous: Variables, types, and expressions</a> \n",
    "<a href=\"ch03_flow.ipynb\" style=\"float:right;\">Next: Flow control</a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
