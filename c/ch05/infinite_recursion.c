#include <stdio.h>

long int factorial(int n)
{
  /* No initial condition! */
  
  return n * factorial(n);
}

int main(void)
{
  printf("Stack overflow due to infinite recursion.\n\n");
  printf("42! = %ld\n", factorial(42));

  return 0;
}
  
