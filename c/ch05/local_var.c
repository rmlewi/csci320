#include <stdio.h>

char *foo(void);

int main(int arc, char **argv)
{
  printf("An illustration of trying to use a pointer that is out of scope.\n\n");
  
  char *string = foo();
  fprintf(stderr, "Value of string before change: \"%s\"\n\n", string);

  fprintf(stderr, "The pointer string points to something that ceased to exist when we returned from foo()!\n");
  string[0] = 'y';
  printf(stderr, "Value of string after change:  \"%s\"\n", string);
}

char *foo(void)
{
  char *s = (char*) "hello, world!";
  return s;  /* Danger!  *s ceases to exist when we exit! */
}
