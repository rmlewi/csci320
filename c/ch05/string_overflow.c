#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
  /*
   * The following simulates the memory layout that clang uses on a Mac for
   *
   * char foo[8]
   * unsigned int m, n;
   */
  
  void *buffer = malloc(8 * sizeof(char) + 2 * sizeof(unsigned int));
  char *foo = (char*) buffer;
  unsigned int *m = (unsigned int*) buffer + 2;
  unsigned int *n = (unsigned int*) buffer + 3;  

  printf("%p\n", (void*) foo);
  printf("%p\n", (void*) m);  

  /* Let's omit the terminating \0. */
  foo[0] = 'f';
  foo[1] = 'o';
  foo[2] = 'o';
  foo[3] = 'b';
  foo[4] = 'a';
  foo[5] = 'r';
  foo[6] = '-';  
  foo[7] = '-';

  *n = 54;
  
  *m = 0;
  printf("When *m = %u, the string is \"%s\" which is %lu bytes long.\n", *m, foo, strlen(foo));

  *m = 42;
  printf("When *m = %u, the string is \"%s\" which is %lu bytes long.\n", *m, foo, strlen(foo));

  *m = 4294967295;
  printf("When *m = %u, the string is \"%s\" which is %lu bytes long.\n", *m, foo, strlen(foo));
}
