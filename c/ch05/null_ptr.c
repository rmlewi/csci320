#include <stdio.h>

int main(void)
{
  printf("An illustration of what happens if you try to dereference the NULL pointer.\n\n");
  
  int *p = NULL;
  
  fprintf(stderr, "p = %p\n",  (void*) p);
  fprintf(stderr, "Fixing to print *p...\n");
  fprintf(stderr, "*p = %d\n", *p);

  return 0;
}
