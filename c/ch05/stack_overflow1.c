#include <stdio.h>
#include <sys/resource.h>  /* Needed for getrlimit(). */

void foo(unsigned int n);
void bar(unsigned int n, unsigned int k);

static int level = 0;

/*
 * An illustration of stack overflow due to recursion.  With each recursive call of foo(),
 * an array x[] of 400,000 bytes (100,000 int) is added to the stack.  At some point this will
 * exceed the stack size, and a stack overflow will ensue.
 *
 * On the other hand, the recursive calls to bar() do not cause a stack overflow.  This is because
 * of the use of dynamic memory allocation.  In the case of bar(), the memory to store all of the 
 * integers is allocated dynamically on the heap, rather than on the stack.
 */

int main()
{
  printf("This is another illustration of a stack overflow.\n\n");

  struct rlimit lim;
  getrlimit(RLIMIT_STACK, &lim);
         
  printf("Current stack size limit: %lu bytes.\n\n", lim.rlim_cur);

  printf("Calling bar()...\n");
  bar(30, 100000); // This call works OK!
  printf("...done!  heap allocation works!!\n\n");

  level = 0;
  
  printf("Calling foo()...\n");
  foo(30);  /* This call produces a stack overflow! */
}

void foo(unsigned int n)
{
  int x[100000]; /* This local variable is allocated on the stack every time foo() is called. */
  x[0] = 42;
  level++;
  printf("foo(), recursion level %2d, %7lu bytes on stack...\n", level, level * 100000 * sizeof(int));
  if (n == 0) return;
  foo(n-1);
}

void bar(unsigned int n, unsigned int k)
{
  int *n = malloc(k * sizeof(int));
  level++;
  printf("bar(), recursion level %2d, %7lu bytes on stack...\n", level, level * k * sizeof(int));  
  if (n == 0) return;
  bar(n-1, k);
  free(n);
}

