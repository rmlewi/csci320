#include <stdlib.h>
#include <stdio.h>

void foo();

int main(int arc, char **argv)
{
  printf("An illustration of dangling pointers.\n\n");

  int *a, *b, *c;

  a = (int*) malloc(42 * sizeof(int));
  b = a;

  free(a);

  c = (int*) malloc(42 * sizeof(int));
  c[0] = 6;
  printf("c[0] = %2d, c[1] = %2d\n\n", c[0], c[1]);  

  a[0] = 42;  /* Hey!  we freed a! */
  b[1] = 54;  /* Oops!  b ends up pointing to a freed chunk of memory! */

  printf("a[0] = %2d, b[1] = %2d\n\n", a[0], b[1]);
  printf("c[0] = %2d, c[1] = %2d\n", c[0], c[1]);

  return 0;
}
