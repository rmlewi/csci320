#include <stdlib.h>
#include <stdio.h>

void foo();

int main(void)
{
  printf("An illustration of a memory leak.\n\n");

  foo();
  foo();

  return 0;
}

void foo(void)
{
  int *a;

  /*
   * Ack!  the following memory remains allocated but it inaccessible
   * once we return from the function!
   */
  
  a = (int*) malloc(42 * sizeof(int));
}
