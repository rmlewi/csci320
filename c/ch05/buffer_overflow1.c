#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *gets(char *s);

/*
 * From the gets() man page:
 *
 * Never use gets().  Because it is impossible to tell without knowing the data in advance how many
 * characters gets() will read, and because gets() will continue to store characters past the end of 
 * the buffer, it is extremely dangerous to use.  It has been used to break computer security.
 * Use fgets() instead.
*/

int main(int argc, char **argv)
{
  printf("An illustration of a buffer overflow exploit and the danger of gets().\n\n");
  
  int  pass;
  char buff;

  /* With gcc on the CS systems pass is one byte past buff.. */
  printf("address of buff: %p\n", (void*) &buff);
  printf("address of pass: %p\n", (void*) &pass);
  /* printf("pass - buff: %ld bytes\n\n", (void*) &pass - (void*) &buff); */

  pass = 0;
  printf("pass: %d\n\n", pass);  

  printf("A password of more than one character goes past the end of buff and makes pass nonzero.\n");
  printf("Try 7 and 77.\n");
  printf("Entering 7 one writes '\\0' to the first byte of pass,"
         "so pass remains False and you are locked out.\n");
  printf("Entering 77 writes '7' to the first byte of pass,"
         "so pass is True and you are authenticated.  Oops!\n\n");

  printf("Enter your password: ");
  gets(&buff);  /* gets() is double-plus unsafe. */

  /* x86-64 is little endian, so '77' sets the low order byte of pass to '7' = 55. */
  printf("pass: %d\n", pass);
  printf("'%c':  %d\n", *(&buff+1), *(&buff+1));

  if (strcmp(&buff, "foo")) {
    printf("The password entered does NOT match the superuser password!\n");
  }
  else {
    printf("The password entered matches the superuser password!\n");
    pass = 1;
  }

  /* Give fake superuser privileges to the user. */  
  if (pass) {
    printf("\n\a");
    printf("*********************************************\n");
    printf("* You are now authenticated as a superuser! *\n");
    printf("*********************************************\n\n");
  }

  return 0;
}
