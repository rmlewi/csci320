#include <stdlib.h>
#include <stdio.h>

int main(void)
{
  printf("Illustration of a buffer overflow.\n\n");
  
  long int *a, *b;
  long int i;

  /* Buffer b follows buffer a in memory. */
  a = (long int*) malloc(1 * sizeof(long int));
  b = (long int*) malloc(1 * sizeof(long int));
  i = b - a;

  printf("a = %p\n", (void*) a);
  printf("b = %p\n", (void*) b);
  printf("b - a = %ld\n", b - a);
  printf("b[0] is %ld long ints (%ld bytes) past a[0]\n\n", i, i*sizeof(long int));
  
  a[i] = 42;   /* Same location as b[0]! */
  printf("b[0] = %ld\n", b[0]);
  
  b[-i] = 54;  /* Same location as a[0]! */
  printf("a[0] = %ld\n", a[0]);

  return 0;
}
