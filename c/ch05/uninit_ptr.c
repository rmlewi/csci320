#include <stdio.h>

void foo(void)
{
  int n = 42;
}

void bar(void)
{
  int *p;

  fprintf(stderr, "p = %p\n",  (void*) p);
  fprintf(stderr, "Fixing to print *p...\n");
  fprintf(stderr, "*p = %d\n", *p);
}

int main(void)
{
  printf("An illustration of what can happen if you dereference an uninitialized pointer.\n\n");

  foo();
  bar();

  return 0;
}
