/* An illustration of memory access errors involving stack variables. */

#include <stdio.h>

int sum(int *a, int n)
{
  int sum = 0;
  for (int i = 0; i < n; i++) {
    sum += a[i];
  }
  return sum;
}

int main(void)
{
  int a[42];
  int n = sum(a, 43);
  printf("sum: %d\n", n);
  
  return 0;
}
