/* An illustration of an error involving an out-of-bounds stack variable. */

#include <stdio.h>

int main(int argc, char **argv)
{
  int a[42] = {1};
  for (int i = 0; i < 42; i++) {
    a[i] = i;
    printf("%d ", a[i]);
  }
  printf("\n");
  printf("Returning a[%d]...\n", argc + 42);
  return a[argc + 42];
}
