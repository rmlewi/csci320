/* An illustration of using an uninitialized stack variable. */

#include <stdio.h>

int sum(int *a, int n)
{
  int sum = 0;
  for (int i = 0; i < n; i++) {
    sum += a[i];
  }
  return sum;
}

int main(void)
{
  int a[42];
  return sum(a, 42);
}
