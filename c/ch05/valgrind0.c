/* An illustration of bugs that valgrind does and does not detect. */

#include <stdio.h>
#include <stdlib.h>

int *bar(void)
{
  /* Return a partially uninitialized array. */
  int *a = (int*) malloc(4 * sizeof(int));
  for (int i = 2; i < 4; i++) {
    a[i] = i * i;
  }
  return a;
}

int main(int argc, char **argv)
{
  int *a;
  int n;

  /* Bug: some of the entries in the array n are uninitialized. */
  a = bar();
  for (int i = 0; i < 4; i++) {
    printf("a[%d] = %d\n", i, a[i]);
  }

  /* Bug: writing outside an allocated block. */
  for (int i = -1; i < 5; i++) {
    a[i] = i * i;
  }  

  /* Bug: Reading outside an allocated block. */
  for (int i = -1; i < 6; i++) {
    n = a[i];
  }  
  
  /* Bug: memory leak since a was not freed. */
  return 0;
}

