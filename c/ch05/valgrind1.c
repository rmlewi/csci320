/* An illustration of bugs that valgrind does and does not detect. */

#include <stdio.h>
#include <stdlib.h>

int *bar(void)
{
  int *a = (int*) malloc(4 * sizeof(int));
  for (int i = 0; i < 4; i++) {
    a[i] = i * i;
  }
  return a;
}

int main(int argc, char **argv)
{
  int *a;
  int n;

  a = bar();
  for (int i = 0; i < 4; i++) {
    printf("a[%d] = %d\n", i, a[i]);
  }

  /* Bug: writing outside an allocated block. */
  for (int i = 0; i < 5; i++) {
    a[i] = i * i;
  }  

  /* Bug: Reading outside an allocated block. */
  for (int i = 0; i < 6; i++) {
    n = a[i];
  }  
  
  free(a);
    
  return 0;
}

