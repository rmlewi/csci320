#include <stdio.h>

int main(int argc, char **argv)
{
  int n = 42;

  while (n < 54) {
    printf("%d\n", n);
    n++;  /* Increment n by 1. */
  }
 
  return 0;
}
