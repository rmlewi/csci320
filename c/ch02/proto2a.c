#include <stdio.h>

int foo(int n);  /* Function prototype. */

int main(int argc, char **argv)
{
  printf("2 + 2 = %d\n", foo(2));
  return 0;
}
