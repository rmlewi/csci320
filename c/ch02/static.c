#include <stdio.h>

static int n = 42;  /* Visible at file scope (i.e., to all code in this file. */

void set_n(int m)
{
  printf("Hello from set_n()!\n");
  printf("Before assignment, n = %d\n", n);
  n = m;
  printf("After assignment,  n = %d\n\n", n); 
}

void use_n(void)
{
  printf("Hello from use_n()!\n");
  printf("n = %d\n\n", n);
}

void local(int m)
{
  static int n = 0;  /* This n is local to this function, but static. */
  
  printf("Hello from local()!\n");
  printf("Before assignment, n = %d\n", n);
  n = m;
  printf("After assignment,  n = %d\n\n", n);  
}

int main(void)
{
  set_n(54);
  use_n();

  local(1);
  local(2);
  
  return 0;
}
