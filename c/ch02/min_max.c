#include <stdio.h>

void min_max(const int a[], const int n, int minmax[2]);

int main(void)
{
    int minmax[2];
    int a[] = {1, 2, 3, 4, 5};
    int n = 5;

    min_max(a, n, minmax);

    printf("min: %d, max: %d\n", minmax[0], minmax[1]);

    return 0;
}

void min_max(const int a[], const int n, int minmax[2])
{
    int min, max;
    min = max = a[0];
    for (int i = 0; i < n; i++) {
        if (a[i] < min) min = a[i];
        if (a[i] > max) max = a[i];
    }
    minmax[0] = min;
    minmax[1] = max;
}
