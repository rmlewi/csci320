# Illustration of call by value and call by reference in Python.

def foo(a, b):
    a = 54
    b[0] = 42

a = 42
b = [1, 2, 3, 4]

print(f"before call to foo(): {a = }, {b = }")

# a will be passed as call by value (a copy of a), while
# b will be passed as call by reference (b itself).

foo(a, b)

print(f"after call to foo():  {a = }, {b = }")
