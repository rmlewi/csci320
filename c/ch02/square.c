#include <stdio.h>

int square(int n)  /* Our first function. */
{
  return n * n;
}

int main(int argc, char **argv)
{
  int n = 42;
  printf("%d**2 = %d\n", n, square(n));

  return 0;
}
