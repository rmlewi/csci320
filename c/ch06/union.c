#include <stdio.h>

int main(void)
{
  union flint {
    int n;
    float x;
  } u;

  union doint {
    int n;
    double x;
  } v;

  printf("sizeof(flint): %lu\n", sizeof(u));
  printf("sizeof(doint): %lu\n", sizeof(v));
  
  u.x = 1.0;
  v.n = 42;
    
  return 0;
}
