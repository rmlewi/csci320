#include <stdio.h>

typedef struct {
  int x;
  int y;
} Point;

void foo(Point *p)
{
  /* This is correct but awkward. */
  printf("x: %d, y: %d\n", (*p).x, (*p).y);
}

int main(void)
{
  Point q = {42, 54};

  foo(&q);
  
  return 0;
}
