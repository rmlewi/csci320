#include <stdio.h>

int main(void)
{
  union flint {
    int n;
    float x;
  } u;

  printf("sizeof(flint): %lu\n", sizeof(u));

  u.x = 1.0;

  printf("bytes of x: %x\n", u.n);
  
  return 0;
}
