#include <stdlib.h>
#include <stdio.h>

typedef struct {
  int x;
  int y;
} Point;

void foo(Point p)
{
  printf("x: %d, y: %d\n", p.x, p.y);
}

Point bar(void)
{
  Point p = {42, 54};
  return p;
}

int main(void)
{
  Point q = bar();
  foo(q);
  
  return 0;
}
