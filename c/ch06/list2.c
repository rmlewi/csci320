#include <stdlib.h>
#include <stdio.h>

typedef struct list_node {
  int value;
  struct list_node *next;
} LIST_NODE;

LIST_NODE *nalloc(int value)
{
  /* Allocate a LIST_NODE and initialize it. */
  LIST_NODE *node = malloc(sizeof(LIST_NODE));

  node->value = value;
  node->next = NULL;

  return node;
}

void append(LIST_NODE *head,  /* The head of the list. */
	    int value)        /* The value being appended. */
{
  /* Append to the end of a list. */
  
  LIST_NODE *node;

  /* Advance to the end of the list. */
  for (node = head; node->next; node = node->next) {}

  /* Add a node at the end. */
  node->next = nalloc(value);
}

void print_list(LIST_NODE *head)
{
  LIST_NODE *node;

  /* Traverse the list until we encounter the NULL at the end. */
  for (node = head; node; node = node->next) {
    printf("%d ", node->value);
  }
}

int main(void)
{
  LIST_NODE *head;

  head = nalloc(42);
  append(head, 0);
  append(head, 1);  
  append(head, 2);  
  append(head, 54);

  print_list(head);
  
  return 0;
}
