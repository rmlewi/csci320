#include <stdlib.h>
#include <stdio.h>

typedef struct {
  int x;
  int y;
} Point;

void foo(Point *p)
{
  /* This is the correct C idiom. */
  printf("x: %d, y: %d\n", p->x, p->y);
}

int main(void)
{
  Point *q = malloc(sizeof(Point));

  q->x = 42;  /* More structure pointer magic! */
  q->y = 54;

  foo(q);
  
  return 0;
}
