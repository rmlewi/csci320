#include <stdio.h>

/* The "class" definition. */
typedef struct rectangle {
  float height;
  float width;

  float (*area)(const struct rectangle);
  float (*perimeter)(const struct rectangle);
} Rectangle;

/* This function will be added as a "method". */
float rect_area(const Rectangle rect)
{
  return rect.height * rect.width;
}

/* This function will be added as a "method". */
float rect_perimeter(const Rectangle rect)
{
  return 2 * rect.height + 2 * rect.width;
}

/* Set up the Rectangle object. */
void rect_init(Rectangle *const rect, const float height, const float width)
{
  rect->height = height;
  rect->width  = width;
  rect->area   = rect_area;
  rect->perimeter = rect_perimeter;
}

void foo(Rectangle rect)
{
  /* Observe we have to explicitly pass the object to its "methods". */
  printf("height: %.2f, width: %.2f\n", rect.height, rect.width);
  printf("area: %.2f, perimeter: %.2f\n", rect.area(rect), rect.perimeter(rect));
}

int main(void)
{
  Rectangle rect;
  rect_init(&rect, 42, 54);
  foo(rect);
}
