#include <stdio.h>

int main(void)
{
  struct point {
    int x;
    int y;
  };

  struct point q = {3, 4};

  q.z = 42;  /* Can't add a new member dynamically! */

  return 0;
}
