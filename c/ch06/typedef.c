#include <stdio.h>

int main(void)
{
  typedef unsigned long int ulint;
  
  typedef struct {
    int x;
    int y;
  } Point;

  ulint n = 42;
  Point q = {42, 54};

  printf("n: %ld\n", n);
  printf("x: %d, y: %d\n", q.x, q.y);
  
  return 0;
}
