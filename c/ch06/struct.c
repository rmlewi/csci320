#include <stdio.h>

int main(void)
{
  struct point {  /* The structure type "point". */
    int x;
    int y;
  };

  struct point p;           /* Uninitialized point structure. */
  struct point q = {3, 4};  /* Definition + initialization: q.x = 3, q.y = 4. */

  printf("%d %d\n", q.x, q.y);

  q.x = 42;   /* Assignment to individual struct members. */
  q.y = 54;
  printf("%d %d\n", q.x, q.y);
  
  return 0;
}
