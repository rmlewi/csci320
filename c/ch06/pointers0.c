#include <stdio.h>

typedef struct {
  int x;
  int y;
} Point;

void foo(Point *p)
{
  /* This won't work since *p.x is the same as *(p.x). */
  printf("x: %d, y: %d\n", *p.x, *p.y);
}
