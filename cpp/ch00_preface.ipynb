{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b48c8dcd",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">C++ Survival School</h1>\n",
    "<h2 style=\"text-align:center;\">Robert Michael Lewis</h2>\n",
    "<h1 style=\"text-align:center;\">Getting started</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ba3d581",
   "metadata": {},
   "source": [
    "Copyright © 2022 Robert Michael Lewis.  All rights reserved.\n",
    "\n",
    "Permission is granted to copy, distribute, and/or modify these notes under the terms of the Creative Commons Attribution-NonCommercial 3.0 Unported License, which is available at http://creativecommons.org/licenses/by-nc/3.0/."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "636db051",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [About this course](#course)\n",
    "* [References](#References-on-C++)\n",
    "* [About C++](#C++)\n",
    "* [&iexcl;Cuidado, llamas!](#llamas)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0db27c1a",
   "metadata": {},
   "source": [
    "# About this course <a id=\"course\"/>\n",
    "\n",
    "This is an introduction to C++ designed for someone who has seen Python and C"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bab1ad0",
   "metadata": {},
   "source": [
    "# References on C++\n",
    "\n",
    "Because C++ changes frequently it is difficult for textbook writers to keep up.  Here are some references I have found useful:\n",
    "* The C++ Programming Language, 4th ed., by Bjarne Stroustrup.  This is a solid if idiosyncatic presentation of C++ by the man who originally developed the language.  This book is based on the 2011 revision of C++.\n",
    "* C++ Primer, 5th ed. by Stanley Lippman, Josée Lajoie, and Barbara Moo.  Another solid reference, also covering the 2011 revision of C++.\n",
    "* C++17 - The Complete Guide, 1st ed., by Nicolai M. Josuttis.  This covers the 2017 revision of the language.\n",
    "* The C++ Standard Library, 2nd ed., by Nicolai M. Josuttis.  This book gives the details of the C++ Standard Template Library (STL).\n",
    "* C++ Templates, 2nd. ed., by David Vandevoorde, Nicolai M. Josuttis, and Douglas Gregor.  This covers the 2017 revision of C++, and explains the often mysterious C++ template process."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cf9d20ec",
   "metadata": {},
   "source": [
    "# About C++ <a id=\"C\"/>\n",
    "\n",
    "It is fair to say that C++ is a superset of the language C.  Strictly speaking this is not true, as there are a few things you can do in C that are not valid in C++. You can read more about C/C++ compatibility [here](https://en.wikipedia.org/wiki/Compatibility_of_C_and_C%2B%2B).\n",
    "\n",
    "It is also fair to say that C++ is probably the most complicated language ever created.  By way of comparison, the current C++ standard, [ISO 14882:2020](https://www.iso.org/standard/79358.html), is 1853 pages long, while the current C standard, [ISO 9899:2018](https://www.iso.org/standard/74528.html), is only 520 pages long.\n",
    "\n",
    "For one thing, C++ continues to grow, undergoing fairly frequent, significant revisions.  Since the original C++ standard was codified in 1998 (C++98) there were revisions in 2003 (C++03), 2011 (C++11), 2014 (C++14), 2017 (C++17), and 2020 (C++20).  There is another revision planned for 2023.  Each revision adds to the complexity of C++, which in turn leads to later revisions that add to the complexity of C++.\n",
    "\n",
    "<blockquote>\n",
    "All new features added to C++ are intended to fix previously new features added to C++. <br/> &ndash; David Jameson\n",
    "</blockquote>\n",
    "\n",
    "In these notes we will restrict ourselves to core language features that seem unlikely to change.\n",
    "\n",
    "C++ elicits strong reactions; you might be interested in this [collection of quotes about C++](http://harmful.cat-v.org/software/c++/).  Here are some of my favorites:\n",
    "* If C++ has taught me one thing, it’s this: Just because the system is consistent doesn’t mean it’s not the work of Satan. &ndash; Andrew Plotkin \n",
    "* If you think C++ is not overly complicated, just what is a protected abstract virtual base pure virtual private destructor and when was the last time you needed one? &ndash; Tom Cargill\n",
    "* I invented the term Object-Oriented, and I can tell you I did not have C++ in mind. &ndash; Alan Kay\n",
    "* It has been discovered that C++ provides a remarkable facility for concealing the trival details of a program &ndash; such as where its bugs are. &ndash; David Keppel\n",
    "* C++ is an insult to the human brain. &ndash; Niklaus Wirth \n",
    "* C++: an octopus made by nailing extra legs onto a dog. &ndash; Steve Taylor\n",
    "* Whenever I solve a difficult problem with C++, I feel like I’ve won a bar fight. &ndash; Michael Fogus\n",
    "\n",
    "That said, C++ is manageable if you stick with a reasonable subset of the language and avoid its many dark corners."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f46f25c",
   "metadata": {},
   "source": [
    "# &iexcl;Cuidado!  &iexcl;llamas!  <a id=\"llamas\"/>\n",
    "\n",
    "<div class=\"danger\"></div> We will use the metric road sign for dangerous curves (also known as the <a href=\"https://en.wikipedia.org/wiki/Bourbaki_dangerous_bend_symbol\">Bourbaki dangerous bend symbol</a>) to indicate important information such as common pitfalls and mistakes.  Two such signs will be used to indicate especially important things.\n",
    "\n",
    "We will use the exploding head emoticon, &#x1f92f;, to indicate in-depth topics.\n",
    "\n",
    "We will use the sceam emoticon, 😱 , to indicate particularly frightening topics.\n",
    "\n",
    "Discussions of things that lead to programming errors (\"bugs\") are marked with a 🐞."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
