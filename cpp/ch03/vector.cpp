#include <array>
#include <iomanip>      // std::setw
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::setw;

int main(int argc, char **argv)
{
  std::vector<int> v(3);  // A vector of 3 ints.

  for (int i = 0; i < 3; i++) {
    v[i] = i;
  }

  cout << "         ";
  cout << "v.size() = " << setw(3) << v.size() << "  ";
  cout << "v.capacity() = " << setw(3) << v.capacity() << '\n';

  for (int i = 4; i <= 128; i++) {
    v.push_back(i);
    cout << "i = " << setw(3) << i << "  ";
    cout << "v.size() = " << setw(3) << v.size() << "  ";
    cout << "v.capacity() = " << setw(3) << v.capacity() << '\n';
  }
  cout << '\n';  
  
  return 0;
}
