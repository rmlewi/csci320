#include <algorithm>  // Needed for std::sort().
#include <iostream>
#include <random>     // Needed for the Mersenne Twister.

int main(void)
{
  std::vector<int> v(10);

  // Fill v with random integers.
  std::mt19937 e(0); // America's favorite pseudo-random number engine: the Mersenne twister.
  std::uniform_int_distribution<int> u(1, 10); // Convert engine output to a uniform integer distribution.
  for (auto &w : v) {
    w = u(e);
  }

  std::cout << "Before sort():" << '\n';
  for (auto &w : v) {
    std::cout << w << ' ';
  }
  std::cout << "\n\n";

  // Pass iterators indicating the range of values to sort.
  std::sort(v.begin(), v.end());

  std::cout << "After sort():" << '\n';  
  for (auto &w : v) {
    std::cout << w << ' ';
  }
  std::cout << "\n";  
    
  return 0;
}
