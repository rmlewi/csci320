#include <array>
#include <iomanip>      // std::setw
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::setw;

int main(int argc, char **argv)
{
  std::array<double, 42> a; // An array of 42 doubles.
  int i;

  i = 1;
  for (auto &e : a) {
    e = i++;
    cout << e << " ";
  }
  cout << '\n';
  
  return 0;
}
