#include <iostream>
#include <map>
#include <string>

using std::cout;

int main(int argc, char **argv)
{
  std::map<std::string, int> dict;

  dict["fee"] = 42;
  dict["fie"] = 54;
  dict["foe"] =  9;
  dict["fum"] =  6;

  // Range-for.
  for (auto &[key, value] : dict) {
    cout << key << " : " << value << '\n';
  }
  cout << '\n';
    
  return 0;
}
