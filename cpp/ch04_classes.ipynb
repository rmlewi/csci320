{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1c7e450d",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">C++ Survival School</h1>\n",
    "<h1 style=\"text-align:center;\">Chapter 4</h1>\n",
    "<h1 style=\"text-align:center;\">Classes</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8bcc386",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [4.1 An `array` class](#4.1-An-array-class)\n",
    "    * [4.1.1 Namespaces](#4.1.1-Namespaces)\n",
    "    * [4.1.2 Information hiding](#4.1.2-Information-hiding)\n",
    "    * [4.1.3 Constructors](#4.1.3-Constructors)\n",
    "    * [4.1.4 Destructors](#4.1.4-Destructors)\n",
    "    * [4.1.5 Class members](#4.1.5-Class-members)\n",
    "    * [4.1.6 Accessing class variables and methods](#4.1.6-Accessing-class-variables-and-methods)\n",
    "* [4.2 Operator overloading](#4.2-Operator-overloading)\n",
    "* [4.3 Copy semantics](#4.3-Copy-semantics)\n",
    "* [4.4 More operator overloading](#4.4-More-operator-overloading)\n",
    "* [4.5 Move semantics](#4.5-Move-semantics)\n",
    "* [4.6 Templated classes](#4.6-Templated-classes)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d970172c",
   "metadata": {},
   "source": [
    "# 4.1 An `array` class\n",
    "\n",
    "Let's jump into the topic of classes with an example.  As in Python, in C++ classes are objects that package data and functions together.\n",
    "\n",
    "Let's develop a ```array``` class that implements one-dimensional (singly indexed) arrays as objects.  We will start with a very rudimentary version and add functionality.  We will also place it in a separate namespace so we do not conflict with other things named `array`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "598d64b5",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/array0.cpp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efccf2d3-6306-4ec2-98e6-be9457a54140",
   "metadata": {},
   "source": [
    "Let's walk through this code in order."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e584d261",
   "metadata": {},
   "source": [
    "# 4.1.1 Namespaces\n",
    "\n",
    "We will place this class in the `rml` namespace.  When we use the class in the declaration of a variable, as we do in `main`, we refer to the class as `rml::array`.  This distinguishes our class from, say, the `array` class in the C++ STL, which would be `std::array`.\n",
    "\n",
    "When we compile the code we see that namespace appears in the names of the class methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d6bcfd6-10be-454f-a023-d212d3c09bbc",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "g++ -c -std=c++17 ch04/array0.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19de79b8-ec1e-47d3-9c87-ca0f8ba84b63",
   "metadata": {},
   "outputs": [],
   "source": [
    "nm array0.o | c++filt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9484c359-5850-43c8-9086-e9590b5cbfeb",
   "metadata": {},
   "source": [
    "# 4.1.2 Information hiding\n",
    "\n",
    "A C++ class has three types of elements (variables and methods) that differ in their accessibility to the outside world, **public**, **private**, and **protected** (Python classes have public and private elements, but not protected ones):\n",
    "\n",
    "**Public elements:**\n",
    "Public elements are accessible from outside the class. Public variables can be read and modified and public methods can be invoked by anyone.  In the preceding example all of the class variables and methods are public.\n",
    "\n",
    "**Private elements:**\n",
    "Private elements are not accessible from outside the class. Private variables can only be accessed via methods in the class and private methods cannot be invoked outside of class methods.  Private elements are not accessible even to child classes (derived classes).  **By default, variables and methods in a class are private.**\n",
    "\n",
    "**Protected elements:** Protected elements are not accessible from outside the class, but can be accessed by derived classes.\n",
    "\n",
    "Each of\n",
    "```\n",
    "public:\n",
    "private:\n",
    "protected:\n",
    "```\n",
    "sets the type for the variables and methods that follow.  You may have as many sections of each type as you want, but my preference is to have only one section of each type, or possibly two sections of each type, one for variables and one for methods.\n",
    "\n",
    "If you try to access a private variable or method from outside the class you will encounter an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3db7106f",
   "metadata": {},
   "outputs": [],
   "source": [
    "g++ -std=c++17 -c ch04/array0.cpp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c3f480e",
   "metadata": {},
   "source": [
    "# 4.1.3 Constructors\n",
    "\n",
    "The method\n",
    "```\n",
    "    Array(const int length) : n{length} {\n",
    "      a = new double[n];  // Allocate a buffer for n doubles.\n",
    "    };\n",
    "```\n",
    "is a special method called a **constructor**.  These play a role similar to `__init__()` in Python and are called to create new instances of the class.\n",
    "\n",
    "Constructors have the same name as the class they construct.  You can have multiple constructors that take different inputs (i.e., constructors can be overloaded, just like regular functions).\n",
    "\n",
    "In the declaration\n",
    "```\n",
    "  rml::array a(42);\n",
    "```\n",
    "in `main()` the constructor is called with `n = 42`.\n",
    "\n",
    "The syntax in our constructor,\n",
    "```\n",
    "array(const int length) : n{length}\n",
    "```\n",
    "is peculiar to constructors.  This is called an **initialization list** and sets `n` to `length`.  Here it is done purely for illustration but in some situations, such as initializing a `const` variable, initialization must be done in this manner.  The use of `{}` in this manner was introduced in C++11; you can also use `()` but this does not check for narrowing conversions.\n",
    "\n",
    "Notice that unlike Python, we do not need to use anything like `self`.  When we refer to `n`, because `n` is not a local variable C++ concludes we mean the member `n` of the `rml::array` object we are initializing (`n` is declared in the private section of the class).\n",
    "\n",
    "If you absolutely, positively must access the object that is invoking a method, C++ provides the ```this``` C-style pointer which points to the object.  The ```this``` pointer is similar to what is conventionally called ```self``` in Python.  However, in C++ you do not need to include ```this``` as an argument to methods &ndash; it just magically is available (in truth, C++ takes care of passing it as an argument).  So we could (needlessly) write \n",
    "```\n",
    "array(int length)\n",
    "{\n",
    "  this->n = length;  // Don't do this.\n",
    "}\n",
    "```\n",
    "\n",
    "Finally, the use of the `const` qualifier for `length` makes `length` immutable, just as in C."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21fcd403-89ea-481d-812d-1d28546e42f7",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 4.1.4 Destructors\n",
    "\n",
    "Our constructor allocates space for `n` doubles:\n",
    "```\n",
    " a = new double[n];\n",
    "```\n",
    "In C++ the `new` operator is used for allocating C-style pointers (we will discuss pointers C++ later).\n",
    "\n",
    "Having allocated this space when we create an `rml::array`, we need to free it when the array passes out of scope since otherwise we will have a memory leak.  This is done in the method\n",
    "```\n",
    "    ~array() {\n",
    "      delete[] a;\n",
    "    }\n",
    "```\n",
    "This method is called a **destructor**.  The destructor is called when an object goes out of scope and typically is used to release any resources allocated to the object.  Destructor calls are automatically inserted where appropriate and it is uncommon in my experience to explicitly call a destructor.\n",
    "\n",
    "C++ uses `delete` to deallocate C-style pointers.  Depending on whether you allocate a single item or an array of items the syntax of `delete` differs:\n",
    "```\n",
    "// A single double.\n",
    "a = new double;\n",
    "delete a;\n",
    "```\n",
    "versus\n",
    "```\n",
    "// An array of doubles.\n",
    "a = new double[2];\n",
    "delete[] a;\n",
    "```\n",
    "Valgrind will catch the error if you use the wrong version of `delete`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "348891b9-0d96-4185-bbb3-16e3fbb99e1d",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 4.1.5 Class members\n",
    "\n",
    "In the `private` section we have the declarations of two variables inside each `rml::array`:\n",
    "```\n",
    "  private:\n",
    "    long int n;  // The length of the array.\n",
    "    double *a;   // The memory storing the array.\n",
    "```\n",
    "\n",
    "Inside class methods, any reference to `n` or `a` will by default refer to these variables.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa38d866-b671-408d-b806-cd6221cc96d6",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 4.1.6 Accessing class variables and methods\n",
    "\n",
    "Accessing class variables and methods uses the same `.` syntax as C structures and Python classes:\n",
    "```\n",
    "int main(int argc, char **argv)\n",
    "{\n",
    "  rml::array a(42);\n",
    "\n",
    "  for (int i = 0; i < a.n; i++) {\n",
    "    a.a[i] = i;\n",
    "}\n",
    "```\n",
    "Of course, because the variables are currently private these accesses are invalid."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0d09f2d-503b-4efd-8e46-3c87e1f15b28",
   "metadata": {},
   "source": [
    "# 4.2 Operator overloading\n",
    "\n",
    "Let's make our class usable by adding some methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ef10bb32-7528-42bd-b46e-5c3dce05e235",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/array1.cpp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f678b6ea-1b86-49bf-8417-3dac3b64143d",
   "metadata": {},
   "source": [
    "The method\n",
    "```\n",
    "long int length(void) {\n",
    "    return n;\n",
    "}\n",
    "```\n",
    "is straightfoward; it allows the outside world to see (but not change!) the length of the array.\n",
    "\n",
    "The method\n",
    "```\n",
    "double &operator[](long int i) const {\n",
    "  return a[i];\n",
    "}\n",
    "```\n",
    "is an example of **operator overloading**, which you have seen in Python.  Here we customize the meaning of the `[]` operator for array access.  We return a `double&` reference to `a[i]`; because we return a reference  we can both write to and read from the `double` referred to, as you can see in `main()`.\n",
    "\n",
    "The `const` that follows the function's name and calling sequence promises that the function will not alter the `rml::array` that invoked it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8c76d38-8b96-4a40-b06a-5113c614dfad",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "g++ -std=c++17 ch04/array1.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13d86861-d32e-4ec4-8110-14e95003aa48",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3a09b03",
   "metadata": {},
   "source": [
    "# 4.3 Copy semantics\n",
    "\n",
    "Let's add some more functionality to our class that involves **copy semantics**.  We will create a constructor that copies another instance of `rml::array`, and we will overload the `=` operator so that assignment copies the right-hand side:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1a3f230",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/array2.cpp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5eab8146",
   "metadata": {},
   "source": [
    "We have introduced a **copy constructor** which creates a new `rml::array` by copying another instance.  We pass a reference to the `rml::array` we are copying to avoid the cost of making a copy for call-by-value:\n",
    "```\n",
    "// Copy constructor.\n",
    "array(const rml:array &other) : n{other.n} {\n",
    "  a = new double[n];  // Allocate a buffer for n doubles.\n",
    "  for (int i = 0; i < n; i++) {\n",
    "    a[i] = other[i];\n",
    "  }\n",
    "}\n",
    "```\n",
    "\n",
    "We have also overloaded the assignment operator `=` so that it copies the right-hand side.  We use `this`, which points to the invoking `rml::array`, twice in this method, once to check whether the left-hand and right-hand sides are the same (so there is not need to copy anything) and also when we need to return ourself as the result of the assignment:\n",
    "```\n",
    "// Copy assignment.\n",
    "rml::array &operator=(const rml::array &other)\n",
    "{\n",
    "  // Check for self-assignment.\n",
    "  if (this == &other) {\n",
    "    return *this;\n",
    "  }\n",
    "\n",
    "  // Throw an exception if the arrays are not the same length.\n",
    "  if (n != other.n) {\n",
    "    throw std::runtime_error(\"Attempted assignment of arrays of different lengths!\");\n",
    "  }\n",
    "\n",
    "  for (int i = 0; i < n; i++) {\n",
    "    a[i] = other[i];\n",
    "  }\n",
    "      \n",
    "  return *this;\n",
    "}    \n",
    "```\n",
    "\n",
    "Finally, we have overloaded the output operator `<<`\n",
    "```\n",
    "std::ostream &operator<<(std::ostream &os, rml::array &a)\n",
    "{\n",
    "  for (int i = 0; i < a.length(); i++) {\n",
    "    os << a[i] << ' ';\n",
    "  }\n",
    "  os << std::endl;\n",
    "\n",
    "  return os;  // An ostream is an output stream.\n",
    "}\n",
    "```\n",
    "Note that this is not a method of `rml::array`.  Instead, when C++ encounters \n",
    "```\n",
    "an_ostream << an_rml::array\n",
    "```\n",
    "it looks for a function `&operator<<` that takes an `std::ostream` and `rml::array` as its input, and calls it.\n",
    "\n",
    "Our `main()` is\n",
    "```\n",
    "int main(int argc, char **argv)\n",
    "{\n",
    "  rml::array a(42), b(54);\n",
    "\n",
    "  for (int i = 0; i < a.length(); i++) {\n",
    "    a[i] = i;\n",
    "  }\n",
    "  std::cout << a << '\\n';\n",
    "\n",
    "  rml::array c(a);  // Copy constructor.\n",
    "\n",
    "  for (int i = 0; i < b.length(); i++) {\n",
    "    b[i] = i;\n",
    "  }  \n",
    "\n",
    "  try {\n",
    "    a = b;\n",
    "  }\n",
    "  catch (...) {\n",
    "    std::cout << \"Ack!  an exception when trying a = b!\" << std::endl;\n",
    "  }\n",
    "\n",
    "  try {\n",
    "    c = a;\n",
    "  }\n",
    "  catch (...) {\n",
    "    std::cout << \"Ack!  an exception when trying c = a!\" << std::endl;    \n",
    "  }\n",
    "}\n",
    "```\n",
    "The line\n",
    "```\n",
    "  rml::vector c(a)\n",
    "```\n",
    "uses the copy constructor to create `c` by copying `a`.  The line\n",
    "```\n",
    "  Fraction c = b;\n",
    "```\n",
    "creates `c` using the default constructor and uses `operator=` to copy `b`.\n",
    "\n",
    "We also use a `try`-`catch` construction to handle any exceptions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70514847",
   "metadata": {},
   "outputs": [],
   "source": [
    "g++ -std=c++17 -Wall ch04/array2.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3df6abf",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38651544-3bc0-4a8c-adc1-b4d96eb39b69",
   "metadata": {},
   "source": [
    "# 4.4 More operator overloading\n",
    "\n",
    "Let's add `operator+()` that adds two of our arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16978c32-4b76-47d9-8e82-ee7aa5fc72a4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/array3.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41192215-7249-453a-a682-10972b963cfb",
   "metadata": {},
   "outputs": [],
   "source": [
    "g++ -std=c++17 -Wall ch04/array3.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6768ee44-3823-41fc-bce3-8d56a72203e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ac99684-f230-4f37-a50b-ed681559c3c0",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 4.5 Move semantics\n",
    "\n",
    "We introduced `operator+()` to illustrate a problem.  When we compute `c = a + b`, `operator+()` returns an array containing `a + b` that is then copied into `c` and then is destroyed.  The copy really isn't necessary if we could somehow transfer ownership of the internals of the temporary `a + b` directly to `c`?  This motivates **move semantics** or **steal semantics**.\n",
    "\n",
    "The temporary `a + b` is an example of an **rvalue** (not to be confused with R-value, which has to do with insulation).  Roughly speaking, an rvalue is an entity that has no identity and cannot be assigned to, but which can appear on the right-hand side of an assignment (hence rvalue, for \"right\").  This is opposed to **lvalues** such as regular variables that can appear on the left-hand side of an assignment.\n",
    "\n",
    "Our move constructor looks like this:\n",
    "```\n",
    "// Move constructor.\n",
    "array(rml::array &&other) : n{other.n}, a{other.a} {\n",
    "  std::cout << \"Calling move constructor: array(rml::array&&)...\\n\";\n",
    "      \n",
    "  other.n = 0;  // Probably a good idea to decommision other.\n",
    "  other.a = nullptr;\n",
    "}\n",
    "```    \n",
    "The `&&` indicates that `other` is an rvalue.  We grab the length and pointer from the rvalue to construct our new array.\n",
    "\n",
    "The move assignment is very similar:\n",
    "```\n",
    "// Move assignment.\n",
    "rml::array &operator=(rml::array &&other) {\n",
    "  std::cout << \"Calling move assignment: operator=(rml::array&&)...\\n\";\n",
    "\n",
    "  n = other.n;\n",
    "  a = other.a;      \n",
    "      \n",
    "  other.n = 0;  // Decomission other.\n",
    "  other.a = nullptr;\n",
    "\n",
    "  return *this;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "caf9e07d-d744-497e-aeb9-6954d5dcd39e",
   "metadata": {},
   "source": [
    "Class definitions are usually placed in header files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aaf6b1ee-3c28-4379-8cd8-f425a59df5d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/array4.hpp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19d4d2bb-0dea-4414-9eb7-f1b749f745a5",
   "metadata": {},
   "source": [
    "**The Big Five** refers to the following methods of a class:\n",
    "1. the copy constructor,\n",
    "2. the assignment operator,\n",
    "3. the destructor,\n",
    "4. the move constructor, and\n",
    "5. the move assignment operator.\n",
    "\n",
    "The **Rule of the Big Five** is that if you implement any one of these you should have a policy in place for the others.  Some people go further and say that you **must** implement all of the others.\n",
    "\n",
    "Move semantics were introduced in C++11.  Prior to that it was the Rule of the Big Three (1-3 above).\n",
    "    \n",
    "Our `main()` is intended to illustrate how the Big Five work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7da992b4-ebbd-4509-8527-166f24dfcdf7",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/array4.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9e296ea-a10e-45b6-b033-62de1502736c",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "g++ -std=c++17 -Wall ch04/array4.cpp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06500671-d3bf-4eb5-92e3-30a25cca9818",
   "metadata": {},
   "source": [
    "I'll explain the warning after we run the code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "847e7332-9d3c-4403-b6a0-ddbad9bec911",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a8c7dc2-8117-4571-96ee-167108414541",
   "metadata": {},
   "source": [
    "You can compare this output with the comments in the code to follow the Big 5 calls.\n",
    "\n",
    "One oddity is at line 45:\n",
    "```\n",
    "ch04/array4.cpp, line 45 => ch04/array4.hpp, line 112:\n",
    "Calling allocate-only constructor: array(const int length)...\n",
    "```\n",
    "The corresponding code is\n",
    "```\n",
    "43  // Create a + b directly in d.  This is a g++ optimization.\n",
    "44  std::cout << __FILE__ << \", \" << \"line \" << __LINE__+1 << \" => \";\n",
    "45  rml::array d(a + b);\n",
    "```\n",
    "\n",
    "This puzzled me for a bit since `a + b` should be an rvalue and trigger a call to the move constructor.  Instead, it looks like we're computing `a + b` directly inside `d`!\n",
    "\n",
    "This is, in fact, what is going on.  The hint is in the warning we got about [**copy elision**](https://en.wikipedia.org/wiki/Copy_elision).  This is a compiler optimization to avoid unnecessary work.  The compiler can see we are copying a temporary entity so it simply makes the output of `operator+` the variable `d` without any copies or moves."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b019d0f0-8b5f-453a-a89d-2597011913ea",
   "metadata": {},
   "source": [
    "# 4.6 Templated classes\n",
    "\n",
    "As mentioned in our discussion of STL containers, classes in C++ can be templated.  This is very handy for things like our ``rml::array` class since there we can have arrays of objects of arbitrary type.\n",
    "\n",
    "The syntax is like that for templated functions.  In this case, we change all the mentions of `double` to a generic typename `T`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a2c77c8-115f-4f3a-a02b-9cb81d924610",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/array5.hpp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce647830-b84b-494c-9092-8eca96d08227",
   "metadata": {},
   "source": [
    "Here we instantiate an `int`, a `float`, and a `double` version of the arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "740943f7-112e-4cff-9b0c-a6698f657ced",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch04/array5.cpp"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
