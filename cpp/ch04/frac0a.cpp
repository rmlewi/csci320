#include <iostream>

class Fraction
{
public:
  // Default constructor.  
  Fraction() : num{0}, den{0} {}
  
  // Constructor.
  Fraction(int m, int n)
  {
    this->num = m;
    den = n;
  }

  long int num, den;
};

int main(void)
{
  Fraction a(1, 2);
  Fraction b(3, 4);
  Fraction c;

  std::cout << a.num << '/' << a.den << '\n';
  std::cout << b.num << '/' << b.den << '\n';
  std::cout << c.num << '/' << c.den << '\n';  
}
