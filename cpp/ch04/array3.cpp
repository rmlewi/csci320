#include <iostream>

namespace rml
{
  class array {
  public:
    // Constructor.
    array(const int length) : n{length} {
      a = new double[n];  // Allocate a buffer for n doubles.
    }

    // Copy constructor.
    array(const rml::array &other) : n{other.n} {
      a = new double[n];  // Allocate a buffer for n doubles.
      for (int i = 0; i < n; i++) {
        a[i] = other[i];
      }
    }

    // Copy assignment.
    rml::array &operator=(const rml::array &other)
    {
      // Check for self-assignment.
      if (this == &other) {
        return *this;
      }

      // Throw an exception if the arrays are not the same length.
      if (n != other.n) {
        throw std::runtime_error("Attempted assignment of arrays of different lengths!");
      }

      for (int i = 0; i < n; i++) {
        a[i] = other[i];
      }
      
      return *this;
    }    

    // Destructor. 
    ~array() {
      delete[] a;
    }

    long int length(void) const {
      return n;
    }

    double &operator[](long int i) const {
      return a[i];
    }

  private:
    long int n;  // The length of the array.
    double *a;   // The memory storing the array.
  };
}

std::ostream &operator<<(std::ostream &os, rml::array &a)
{
  for (int i = 0; i < a.length(); i++) {
    os << a[i] << ' ';
  }
  os << std::endl;

  return os;  // An ostream is an output stream.
}

rml::array operator+(const rml::array &a, const rml::array &b)
{
  rml::array c(a.length());

  // Throw an exception if the arrays are not the same length.
  if (a.length() != b.length()) {
    throw std::runtime_error("Attempted assignment of arrays of different lengths!");
  }

  for (int i = 0; i < c.length(); i++) {
    c[i] = a[i] + b[i];
  }

  return c;
}

int main(int argc, char **argv)
{
  rml::array a(42), b(42);

  for (int i = 0; i < a.length(); i++) {
    a[i] = i + 1;
  }
  std::cout << a << '\n';

  b = a;

  rml::array c = a + b;

  std::cout << c << '\n';
}
