#include <iostream>

class Fraction
{
public:
  // Default constructor.  
  Fraction() : num{0}, den{0} {}
  
  // Constructor.
  Fraction(const int m, const int n) : num{m}, den{n} {}

  // Copy constructor.
  Fraction(const Fraction &f) : num{f.num}, den{f.den} {}

  // Copy assignment.
  Fraction &operator=(const Fraction &other)
  {
    // Check for self-assignment.
    if (this == &other) {
      return *this;
    }

    num = other.get_num();
    den = other.get_den();

    return *this;
  }
  
  long int get_num() const {return num;}
  long int get_den() const {return den;}

private:
  long int num, den;
};

// Output operator.
std::ostream &operator<<(std::ostream &os, const Fraction &f)
{
  os << f.get_num() << '/' << f.get_den();

  return os;  // An ostream is an output stream.
}

// Addition.
Fraction operator+(const Fraction &f, const Fraction &g)
{
  return Fraction(f.get_num() * g.get_den() + f.get_den() * g.get_num(), f.get_den() * g.get_den());
}

// Multiplication.
Fraction operator*(const Fraction &f, const Fraction &g)
{
  return Fraction(f.get_num() * g.get_num(), f.get_den() * g.get_den());
}

int main(void)
{
  Fraction a(1, 2);
  Fraction b(3, 4);
  Fraction c;

  std::cout << "a = " << a << std::endl;
  std::cout << "b = " << b << std::endl;

  std::cout << "a + b = " << a + b << std::endl;

  std::cout << "a * b = " << a * b << std::endl;  
}
