#ifndef __ARRAY5__
#define __ARRAY5__

#include <iostream>

namespace rml
{
  template <typename T>
  class array {
  public:
    // Default constructor.
    array() : n{0}, a{nullptr} {
      std::cout << "Calling default constructor: array()...\n";
    };
    
    // Constructor.
    array(const int length) : n{length} {
      std::cout << "Calling allocate-only constructor: array(const int length)...\n";
      
      a = new T[n];  // Allocate a buffer for n items of type T.
    }

    // Copy constructor.
    array(const rml::array &other) : n{other.n} {
      std::cout << "Calling copy constructor: array(const rml::array&)...\n";
      
      a = new T[n];  // Allocate a buffer for n items of type T.
      for (int i = 0; i < n; i++) {
        a[i] = other[i];
      }
    }

    // Move constructor.
    array(rml::array &&other) : n{other.n}, a{other.a} {
      std::cout << "Calling move constructor: array(rml::array&&)...\n";

      // Decommision other so that when its destructor is called, we don't
      // free the memory we just moved.
      other.n = 0;
      other.a = nullptr;
    }

    // Copy assignment.
    array &operator=(const rml::array &other)
    {
      std::cout << "Calling copy assignment: operator=(const rml::array&)...\n";
      
      // Check for self-assignment.
      if (this == &other) {
        return *this;
      }

      // Throw an exception if the arrays are not the same length.
      if (n != other.n) {
        throw std::runtime_error("Attempted assignment of arrays of different lengths!");
      }

      for (int i = 0; i < n; i++) {
        a[i] = other[i];
      }
      
      return *this;
    }

    // Move assignment.
    rml::array &operator=(rml::array &&other) {
      std::cout << "Calling move assignment: operator=(rml::array&&)...\n";

      n = other.n;
      a = other.a;      
      
      // Decommision other so that when its destructor is called, we don't
      // free the memory we just moved.
      other.n = 0;
      other.a = nullptr;

      return *this;
    }
    
    // Destructor. 
    ~array() {
      std::cout << "Calling: ~array()...\n";      
      delete[] a;
    }

    long int length(void) const {
      return n;
    }

    T &operator[](long int i) const {
      return a[i];
    }

  private:
    long int n;  // The length of the array.
    T *a;   // The memory storing the array.
  };
}

std::ostream &operator<<(std::ostream &os, rml::array &a)
{
  for (int i = 0; i < a.length(); i++) {
    os << a[i] << ' ';
  }
  os << std::endl;

  return os;  // An ostream is an output stream.
}

rml::array operator+(const rml::array &a, const rml::array &b)
{
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";  
  rml::array c(a.length());

  // Throw an exception if the arrays are not the same length.
  if (a.length() != b.length()) {
    throw std::runtime_error("Attempted assignment of arrays of different lengths!");
  }

  for (int i = 0; i < c.length(); i++) {
    c[i] = a[i] + b[i];
  }

  return c;
}
#endif
