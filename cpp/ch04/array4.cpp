#include <iostream>
#include <vector>
#include "array4.hpp"

void foo(void);

int main (int argc, char **argv)
{
  const int n = 42;

  // Call the allocate-only constructor.  
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";
  rml::array a(n);

  // Call the copy constructor.  
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";
  rml::array b(a); 

  // 1. Create right-hand side,
  // 2. create a with the move constructor,
  // 3. destroy right-hand side.
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";
  a = rml::array(42);

  for (int i = 0; i < a.length(); i++) {
    a[i] = i;
  }

  // Call the copy assignment.
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";
  b = a;

  // Call the default constructor.  
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";
  rml::array c;

  // Create a + b,
  // move a + b to c with the move assignment,
  // destroy a + b.
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << " => ";  
  c = a + b;

  // Create a + b directly in d.  This is a g++ optimization.
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << " => ";
  rml::array d(a + b);

  // 1. Create a + b,
  // 2. create e with the move constructor,
  // 3. destroy a + b.
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << " => ";
  rml::array e = std::move(a + b);  

  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << " => ";
  foo();

  // Destroy the active rml::array.
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";  
  return 0;
}

void foo(void)
{
  // 1. Create rml::array(42),
  // 2. use the copy constructor 3 times to fill the vector,
  // 3. destroy rml::array(42)  
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";
  std::vector<rml::array> v(3, rml::array(42));

  // Call the destructor on the 3 entries in the vector.  
  std::cout << __FILE__ << ", " << "line " << __LINE__+1 << ":\n";
  return;
}
