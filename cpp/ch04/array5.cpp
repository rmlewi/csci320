#include <iostream>
#include <vector>
#include "array5.hpp"

void foo(void);

int main (int argc, char **argv)
{
  const int n = 42;
  rml::array<int> a(n);
  rml::array<float> b(n); 
  rml::array<double> b(n); 

  return 0;
}
