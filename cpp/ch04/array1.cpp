#include <iostream>

namespace rml
{
  class array {
  public:
    // Constructor.
    array(const int length) : n{length} {
      a = new double[n];  // Allocate a buffer for n doubles.
    };

    // Destructor. 
    ~array() {
      delete[] a;
    }

    long int length(void) {
      return n;
    }

    double &operator[](long int i) const {
      return a[i];
    }

  private:
    long int n;  // The length of the array.
    double *a;   // The memory storing the array.
  };
}

int main(int argc, char **argv)
{
  rml::array a(42);

  for (int i = 0; i < a.length(); i++) {
    a[i] = i;
  }

  for (int i = 0; i < a.length(); i++) {
    std::cout << a[i] << ' ';
  }
}
