#include <iostream>

int foo(const int &n);

void bar(int &n)
{
  n = 42;
}

int main(void)
{
  int n = 2;
  
  std::cout << "foo(2) = " << foo(2) << '\n';

  bar(n);
  std::cout << "After bar(n), n = " << n << '\n';  
  
  return 0;
}

int foo(const int &n)
{
  return n * n;
}
