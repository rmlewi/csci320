#include "square.hpp"

int foo(int a)
{
  // Instantiate a version of square() that takes an int and returns an int.

  return square<int, int>(a);
}

int bar(int a)
{
  // Instantiate a version of square() that takes an int and returns an double.  

  return square<double, int>(a);
}
