#include <string>

#include "square.hpp"

int foo(int a)
{
  // Instantiate a version of square() that takes a std::string and returns an int.

  std::string s = "The mediocre are always at their best!";
  
  return square<int, std::string>(s);
}
