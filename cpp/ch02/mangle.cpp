int square(int n);
int square(float x);
double square(double x);

int square(int n)
{
  return n*n;
}

int square(float x)
{
  return x*x;
}

double square(double x)
{
  return x*x;
}
