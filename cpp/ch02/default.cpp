#include <iostream>

int foo(const int n = 42);  // Default input value specified in prototype.

int bar(const int n = 42)  // Default input value specified in definition.
{
  return n * n * n;
}

int main(void)
{
  std::cout << "foo()  = " << foo() << '\n';
  std::cout << "foo(2) = " << foo(2) << '\n';

  std::cout << "bar()  = " << bar() << '\n';
  std::cout << "bar(2) = " << bar(2) << '\n';  
  
  return 0;
}

int foo(const int n)  // Default input value specified in prototype, not definition.
{
  return n * n;
}
