#include <iostream>
#include <vector>

void foo(void)
{
  std::vector<int> p(42);  // Allocate an vector for 42 ints.

  p[0] = 54;
  p[1] = 9;
  p[2] = 6;

  std::cout << p[0] << ", " << p[1] << ", " << p[2] << std::endl;
}

int main(int argc, char **argv)
{
  foo();
  foo();
}
