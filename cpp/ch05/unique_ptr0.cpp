#include <iostream>
#include <memory>

int main(void)
{
  const int n = 42;
  std::unique_ptr<int[]> a(new int[n]);

  for (int i = 0; i < n; i++) {
    a[i] = i+1;
  }
  for (int i = 0; i < n; i++) {
    std::cout << a[i] << ' ';
  }
  std::cout << std::endl;

  // The following is only included if we compile with -DFOO
  // This makes the macro FOO defined.
#ifdef FOO
  auto b = a; // This won't work!
  std::cout << "b[0]: " << b[0] << std::endl;    
#endif

#ifdef BAR
  auto b = std::move(a);
  std::cout << "b[0]: " << b[0] << std::endl;    
#endif

  return 0;
}
