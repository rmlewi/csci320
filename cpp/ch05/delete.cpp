int main(void)
{
  int *p = new int;
  int *q = new int[42];

  delete p;
  delete q;  // This should be delete[] q.

  return 42;;
}
