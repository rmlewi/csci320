#include <iostream>
#include <memory>

int *foo()
{
  const int n = 42;
  std::unique_ptr<int[]> a(new int[n]);

  a[0] = n;

  auto b = a.get();

  // The memory associated with a gets freed here,
  // so b is no longer a valid pointer.
  return b;
}

int main(void)
{
  auto p = foo();
  std::cout << p[0] << "\n";
  return 0;
}
