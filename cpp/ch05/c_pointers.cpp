#include <iostream.

int main(int argv, char **argv)
{
  // C style pointers.
  int *p = nullptr;
  int *q = nullptr;

  // C style pointers in C++ behave just as in C.

  q = new int;  // Allocate a single int.
  *q = 42;
  std::cout << *q << std::endl;

  delete q;  // Delete a pointer to a single object.

  p = new int[42];  // Allocate an array of 42 ints.
  p[0] = 54;
  *(p + 1) = 9;
  *(p + 2) = 6;

  std::cout << p[0] << ", " << p[1] << ", " << p[2] << std::endl;

  delete[] p;  // Observe the [].
}
