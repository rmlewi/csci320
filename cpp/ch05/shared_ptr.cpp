#include <iostream>
#include <memory>

void foo(std::shared_ptr<int[]> u)
{
  auto v = u;
  std::cout << "Inside foo(): " << u.use_count() << "\n";
  return;
}

void bar(std::shared_ptr<int[]> &u)
{
  auto v = u;
  std::cout << "Inside bar(): " << u.use_count() << "\n";
  return;
}

int main(void)
{
  std::shared_ptr<int[]> u(new int[42]);
  std::cout << "Use count: " << u.use_count() << "\n";
  
  std::shared_ptr<int[]> v(u);  // v and u point to the same array.
  std::cout << "Use count: " << u.use_count() << "\n";
  
  auto w = u;
  std::cout << "Use count: " << u.use_count() << "\n";

  foo(u);
  std::cout << "Back from foo()!\n";
  std::cout << "Use count: " << u.use_count() << "\n";

  bar(u);
  std::cout << "Back from bar()!\n";
  std::cout << "Use count: " << u.use_count() << "\n";

  return 0;
}
