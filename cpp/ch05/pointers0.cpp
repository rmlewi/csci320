#include <iostream>

void foo(void)
{
  int *p = nullptr;  // A C style pointer.

  p = new int[42];  // Allocate an array of 42 ints.

  // C style pointers in C++ behave just as in C.
  p[0] = 54;
  *(p + 1) = 9;
  *(p + 2) = 6;

  std::cout << p[0] << ", " << p[1] << ", " << p[2] << std::endl;
}

int main(int argc, char **argv)
{
  foo();
  foo();
}
