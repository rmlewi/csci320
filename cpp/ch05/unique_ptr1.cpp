#include <iostream>
#include <memory>

int main(void)
{
  const int n = 42;
  std::unique_ptr<int[]> a(new int[n]);

  auto b = a.get();
  std::cout << "a's pointer after get(): " << a.get() << "\n";
  std::cout << "b: " << b << "\n";

  auto c = a.release();
  std::cout << "a's pointer after release(): " << a.release() << "\n";
  std::cout << "c: " << c << "\n";

  return 0;
}
