#include <chrono>
#include <iostream>

int main (int argc, char **argv)
{
  auto n = 42 + 54;  // n will be an int.
  std::cout << "n is " << sizeof(n) << " bytes long." << std::endl;

  auto m = n;  // m will be an int.
  std::cout << "m is " << sizeof(m) << " bytes long." << std::endl;

  auto k = 42L + 54;  // 42L is a long int, so k is a long int.
  std::cout << "k is " << sizeof(k) << " bytes long." << std::endl;

  // What is s?  maybe a long int?  
  auto s = std::chrono::system_clock::now().time_since_epoch().count();
  std::cout << "s is " << sizeof(s) << " bytes long." << std::endl;  
  std::cout << "s = " << s << std::endl;
  
  return 0;
}
