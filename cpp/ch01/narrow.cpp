// Illustration of narrowing conversions.

void f()
{
  double x{1};
  float y{3.14};
  int n = {1024};
  char z = {127};

  x =  y;   // Not narrowing: float to double.
  y =  x;   // Narrowing but not flagged: double to float.
  y = {x};  // Narrowing but flagged: double to float.

  z = n;    // Narrowing but not flagged: int to char.
  z = {n};  // Narrowing but flagged: int to char.
}
