#include <iostream>

int main(int argc, char **argv)
{
  int n;
  int &m = n;

  n = 42;  // Set n.
  std::cout << "before: n = " << n << std::endl;

  m = 54;  // Set m, a reference to n, without using pointer dereferencing syntax *m.
  std::cout << "after:  n = " << n << std::endl;
}

