#include <iostream>

int main(void)
{
  int m = 42;
  int &n = m;  // n is a reference to m; reminiscent of int *n = &m

  std::cout << "m = " << m << '\n';
  std::cout << "n = " << n << '\n';

  n = 54;
  std::cout << "m = " << m << '\n';
  std::cout << "n = " << n << '\n';

  return 0;
}
