#include <iostream>
#include <vector>
 
int main() {
  // Create and initialize two vectors of integers.
  std::vector<int> v = {0, 1, 2, 3, 4, 5};
  std::vector<int> w = {0, 1, 2, 3, 4, 5};

  for (int i = 0; i < 5; i++) {  // A C style for loop.
    std::cout << v[i] << ' ';
  }
  std::cout << '\n';    
 
  for (int &e : v) {  // A range-for with loop access via a reference.
    std::cout << e << ' ';
    e = 42;  // Changes the value in v.
  }
  std::cout << '\n';

  for (auto &e : v) {  // A range-for using auto.
    std::cout << e << ' ';
  }
  std::cout << '\n';  
 
  for (auto e : w) {  // A range-for with loop access by value.
    std::cout << e << ' ';
    e = 42;  // Changes the value of copies of values in w; no change to w.
  }
  std::cout << '\n';

  for (auto e : w) {
    std::cout << e << ' ';
  }
  std::cout << '\n';  
}
