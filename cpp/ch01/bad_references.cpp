#include <iostream>

int main(int argc, char **argv)
{
  int &k;  // Invalid unbound reference.

  int m;
  int &n = m;  // n is a reference to m.

  int i;
  
  n = &i;  // References are immutable.
}

